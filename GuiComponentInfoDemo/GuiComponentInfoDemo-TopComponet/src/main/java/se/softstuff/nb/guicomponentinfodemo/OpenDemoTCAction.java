/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package se.softstuff.nb.guicomponentinfodemo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class OpenDemoTCAction implements ActionListener {

    public void actionPerformed(ActionEvent e) {
//        WindowManager manager = WindowManager.getDefault();
//        TopComponent tc = manager.findTopComponent("DemoTopComponent"); // NOI18N
        TopComponent tc = new DemoTopComponent();
        if (tc!=null && !tc.isOpened()) {
            tc.open();
            tc.requestActive();
        }
    }
}
