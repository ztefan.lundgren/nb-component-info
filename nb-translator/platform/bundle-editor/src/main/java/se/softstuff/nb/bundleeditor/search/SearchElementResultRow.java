package se.softstuff.nb.bundleeditor.search;

/**
 *
 * @author stefan
 */
public class SearchElementResultRow {
    private final String key;
    private final String original;
    private String value;
    private final String language;
    private final String module;
    private final String resource;

    public SearchElementResultRow(String key, String original, String value, String language, String module, String resouce) {
        this.key = key;
        this.original = original;
        this.value = value;
        this.language = language;
        this.module = module;
        this.resource = resouce;
    }

    public String getKey() {
        return key;
    }

    public String getOriginal() {
        return original;
    }
    
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLanguage() {
        return language;
    }

    public String getModule() {
        return module;
    }

    public String getResource() {
        return resource;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.key != null ? this.key.hashCode() : 0);
        hash = 97 * hash + (this.original != null ? this.original.hashCode() : 0);
        hash = 97 * hash + (this.module != null ? this.module.hashCode() : 0);
        hash = 97 * hash + (this.resource != null ? this.resource.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SearchElementResultRow other = (SearchElementResultRow) obj;
        if ((this.key == null) ? (other.key != null) : !this.key.equals(other.key)) {
            return false;
        }
        if ((this.original == null) ? (other.original != null) : !this.original.equals(other.original)) {
            return false;
        }
        if ((this.module == null) ? (other.module != null) : !this.module.equals(other.module)) {
            return false;
        }
        if ((this.resource == null) ? (other.resource != null) : !this.resource.equals(other.resource)) {
            return false;
        }
        return true;
    }
    
    
    
}
