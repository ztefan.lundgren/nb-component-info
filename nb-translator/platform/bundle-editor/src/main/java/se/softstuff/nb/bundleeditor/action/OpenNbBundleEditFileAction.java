/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.bundleeditor.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import org.apache.log4j.Logger;
import org.openide.awt.ActionRegistration;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionID;
import org.openide.cookies.OpenCookie;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.Exceptions;
import org.openide.windows.WindowManager;

@ActionID(category = "Tools/NbTranslate",
id = "se.softstuff.nb.bundleeditor.OpenNbBundleEditFileAction")
@ActionRegistration(iconBase = "se/softstuff/nb/bundleeditor/nb_translate.png",
displayName = "#CTL_OpenNbBundleEditFileAction")
@ActionReferences({
    @ActionReference(path = "Menu/Tools/NbTranslate", position = 0)
})
public final class OpenNbBundleEditFileAction implements ActionListener {

    private Logger log = Logger.getLogger(OpenNbBundleEditFileAction.class);
    @Override
    public void actionPerformed(ActionEvent e) {
        
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(createFileFilter());
        int returnVal = fileChooser.showOpenDialog(WindowManager.getDefault().getMainWindow());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                File file = fileChooser.getSelectedFile();
                FileObject f = FileUtil.toFileObject(file);
                DataObject find = DataObject.find(f);                
                OpenCookie open = find.getLookup().lookup(OpenCookie.class);
                log.debug("opening");
                open.open();
                
            } catch (DataObjectNotFoundException ex) {
                Exceptions.printStackTrace(ex);
            }


        } else {
            System.out.println("File access cancelled by user.");
        }
    }
    
    private FileFilter createFileFilter() {
        return new FileFilter() {

            @Override
            public boolean accept(File f) {
                return f.getName().endsWith(".xml");
            }

            @Override
            public String getDescription() {
                return "*.nbe";
            }
        };
    }
}
