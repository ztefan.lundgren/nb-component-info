package se.softstuff.nb.bundleeditor.search;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JToolBar;
import javax.swing.SwingWorker;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.core.spi.multiview.CloseOperationState;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.MultiViewElementCallback;
import org.openide.awt.UndoRedo;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import se.softstuff.nb.domain.NbBundleDiffDataObject;
import se.softstuff.nb.domain.swing.ColumnRenderer;
import se.softstuff.nb.domain.tracer.DontMarkThisComponent;

@MultiViewElement.Registration(displayName = "#LBL_NbBundleEdit_SEARCH",
iconBase = "se/softstuff/nb/bundleeditor/nb_translate.png",
mimeType = "text/nb-bundle-diff+xml",
persistenceType = TopComponent.PERSISTENCE_NEVER,
preferredID = "NbBundleSearch",
position = 2100)
public class SearchElement extends javax.swing.JPanel implements MultiViewElement, DontMarkThisComponent {

    private NbBundleDiffDataObject diffDataObject;

    private JToolBar toolbar = new JToolBar();

    private transient MultiViewElementCallback callback;

    /**
     * Creates new form SearchElement
     */
    public SearchElement(Lookup lookup) {
        initComponents();
        initColumns();
        diffDataObject = lookup.lookup(NbBundleDiffDataObject.class);
        pressentationModel.init(diffDataObject.getContent());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        searchResultTableModel = new se.softstuff.nb.domain.swing.DynamicTableModel();
        pressentationModel = new se.softstuff.nb.bundleeditor.search.SearchPressentationModel();
        searchTextField = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        searchResultTable = new javax.swing.JTable();
        searchButton = new javax.swing.JButton();

        setName("Form"); // NOI18N

        searchTextField.setName("searchTextField"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        searchResultTable.setModel(searchResultTableModel);
        searchResultTable.setName("searchResultTable"); // NOI18N
        jScrollPane1.setViewportView(searchResultTable);

        org.openide.awt.Mnemonics.setLocalizedText(searchButton, org.openide.util.NbBundle.getMessage(SearchElement.class, "SearchElement.searchButton.text")); // NOI18N
        searchButton.setName("searchButton"); // NOI18N
        searchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 638, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(searchTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(searchButton)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(searchTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(searchButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void searchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchButtonActionPerformed
        new SearchWorker(searchTextField.getText()).execute();
    }//GEN-LAST:event_searchButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private se.softstuff.nb.bundleeditor.search.SearchPressentationModel pressentationModel;
    private javax.swing.JButton searchButton;
    private javax.swing.JTable searchResultTable;
    private se.softstuff.nb.domain.swing.DynamicTableModel searchResultTableModel;
    private javax.swing.JTextField searchTextField;
    // End of variables declaration//GEN-END:variables

    @Override
    public String getName() {
        return "NbBundleSearch";
    }

    @Override
    public JComponent getVisualRepresentation() {
        return this;
    }

    @Override
    public JComponent getToolbarRepresentation() {
        return toolbar;
    }

    @Override
    public Action[] getActions() {
        return new Action[]{};
    }

    @Override
    public Lookup getLookup() {
        return diffDataObject.getLookup();
    }

    @Override
    public void componentOpened() {
    }

    @Override
    public void componentClosed() {
    }

    @Override
    public void componentShowing() {
    }

    @Override
    public void componentHidden() {
    }

    @Override
    public void componentActivated() {
    }

    @Override
    public void componentDeactivated() {
    }

    @Override
    public UndoRedo getUndoRedo() {
        return UndoRedo.NONE;
    }

    @Override
    public void setMultiViewCallback(MultiViewElementCallback callback) {
        this.callback = callback;
    }

    @Override
    public CloseOperationState canCloseElement() {
        return CloseOperationState.STATE_OK;
    }

    private void initColumns() {

        ColumnRenderer keyColumn = new ColumnRenderer<SearchElementResultRow, String>() {
            @Override
            public String headerText() {
                return NbBundle.getMessage(SearchElement.class, "SearchElement.key.column.header");
            }

            @Override
            public String render(SearchElementResultRow row) {
                return row.getKey();
            }
        };
        ColumnRenderer originalColumn = new ColumnRenderer<SearchElementResultRow, String>() {
            @Override
            public String headerText() {
                return NbBundle.getMessage(SearchElement.class, "SearchElement.original.column.header");
            }

            @Override
            public String render(SearchElementResultRow row) {
                return row.getOriginal();
            }
        };
        ColumnRenderer valueColumn = new ColumnRenderer<SearchElementResultRow, String>() {
            @Override
            public String headerText() {
                return NbBundle.getMessage(SearchElement.class, "SearchElement.value.column.header");
            }

            @Override
            public String render(SearchElementResultRow row) {
                return row.getValue();
            }

            @Override
            public boolean isCellEditable(SearchElementResultRow row) {
                return true;
            }

            @Override
            public void setValueAt(SearchElementResultRow row, String newValue) {
                pressentationModel.editRow(row, newValue);
                super.setValueAt(row, newValue);
            }            
        };

        ColumnRenderer langColumn = new ColumnRenderer<SearchElementResultRow, String>() {
            @Override
            public String headerText() {
                return NbBundle.getMessage(SearchElement.class, "SearchElement.language.column.header");
            }

            @Override
            public String render(SearchElementResultRow row) {
                return row.getLanguage();
            }
        };
        ColumnRenderer moduleColumn = new ColumnRenderer<SearchElementResultRow, String>() {
            @Override
            public String headerText() {
                return NbBundle.getMessage(SearchElement.class, "SearchElement.module.column.header");
            }

            @Override
            public String render(SearchElementResultRow row) {
                return row.getModule();
            }
        };

        ColumnRenderer resouceColumn = new ColumnRenderer<SearchElementResultRow, String>() {
            @Override
            public String headerText() {
                return NbBundle.getMessage(SearchElement.class, "SearchElement.resouce.column.header");
            }

            @Override
            public String render(SearchElementResultRow row) {
                return row.getResource();
            }
        };

        this.searchResultTableModel.addColumn(keyColumn, originalColumn, valueColumn, langColumn, moduleColumn, resouceColumn);
    }

    private class SearchWorker extends SwingWorker<Collection<SearchElementResultRow>, Void> {

        private String searchString;

        private ProgressHandle progress;

        public SearchWorker(String searchString) {
            this.searchString = searchString;
            progress = ProgressHandleFactory.createHandle(NbBundle.getMessage(SearchElement.class, "SearchElement.progress.title", searchString)); //NOI18N
        }

        @Override
        protected Collection<SearchElementResultRow> doInBackground() throws Exception {
            progress.start();
            Collection<SearchElementResultRow> rows = pressentationModel.search(searchString);
            return rows;
        }

        @Override
        protected void done() {
            progress.finish();
            try {
                searchResultTableModel.setRows(get());
            } catch (InterruptedException ex) {
                Exceptions.printStackTrace(ex);
            } catch (ExecutionException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }
}
