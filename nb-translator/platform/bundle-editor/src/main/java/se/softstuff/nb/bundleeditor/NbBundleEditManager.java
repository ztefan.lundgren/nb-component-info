package se.softstuff.nb.bundleeditor;

import se.softstuff.nb.domain.NbBundleDiff;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import org.apache.log4j.Logger;
import org.openide.DialogDisplayer;
import org.openide.NotifyDescriptor;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.*;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.util.Utilities;
import org.openide.util.editable.NbBundle;
import se.softstuff.nb.bundleeditor.editor.ComponentEditorPanel;
import se.softstuff.nb.domain.NbBundleDiffDataObject;
import se.softstuff.nb.domain.tracer.HighlightedComponent;
import se.softstuff.nb.domain.tracer.TracerManager;
import se.softstuff.nb.domain.xml.NbBundleDiffXmlHandler;

/**
 *
 * @author Stefan
 */
public class NbBundleEditManager implements PropertyChangeListener, VetoableChangeListener, FileChangeListener {

    private static final NbBundleEditManager INSTANCE = new NbBundleEditManager();

    public static NbBundleEditManager getInstance() {
        return INSTANCE;
    }
    private final Logger log = Logger.getLogger(getClass());
    private NbBundleDiffDataObject data;
    private FileLock lock;
    private NbBundleDiff diff;
    private EditableBundler bundler;

    private NbBundleEditManager() {
    }

    public boolean isAttached() {
        return data != null;
    }

    public synchronized void attach(NbBundleDiffDataObject data) throws IOException {
        detach();
        
        log.debug("start begin");
//        lock = data.getPrimaryFile().lock();
        data.addPropertyChangeListener(this);
        data.addVetoableChangeListener(this);
        data.getPrimaryFile().addFileChangeListener(this);

        diff = data.getContent();

        bundler = new EditableBundler(diff);
        NbBundle.setBundler(bundler);
        this.data = data;
        
        log.debug("start done");

    }

    public synchronized void detach() throws IOException {

        log.debug("stop begin");
        
        if(data == null){
            return;
        }

        if (data.isModified()) {
            String saveQuestion = NbBundle.getMessage(NbBundleEditManager.class, "NbBundleEditManager.already_attached.save_message", this.data.getName());
            NotifyDescriptor dialog = new NotifyDescriptor.Confirmation(this.data.getName(), saveQuestion, NotifyDescriptor.YES_NO_OPTION);
            Object answer = DialogDisplayer.getDefault().notify(dialog);
            if (answer == NotifyDescriptor.YES_OPTION) {
                saveToFile();
            }
        }

        bundler = null;
        NbBundle.setStandardBundler();

        data.removePropertyChangeListener(this);
        data.removeVetoableChangeListener(this);
        data.getPrimaryFile().removeFileChangeListener(this);
//        lock.releaseLock();
        data = null;
        
        TracerManager tracerManager = Lookup.getDefault().lookup(TracerManager.class);
        tracerManager.removePropertyChangeListener(this);
        log.debug("start done");
    }

    public synchronized void saveToFile() throws IOException {
        data.getCookie(SaveCookie.class).save();
    }

        
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        log.debug("propertyChange " + evt);
    }

    @Override
    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        log.debug("vetoableChange " + evt);
    }

    @Override
    public void fileFolderCreated(FileEvent fe) {
        log.debug("fileFolderCreated " + fe);
    }

    @Override
    public void fileDataCreated(FileEvent fe) {
        log.debug("fileDataCreated " + fe);
    }

    @Override
    public void fileChanged(FileEvent fe) {
        log.debug("fileChanged " + fe);
    }

    @Override
    public void fileDeleted(FileEvent fe) {
        log.debug("fileDeleted " + fe);
    }

    @Override
    public void fileRenamed(FileRenameEvent fe) {
        log.debug("fileRenamed " + fe);
    }

    @Override
    public void fileAttributeChanged(FileAttributeEvent fe) {
        log.debug("fileAttributeChanged " + fe);
    }

    
}
