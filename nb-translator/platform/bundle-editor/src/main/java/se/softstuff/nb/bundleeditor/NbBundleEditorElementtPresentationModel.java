package se.softstuff.nb.bundleeditor;

import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.prefs.Preferences;
import org.apache.log4j.spi.TriggeringEventEvaluator;
import org.openide.util.Lookup;
import org.openide.util.NbPreferences;
import se.softstuff.nb.bundleeditor.editor.ComponentEditorPanel;
import se.softstuff.nb.domain.NbBundleDiffDataObject;
import se.softstuff.nb.domain.tracer.HighlightedComponent;
import se.softstuff.nb.domain.tracer.TracerManager;
import se.softstuff.nb.domain.tracer.TracerTriggerKeys;

/**
 *
 * @author stefan
 */
public class NbBundleEditorElementtPresentationModel {
    
    public static final String PROP_START_TRIGGER = "startTrigger";
    public static final String PROP_DATA = "data";
    public static final String PROP_SHIFT_ENABLED = "shiftEnabled";
    public static final String PROP_CTRL_ENABLED = "ctrlEnabled";
    public static final String PROP_ALTENABLE = "altEnable";

    private boolean shiftEnabled = true;
    private boolean altEnable = true;
    private boolean ctrlEnabled = true;

    private transient final PropertyChangeSupport propertyChangeSupport;
    
    private TracerTriggerKeys startTrigger;
    
    private NbBundleDiffDataObject data;
    private Lookup lookup;

    public NbBundleEditorElementtPresentationModel() {
        
        propertyChangeSupport = new PropertyChangeSupport(this);
        loadTriggers();
    }
    
    public boolean isShiftEnabled() {
        return shiftEnabled;
    }

    public void setShiftEnabled(boolean shiftEnabled) {
        boolean oldShiftEnabled = this.shiftEnabled;
        this.shiftEnabled = shiftEnabled;
        propertyChangeSupport.firePropertyChange(PROP_SHIFT_ENABLED, oldShiftEnabled, shiftEnabled);
    }

    public boolean isCtrlEnabled() {
        return ctrlEnabled;
    }

    public void setCtrlEnabled(boolean ctrlEnabled) {
        boolean oldCtrlEnabled = this.ctrlEnabled;
        this.ctrlEnabled = ctrlEnabled;
        propertyChangeSupport.firePropertyChange(PROP_CTRL_ENABLED, oldCtrlEnabled, ctrlEnabled);
    }

    public boolean isAltEnable() {
        return altEnable;
    }

    public void setAltEnable(boolean altEnable) {
        boolean oldAltEnable = this.altEnable;
        this.altEnable = altEnable;
        propertyChangeSupport.firePropertyChange(PROP_ALTENABLE, oldAltEnable, altEnable);
    }
    
    
    

    final void loadTriggers() {
        Preferences pref = NbPreferences.forModule(NbBundleEditorElementtPresentationModel.class);
        
        startTrigger = new TracerTriggerKeys( pref,PROP_START_TRIGGER+"_"){

            @Override
            public boolean fireMarkedAndClicked(MouseEvent mouseEvent) {
                TracerManager tracer = getTracer();
                tracer.stopTracker();
                ComponentEditorPanel.edit(mouseEvent, data.getContent());
                return true;
            }
            
        };
        
        startTrigger.addPropertyChangeListener(new NotAllOffLogic());
        
        
    }
    
    void saveTriggers() {
        Preferences pref = NbPreferences.forModule(NbBundleEditorElementtPresentationModel.class);
        startTrigger.persist(pref, PROP_START_TRIGGER+"_");
    }
    
    
    
    public TracerTriggerKeys getStartTrigger() {
        return startTrigger;
    }

    public void setStartTrigger(TracerTriggerKeys startTrigger) {
        TracerTriggerKeys oldStartTrigger = this.startTrigger;
        this.startTrigger = startTrigger;
        propertyChangeSupport.firePropertyChange(PROP_START_TRIGGER, oldStartTrigger, startTrigger);
    }

    public NbBundleDiffDataObject getData() {
        return data;
    }

    public void setData(NbBundleDiffDataObject data) {
        propertyChangeSupport.firePropertyChange(PROP_DATA, this.data, this.data = data);
    }
    
    

    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    void setup(Lookup lkp) {
        this.lookup = lkp;
        setData( lookup.lookup(NbBundleDiffDataObject.class));
    }

    Lookup getLookup() {
        return lookup;
    }
    
    TracerManager getTracer(){
        return Lookup.getDefault().lookup(TracerManager.class);
    }

    
    private class NotAllOffLogic implements PropertyChangeListener {


        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if(evt.getNewValue().equals(Boolean.TRUE)){
                setShiftEnabled(true);
                setCtrlEnabled(true);
                setAltEnable(true);
            } else {
                if(sum()==1) {
                    setShiftEnabled(!startTrigger.isShift());
                    setCtrlEnabled(!startTrigger.isCtrl());
                    setAltEnable(!startTrigger.isAlt());
                }
            }
        }
        
        int sum(){
            int sum = 0;
            if( startTrigger.isShift()){
                sum++;
            }
            if(startTrigger.isCtrl()){
                sum++;
            }
            if(startTrigger.isAlt()){
                sum++;
            }
            return sum;
        } 
        
    }
}
