package se.softstuff.nb.bundleeditor.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.loaders.DataObject;
import org.openide.awt.ActionID;
import org.openide.awt.ActionRegistration;

@ActionID(
    category = "File",
id = "se.softstuff.nb.bundleeditor.SaveAction")
@ActionRegistration(
    iconBase = "se/softstuff/nb/bundleeditor/save.png",
displayName = "#CTL_SaveAction")
public final class SaveAction implements ActionListener {

    private final DataObject context;

    public SaveAction(DataObject context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        // TODO use context
    }
}
