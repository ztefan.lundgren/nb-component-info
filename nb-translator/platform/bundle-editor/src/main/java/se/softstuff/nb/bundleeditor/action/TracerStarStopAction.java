/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.bundleeditor.action;

import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.util.Lookup;
import se.softstuff.nb.domain.tracer.TracerManager;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;

public final class TracerStarStopAction extends AbstractAction implements PropertyChangeListener {

    private final static ImageIcon ICON = ImageUtilities.loadImageIcon("se/softstuff/nb/bundleeditor/action/marker.png", false);
            
    private TracerManager tracer;
    public TracerStarStopAction() {
        tracer = Lookup.getDefault().lookup(TracerManager.class);
        if(tracer == null){
            throw new IllegalStateException("No TracerManager is registerd"); //NOI18N
        }
        
        putValue(Action.SMALL_ICON, ICON);
        putValue(Action.LARGE_ICON_KEY, ICON);
        tracer.addPropertyChangeListener((PropertyChangeListener)this);
        updateTitle();
    }

    @Override
    public void actionPerformed(ActionEvent ev) {
        
        
        
        tracer.toggleTracker();
        
        updateTitle();
        
    }

    private void updateTitle() {
       String key = String.format("TracerStarStopAction.%s.title", (tracer.isTrackerEnabled()?"stop":"start"));
       String title = NbBundle.getMessage(TracerStarStopAction.class, key);
       
       super.putValue(Action.NAME, title);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        
        updateTitle();
    }
}
