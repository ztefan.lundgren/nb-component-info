package se.softstuff.nb.bundleeditor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.*;
import javax.swing.table.AbstractTableModel;
import org.openide.util.editable.EditableNbResourceBundler;
import org.openide.util.editable.NbBundle;
import org.openide.util.editable.NbResourceBundler;

/**
 *
 * @author stefan
 */
public class BundleRowNodeTableModel extends AbstractTableModel{


    public enum COLUMN {
        BINARY, BUNDLE, KEY
    };
    
    private final List<BundleRowNode> rows;

    private final List<String> headers;
        
    private List<String> firstColumns;
    private Set<String> langColumns;

    public BundleRowNodeTableModel() {
                        
        rows = new ArrayList<BundleRowNode>();
        headers = new ArrayList<String>();
        langColumns = new HashSet<String>();
        firstColumns = new ArrayList<String>(3);
        firstColumns.add(NbBundle.getMessage(this.getClass(), "Table.column.header.binary")); //NOI18N
        firstColumns.add(NbBundle.getMessage(this.getClass(), "Table.column.header.bundle")); //NOI18N
        firstColumns.add(NbBundle.getMessage(this.getClass(), "Table.column.header.key")); //NOI18N

    }
    
    
    public void initHeaders() {
        langColumns.clear();
        addHeaders(Collections.EMPTY_LIST);
    }

    @Override
    public int getRowCount() {
        return rows.size();
    }

    @Override
    public int getColumnCount() {
        return headers.size();
    }

    @Override
    public String getColumnName(int column) {
        if(column>=headers.size()){
            return ""; //NOI18N
        }
        return headers.get(column);
    }

    
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (rowIndex >= rows.size() || columnIndex >= getColumnCount()) {
            return null;
        }
        BundleRowNode row = rows.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return row.getBinary();
            case 1:
                return row.getBundlePackage();
            case 2:
                return row.getBundleKey();
            default:
                String header = getColumnName(columnIndex);
                return row.getValue(header);
        }
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return String.class;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
//        String header = getColumnName(columnIndex);
//        BundleRowNode row = rows.get(rowIndex);
//        row.isColumnEditable(header);
        
        NbResourceBundler bundler = NbBundle.getBundler();
        return (bundler instanceof EditableNbResourceBundler);
//        Iterator<? extends BundleEditorService> iterator = Lookup.getDefault().lookupAll(BundleEditorService.class).iterator();
//        while(iterator.hasNext()){
//            if(iterator.next().isEditable()){
//                return true;
//            }
//        }
//        return super.isCellEditable(rowIndex, columnIndex);
    }

    
    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        String newValue = (String)aValue;
        
        String header = getColumnName(columnIndex);
        BundleRowNode row = rows.get(rowIndex);
        row.setValue(header, newValue);
                
//        Locale language = getLanguage(columnIndex);
//        String value = (String)aValue;
//        
//        Iterator<? extends BundleEditorService> iterator = Lookup.getDefault().lookupAll(BundleEditorService.class).iterator();
//        while(iterator.hasNext()){
//            value = iterator.next().setValue(row, language, value);
//        }
        super.setValueAt(newValue, rowIndex, columnIndex);
        fireTableCellUpdated(rowIndex, columnIndex);
    }
    
    
    public void clearRows(){
        this.rows.clear();
        fireTableDataChanged();
    }
    
    public void addRows(Collection<? extends BundleRowNode> rows ){
        this.rows.addAll(rows);
        
    }
    
    
    public void addHeaders(Collection<String> newHeaders) {
        langColumns.addAll(newHeaders);
        headers.clear();
        headers.addAll(firstColumns);
        headers.addAll(langColumns);
    }
    
    public void updateCompleted() {
        fireTableStructureChanged();
//        fireTableDataChanged();
    }
}
