package se.softstuff.nb.bundleeditor.editor;

import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Component;
import java.awt.TextArea;
import java.awt.TextComponent;
import java.awt.TextField;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import javax.swing.AbstractButton;
import javax.swing.JLabel;
import javax.swing.text.JTextComponent;
import org.openide.util.editable.BrandedLocale;
import org.openide.util.editable.BrandedLocaleBuilder;
import org.openide.util.editable.EditableResourceBundle;
import org.openide.util.editable.NbBundle;
import se.softstuff.nb.bundleeditor.EditableBundler;
import se.softstuff.nb.bundleeditor.search.SearchElementResultRow;
import se.softstuff.nb.domain.NbBundleDiff;
import se.softstuff.nb.domain.tracer.MarkerUtilities;
import se.softstuff.nb.domain.tracer.MarkerUtilities.BundlePath;

/**
 *
 * @author stefan
 */
public class ComponentEditorPresentationModel {

    private Component component;
    private List<EditableResourceBundle> bundles;
    private NbBundleDiff translation;
    private EditableBundler editableBundler;
    
    
    void init(Component componentToTranslate, NbBundleDiff translation, EditableBundler editableBundler) {
        component = componentToTranslate;
        this.editableBundler = editableBundler;
        String componentText = findComponentText(component);
        findBundlesInParentPath(componentToTranslate);
        this.translation = translation;
    }

    private void findBundlesInParentPath(Component forComponent) {
        
        editableBundler.clearCache();
        bundles = new ArrayList<EditableResourceBundle>();
        
        String branding = NbBundle.getBranding();
        
        while(forComponent != null){
            Class clazz = forComponent.getClass();
            
            String resourceBase = MarkerUtilities.findResoursePath(clazz)+"Bundle";
            ClassLoader classLoader = clazz.getClassLoader();
            BundlePath bundlePaths = MarkerUtilities.findAllBundlePaths(clazz);
            for(String resource : bundlePaths.bundleResoursePaths){
                BrandedLocale locale = BrandedLocaleBuilder.parse(resource);
                EditableResourceBundle bundle = editableBundler.getBundle(locale.getBranding(), resourceBase, locale.getLocale(), classLoader, false);

                bundles.add(bundle);
            }
            
            forComponent = forComponent.getParent();
        }
    }

    String findComponentText(Component comp) {
        String text = null;
        
        if(comp instanceof TextComponent){ // TextArea, TextField
            text = ((TextComponent)comp).getText();            
        } else if(comp instanceof JTextComponent){ // JEditorPane, JTextArea, JTextField
            text = ((JTextComponent)comp).getText();            
        } else if(comp instanceof Button){
            text = ((Button)comp).getLabel();
        }else if(comp instanceof Checkbox){
            text = ((Checkbox)comp).getLabel();
        } else if(comp instanceof AbstractButton){ // JButton, JMenuItem, JToggleButton, JCheckBox, JRadioButton
            text = ((AbstractButton)comp).getText();
        } else if(comp instanceof JLabel){
            text = ((JLabel)comp).getText();
        }
        return text;
    }
    
    void setComponentText(Component comp, String newText) {
        
        if(comp instanceof TextArea){ 
            ((TextArea)comp).setText(newText);    
        } else if(comp instanceof TextField){ 
            ((TextField)comp).setText(newText); 
        } else if(comp instanceof JTextComponent){ // JEditorPane, JTextArea, JTextField
            ((JTextComponent)comp).setText(newText);            
        } else if(comp instanceof Button){
            ((Button)comp).setLabel(newText);
        }else if(comp instanceof Checkbox){
            ((Checkbox)comp).setLabel(newText);
        } else if(comp instanceof AbstractButton){ // JButton, JMenuItem, JToggleButton, JCheckBox, JRadioButton
            ((AbstractButton)comp).setText(newText);
        } else if(comp instanceof JLabel){
            ((JLabel)comp).setText(newText);
        }
    }

    List<SearchElementResultRow> search(String nameSearch, String textSearch) {
        List<SearchElementResultRow> rows = new ArrayList<SearchElementResultRow>();
        
        for(EditableResourceBundle bundle : bundles) {
            Enumeration<String> keys = bundle.getKeys();
            String binar = bundle.getBundleFile();
            String resource = createResourceKey(bundle.getResourceBaseName(),bundle.getBrandingToken(),bundle.getLocale());
            String languageDisp = bundle.getLocale().getDisplayName();
            String language = bundle.getLocale().toString();
            String brandingToken = bundle.getBrandingToken();
//            NbBundleDiff.getBundleBrandingAndLocaleString(module)
            String module = MarkerUtilities.locateModuleRef(binar);
            
            while(keys.hasMoreElements()){
                String key = keys.nextElement();
                String value = bundle.getString(key);
                String original = bundle.getOrginalString(key);
                
                if(containsOrNull(key,nameSearch) && (containsOrNull(value,textSearch) || containsOrNull(original,textSearch))){                   
                    
                    SearchElementResultRow row = new SearchElementResultRow(key, original, value, language, module, resource);
                    rows.add(row);
                }
            }
        }
        
        return rows;
    }
   private boolean containsOrNull(String value, String compare){
       if(compare == null || value == null || compare.isEmpty() || value.isEmpty()){
           return true;
       }
       return value.toLowerCase().contains(compare.toLowerCase());
   }

    void editRow(SearchElementResultRow row, String newValue) {
        translation.addResourceChange(row.getModule(), row.getResource(), row.getKey(), newValue);
        row.setValue(newValue);
    }
    
    void editLanguageRow(LanguageRowModel row, String newValue) {
        translation.addResourceChange(row.getModule(), row.getResource(), row.getKey(), newValue);
        row.setNewValue(newValue);
    }

    String createResourceKey(String resourceBaseName, String brandingToken, Locale locale) {
        StringBuilder sb = new StringBuilder(resourceBaseName);
        if(brandingToken != null){
            sb.append("_").append(brandingToken);
        }
        if(locale != null && !locale.toString().isEmpty()){
            sb.append("_").append(locale);
        }
        sb.append(".properties");
        return sb.toString();
    }

    private Collection<LanguageRowModel> createRows(BundlePath bundlePaths, SearchElementResultRow selected) {
        List<LanguageRowModel> rows = new LinkedList<LanguageRowModel>();
        String module = getModule(bundlePaths.jarName);
        
        if(!module.equals(selected.getModule())){
            return rows;
        }
        String selectedResourcePath = MarkerUtilities.locateResourcePath(selected.getResource());
        
        List<String> resources = new ArrayList<String>(bundlePaths.bundleResoursePaths);
        List<String> languages = new ArrayList<String>(bundlePaths.bundleTypes);
        if(resources.size() != languages.size()){
            throw new IllegalArgumentException(String.format("resources.size %s != languages.size %s for ", resources.size(), languages.size(), bundlePaths));
        }
        for(int i=0; i< resources.size(); i++) {
            String resource = resources.get(i).replace("/", ".");
            String resourcePath = MarkerUtilities.locateResourcePath(resources.get(i));
            
            if(!resourcePath.equals(selectedResourcePath)){
                continue;
            }
            
            for(EditableResourceBundle bundle : bundles){
                String currentFile = bundle.getBundleFile();
                if(currentFile == null) {
                    continue;
                }
                
                String currentResourceFile = MarkerUtilities.locateResourceFile(currentFile);
                String currentResourcePath = MarkerUtilities.locateResourcePath(currentFile);
                
                String currentModule = MarkerUtilities.locateModuleRef(currentFile);
                String language = MarkerUtilities.getBundleBrandingAndLocaleString(currentFile);
                
                if(currentModule.equals(module) && currentResourcePath.equals(selectedResourcePath)){
                    String orgValue = bundle.getOrginalString(selected.getKey());
                    String newValue = bundle.getString(selected.getKey());
                    LanguageRowModel row = new LanguageRowModel(currentModule, currentResourceFile, language, selected.getKey(), orgValue, newValue);
                    rows.add(row);
                }
            }
            
//            
        }
        return rows;
    }

    
    Collection<LanguageRowModel> findLanguageRowsFor(SearchElementResultRow selected) {
        Set<LanguageRowModel> result = new HashSet<LanguageRowModel>();
        Component current = component;
        while(current != null){
            
            BundlePath bundlePaths = MarkerUtilities.findAllBundlePaths(current.getClass());
            
            Collection<LanguageRowModel> rows = createRows(bundlePaths, selected); 
            result.addAll(rows);
            
            current = current.getParent();
        }
        
        return result;
    }

    private String getModule(String jarName) {
        int end = jarName.lastIndexOf(".");
        if(end != -1){
            jarName = jarName.substring(0, end);
        }
        return jarName;
    }

    
    
}
