package se.softstuff.nb.bundleeditor.editor;

import org.openide.util.editable.NbBundle;
import se.softstuff.nb.domain.swing.ColumnRenderer;

/**
 *
 * @author stefan
 */
public class LanguageRowModel {
    private final String module;
        private final String resource;
        private final String language;
        private final String key;
        private final String standardValue;
        private String newValue;

    public LanguageRowModel(String module, String resource, String language, String key, String standardValue, String newValue) {
        this.module = module;
        this.resource = resource;
        this.language = language;
        this.key = key;
        this.standardValue = standardValue;
        this.newValue = newValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getStandardValue() {
        return standardValue;
    }
    
    public String getModule() {
        return module;
    }

    public String getResource() {
        return resource;
    }

    public String getLanguage() {
        return language;
    }

    public String getKey() {
        return key;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.module != null ? this.module.hashCode() : 0);
        hash = 97 * hash + (this.resource != null ? this.resource.hashCode() : 0);
        hash = 97 * hash + (this.language != null ? this.language.hashCode() : 0);
        hash = 97 * hash + (this.key != null ? this.key.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LanguageRowModel other = (LanguageRowModel) obj;
        if ((this.module == null) ? (other.module != null) : !this.module.equals(other.module)) {
            return false;
        }
        if ((this.resource == null) ? (other.resource != null) : !this.resource.equals(other.resource)) {
            return false;
        }
        if ((this.language == null) ? (other.language != null) : !this.language.equals(other.language)) {
            return false;
        }
        if ((this.key == null) ? (other.key != null) : !this.key.equals(other.key)) {
            return false;
        }
        return true;
    }

    
        
        
        

    @Override
    public String toString() {
        return "LanguageRowModel{" + "language=" + language + ", key=" + key + ", newValue=" + newValue + '}';
    }
    
//    private final String module;
//        private final String resource;
//        private final String language;
//        private final String key;
//        private final String standardValue;
//        private String newValue;
    public static ColumnRenderer createModuleColumn(){
        return new ColumnRenderer<LanguageRowModel,String>(){

            @Override
            public String headerText() {
                return NbBundle.getMessage(LanguageRowModel.class, "LanguageRowModel.module.header");
            }

            @Override
            public String render(LanguageRowModel row) {
                return row.getModule();
            }            
        };
    }
    
    public static ColumnRenderer createResourceColumn(){
        return new ColumnRenderer<LanguageRowModel,String>(){

            @Override
            public String headerText() {
                return NbBundle.getMessage(LanguageRowModel.class, "LanguageRowModel.resource.header");
            }

            @Override
            public String render(LanguageRowModel row) {
                return row.getResource();
            }            
        };
    }
    
    public static ColumnRenderer createLanguageColumn(){
        return new ColumnRenderer<LanguageRowModel,String>(){

            @Override
            public String headerText() {
                return NbBundle.getMessage(LanguageRowModel.class, "LanguageRowModel.language.header");
            }

            @Override
            public String render(LanguageRowModel row) {
                return row.getLanguage();
            }            
        };
    }
    
    public static ColumnRenderer createKeyColumn(){
        return new ColumnRenderer<LanguageRowModel,String>(){

            @Override
            public String headerText() {
                return NbBundle.getMessage(LanguageRowModel.class, "LanguageRowModel.key.header");
            }

            @Override
            public String render(LanguageRowModel row) {
                return row.getKey();
            }            
        };
    }
    
    public static ColumnRenderer createStandardValueColumn(){
        return new ColumnRenderer<LanguageRowModel,String>(){

            @Override
            public String headerText() {
                return NbBundle.getMessage(LanguageRowModel.class, "LanguageRowModel.standardValue.header");
            }

            @Override
            public String render(LanguageRowModel row) {
                return row.getStandardValue();
            }            
        };
    }
        
}
