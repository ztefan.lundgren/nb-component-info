package se.softstuff.nb.bundleeditor;

import java.awt.Component;
import java.awt.Container;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JLabel;
import org.openide.util.editable.EditableResourceBundle;
import org.openide.util.editable.NbBundle;
import org.openide.util.editable.NbResourceBundler;
import org.openide.util.editable.EditableNbResourceBundler;
import se.softstuff.nb.domain.tracer.MarkerUtilities;

/**
 * Represents the row model
 * @author stefan
 */
public class BundleRowNode {

    private final String bundlePackage;

    private final String bundleKey;

    private final String binary;

    private final Component markedComponent;
    
    private final Class markedComponentClass;
    
    private final String baseName; 
    
    private final BundleRowEngine engine;

    /**
     * 
     * @param key bundle key
     * @param clazz the marked component class
     */
    public BundleRowNode(String bundlePackage, String bundleKey, Component markedComponent, BundleRowEngine engine) {
        this.bundlePackage = bundlePackage;
        this.bundleKey = bundleKey;
        this.markedComponent = markedComponent;
        this.markedComponentClass = markedComponent.getClass();
        this.binary = MarkerUtilities.findJarFileName(markedComponentClass);
        this.baseName = findName(markedComponentClass);
        this.engine = engine;
    }

    public String getBundlePackage() {
        return bundlePackage;
    }

    public String getBundleKey() {
        return bundleKey;
    }

    public String getBaseName() {
        return baseName;
    }
    
    public String getBinary() {
        return binary;
    }

    public Class getMarkedComponentClass() {
        return markedComponentClass;
    }
    
    
    public String getValue(String columnHeader) {
        try {
            String branding = engine.getBranding(columnHeader);
            Locale language = engine.getLocale(columnHeader);
            ClassLoader classLoader = markedComponentClass.getClassLoader();
            NbResourceBundler bundler = NbBundle.getBundler();
            ResourceBundle bundle;
            if(bundler instanceof EditableNbResourceBundler){
                EditableNbResourceBundler exactBundle = (EditableNbResourceBundler)bundler;
                bundle = exactBundle.getBundle(branding, baseName, language, classLoader, false);
            } else {
                bundle = bundler.getBundle(baseName, language, classLoader);
            }
            String value = bundle.getString(bundleKey);
            return value;
        } catch (MissingResourceException ex) {
            return null;
        }
    }
    
    public void setValue(String columnHeader, String newValue) {
        String branding = engine.getBranding(columnHeader);
        Locale language = engine.getLocale(columnHeader);
        ClassLoader classLoader = markedComponentClass.getClassLoader();
        NbResourceBundler bundler = NbBundle.getBundler();
        if(bundler instanceof EditableNbResourceBundler){
            EditableNbResourceBundler exactBundle = (EditableNbResourceBundler)bundler;
            EditableResourceBundle bundle = exactBundle.getBundle(branding, baseName, language, classLoader, false);
            
            bundle.put(bundleKey, newValue);
            
        } 
        
    }

    @Override
    public String toString() {
        return bundleKey;
    }

    /** Finds package name for given class */
    private static String findName(Class clazz) {
        String pref = clazz.getName();
        int last = pref.lastIndexOf('.');

        if (last >= 0) {
            pref = pref.substring(0, last + 1);

            return pref + "Bundle"; // NOI18N
        } else {
            // base package, search for bundle
            return "Bundle"; // NOI18N
        }
    }

  
    
}
