package se.softstuff.nb.bundleeditor.editor;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import org.openide.util.Lookup;
import se.softstuff.nb.domain.tracer.HighlightedComponent;
import se.softstuff.nb.domain.tracer.TracerManager;
import se.softstuff.nb.domain.tracer.TracerTriggerKeys;

/**
 *
 * @author stefan
 */
public class EditorTrigger implements PropertyChangeListener {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
//            if(evt.getPropertyName().equalsIgnoreCase(TracerTriggerKeys.PROP_TRIGGERED)){
//                HighlightedComponent highlighted = Lookup.getDefault().lookup(HighlightedComponent.class);
//                if(highlighted != null){
//                    TracerManager tracer = getTracer();
//                    tracer.removePropertyChangeListener(this);
//                    tracer.stopTracker();
//                    ComponentEditorPanel.edit(highlighted);
//                }
//            }
        }
        
        private TracerManager getTracer() {
        return Lookup.getDefault().lookup(TracerManager.class);
    }
    
}
