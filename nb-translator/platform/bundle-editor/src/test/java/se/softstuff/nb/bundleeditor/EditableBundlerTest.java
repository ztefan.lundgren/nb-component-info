/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.bundleeditor;

import java.util.Locale;
import org.junit.*;
import static org.junit.Assert.*;
import org.openide.util.editable.EditableResourceBundle;

/**
 *
 * @author Stefan
 */
public class EditableBundlerTest {
    
    private EditableBundler instance;
    
    private String path = "file:/C:/work/guicomponentinfo~svn/trunk/GuiComponentInfoDemo/application/target/guicomponentinfodemo/guicomponentinfodemo/modules/se-softstuff-nb-GuiComponentInfoDemo-TopComponet.jar!/se/softstuff/nb/guicomponentinfodemo/Bundle_sv_SE.properties";
    
    @Before
    public void setUp() {
        instance = new EditableBundler(null);
    }
    

//    /**
//     * Test of getBranding method, of class EditableBundler.
//     */
//    @Test
//    public void testGetBranding() {
//        System.out.println("getBranding");
//        EditableBundler instance = null;
//        String expResult = "";
//        String result = instance.getBranding();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setBranding method, of class EditableBundler.
//     */
//    @Test
//    public void testSetBranding() {
//        System.out.println("setBranding");
//        String brandingToken = "";
//        EditableBundler instance = null;
//        instance.setBranding(brandingToken);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getBundle method, of class EditableBundler.
//     */
//    @Test
//    public void testGetBundle_3args() {
//        System.out.println("getBundle");
//        String baseName = "";
//        Locale locale = null;
//        ClassLoader loader = null;
//        EditableBundler instance = null;
//        EditableResourceBundle expResult = null;
//        EditableResourceBundle result = instance.getBundle(baseName, locale, loader);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getBundle method, of class EditableBundler.
//     */
//    @Test
//    public void testGetBundle_4args() {
//        System.out.println("getBundle");
//        String branding = "";
//        String baseName = "";
//        Locale locale = null;
//        ClassLoader loader = null;
//        EditableBundler instance = null;
//        EditableResourceBundle expResult = null;
//        EditableResourceBundle result = instance.getBundle(branding, baseName, locale, loader);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getBundle method, of class EditableBundler.
//     */
//    @Test
//    public void testGetBundle_5args() {
//        System.out.println("getBundle");
//        String branding = "";
//        String baseName = "";
//        Locale locale = null;
//        ClassLoader loader = null;
//        boolean useFallback = false;
//        EditableBundler instance = null;
//        EditableResourceBundle expResult = null;
//        EditableResourceBundle result = instance.getBundle(branding, baseName, locale, loader, useFallback);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getEditableBundle method, of class EditableBundler.
//     */
//    @Test
//    public void testGetEditableBundle() {
//        System.out.println("getEditableBundle");
//        String brandingToken = "";
//        String baseName = "";
//        Locale locale = null;
//        ClassLoader loader = null;
//        boolean useFallback = false;
//        EditableBundler instance = null;
//        EditableResourceBundle expResult = null;
//        EditableResourceBundle result = instance.getEditableBundle(brandingToken, baseName, locale, loader, useFallback);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of clearCache method, of class EditableBundler.
//     */
//    @Test
//    public void testClearCache() {
//        System.out.println("clearCache");
//        EditableResourceBundle bundle = null;
//        EditableBundler instance = null;
//        boolean expResult = false;
//        boolean result = instance.clearCache(bundle);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of loadBundle method, of class EditableBundler.
//     */
//    @Test
//    public void testLoadBundle() {
//        System.out.println("loadBundle");
//        String brandingToken = "";
//        String baseName = "";
//        Locale preferredLocale = null;
//        ClassLoader loader = null;
//        boolean useFallback = false;
//        EditableBundler instance = null;
//        EditableResourceBundle expResult = null;
//        EditableResourceBundle result = instance.loadBundle(brandingToken, baseName, preferredLocale, loader, useFallback);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    /**
     * Test of extractResourceKey method, of class EditableBundler.
     */
    @Test
    public void testExtractResourceKey() {
        String expResult = "se.softstuff.nb.guicomponentinfodemo.Bundle_sv_SE.properties";
        String result = instance.extractResourceKey(path);
        assertEquals(expResult, result);
    }

    /**
     * Test of extractModuleKey method, of class EditableBundler.
     */
    @Test
    public void testExtractModuleKey() {
        String expResult = "se-softstuff-nb-GuiComponentInfoDemo-TopComponet";
        String result = instance.extractModuleKey(path);
        assertEquals(expResult, result);
    }
}
