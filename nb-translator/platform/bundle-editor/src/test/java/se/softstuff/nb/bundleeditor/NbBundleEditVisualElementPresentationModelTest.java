/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.bundleeditor;

import org.junit.*;
import static org.junit.Assert.*;
import se.softstuff.nb.domain.tracer.TracerTriggerKeys;

/**
 *
 * @author Stefan
 */
public class NbBundleEditVisualElementPresentationModelTest {
    
    @Before
    public void setUp() {
    }
    

    
    @Test
    public void testNotAllOffLogic() {
        
        NbBundleEditVisualElementPresentationModel instance = new NbBundleEditVisualElementPresentationModel();
        instance.loadTriggers();
        TracerTriggerKeys result = instance.getStartTrigger();
        // defaults
        assertTrue(result.isShift());
        assertTrue(result.isCtrl());
        assertFalse(result.isAlt());
        assertTrue(instance.isShiftEnabled());
        assertTrue(instance.isCtrlEnabled());
        assertTrue(instance.isCtrlEnabled());
        
        result.setCtrl(false);
        
        // shift disabled
        assertFalse(instance.isShiftEnabled());
        assertTrue(instance.isCtrlEnabled());
        assertTrue(instance.isAltEnable());
        
        result.setCtrl(true);
        result.setShift(false);
        
        // ctrl disabled
        assertTrue(instance.isShiftEnabled());
        assertFalse(instance.isCtrlEnabled());
        assertTrue(instance.isAltEnable());
        
        result.setAlt(true);
        result.setCtrl(false);
        
        // alt disabled
        assertTrue(instance.isShiftEnabled());
        assertTrue(instance.isCtrlEnabled());
        assertFalse(instance.isAltEnable());
        
        result.setCtrl(true);
        
        // all good
        assertTrue(instance.isShiftEnabled());
        assertTrue(instance.isCtrlEnabled());
        assertTrue(instance.isAltEnable());
        
    }

}
