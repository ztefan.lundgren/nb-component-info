package se.softstuff.nb.bundleeditor.editor;

import java.util.Locale;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author stefan
 */
public class ComponentEditorPresentationModelTest {
    
    private ComponentEditorPresentationModel instance;
    @Before
    public void setUp() {
        instance = new ComponentEditorPresentationModel();
    }

    @Test
    public void testCreateResourceKey() {
        String expected = "se.softstuff.nb.corecomponent.api.Bundle_soft_sv_SE.properties";
        String resourceBaseName = "se.softstuff.nb.corecomponent.api.Bundle";
        String brandingToken = "soft";
        Locale locale = new Locale("sv", "SE");
        String result = instance.createResourceKey(resourceBaseName, brandingToken, locale);
        assertEquals(expected, result);
        
        brandingToken = null;
        expected = "se.softstuff.nb.corecomponent.api.Bundle_sv_SE.properties";
        result = instance.createResourceKey(resourceBaseName, brandingToken, locale);
        assertEquals(expected, result);
        
        locale = null;
        expected = "se.softstuff.nb.corecomponent.api.Bundle.properties";
        result = instance.createResourceKey(resourceBaseName, brandingToken, locale);
        assertEquals(expected, result);
    }
}
