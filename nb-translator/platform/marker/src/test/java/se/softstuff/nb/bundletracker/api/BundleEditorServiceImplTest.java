/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.bundletracker.api;

import org.junit.*;

/**
 *
 * @author Stefan
 */
public class BundleEditorServiceImplTest {

    BundleEditorServiceImpl instance;
    public BundleEditorServiceImplTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        instance = new BundleEditorServiceImpl();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of isEditable method, of class BundleEditorServiceImpl.
     */
//    @Test
//    public void testIsEditable() {
//        System.out.println("isEditable");
//        BundleEditorServiceImpl instance = new BundleEditorServiceImpl();
//        boolean expResult = false;
//        boolean result = instance.isEditable();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of setValue method, of class BundleEditorServiceImpl.
     */
//    @Test
//    public void testSetValue() {
//        System.out.println("setValue");
//        BundleRowNode row = null;
//        Locale language = null;
//        String newValue = "";
//        BundleEditorServiceImpl instance = new BundleEditorServiceImpl();
//        String expResult = "";
//        String result = instance.setValue(row, language, newValue);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
    /**
     * Test of createJarArchive method, of class BundleEditorServiceImpl.
     */
    @Test
    public void testCreateJarArchive() {
//        System.out.println("createJarArchive");
//        File archiveFile = new File("c:/Temp/my.jar");
//        File[] tobeJared = new File[]{new File("c:/Temp/fil1.txt")};        
//        instance.createJarArchive(archiveFile, tobeJared);
    }

//    @Test
//    public void findBundleInputStream() throws IOException {
//        JarFile jar = new JarFile("c:/Temp/se-softstuff-nb-GuiComponentInfoDemo-TopComponet.jar");
//        String resourse = "se/softstuff/nb/guicomponentinfodemo";        
//        Locale lang = new Locale("sv", "SE");
//        
//        InputStream input = instance.findBundleInputStream(jar,resourse,lang);
//        
//        assertNotNull(input);
//        
//    }
    
    
}
