/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.bundletracker.api;

import java.util.Locale;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Stefan
 */
public class BundleRowEngineTest {
    
    
    
    @Before
    public void setUp() {
    }
    
//
//    /**
//     * Test of getBranding method, of class BundleRowEngine.
//     */
//    @Test
//    public void testGetBranding() {
//        System.out.println("getBranding");
//        int columnIndex = 0;
//        BundleRowEngine instance = null;
//        String expResult = "";
//        String result = instance.getBranding(columnIndex);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of getLocale method, of class BundleRowEngine.
//     */
//    @Test
//    public void testGetLocale() {
//        System.out.println("getLocale");
//        int columnIndex = 0;
//        BundleRowEngine instance = null;
//        Locale expResult = null;
//        Locale result = instance.getLocale(columnIndex);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }

    private static final Locale DEFAULT_LOCALE = new Locale("");
    /**
     * Test of buildLocale method, of class BundleRowEngine.
     */
    @Test
    public void testBuildLocale() {
        System.out.println("buildLocale");
        String[] part = null;
        int beginIndex = 0;
        Locale expResult = DEFAULT_LOCALE;
        Locale result = BundleRowEngine.buildLocale(part, beginIndex);
        assertEquals(expResult, result);
        
        part = new String[]{""};
        beginIndex = 0;
        result = BundleRowEngine.buildLocale(part, beginIndex);
        assertEquals(expResult, result);
        
        part = new String[]{"gurka","se", "SV"};
        beginIndex = 1;
        expResult = new Locale("se", "SV");
        result = BundleRowEngine.buildLocale(part, beginIndex);
        assertEquals(expResult, result);
        
        part = new String[]{"se", "SV"};
        beginIndex = 0;
        expResult = new Locale("se", "SV");
        result = BundleRowEngine.buildLocale(part, beginIndex);
        assertEquals(expResult, result);
        
        part = new String[]{"gurka","se"};
        beginIndex = 1;
        expResult = new Locale("se");
        result = BundleRowEngine.buildLocale(part, beginIndex);
        assertEquals(expResult, result);
        
        part = new String[]{"se"};
        beginIndex = 0;
        expResult = new Locale("se");
        result = BundleRowEngine.buildLocale(part, beginIndex);
        assertEquals(expResult, result);
        
        
    }
}
