@ContainerRegistration(id = "OptionPanel", categoryName = "#OptionsCategory_Name_OptionPanel", iconBase = "se/softstuff/nb/bundletracker/<none>", keywords = "#OptionsCategory_Keywords_OptionPanel", keywordsCategory = "OptionPanel")
package se.softstuff.nb.bundletracker;

import org.netbeans.spi.options.OptionsPanelController.ContainerRegistration;
