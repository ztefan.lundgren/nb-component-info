package se.softstuff.nb.guicomponentparentstack;

import java.awt.Component;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import se.softstuff.nb.domain.tracer.HighlightedComponent;

/**
 * 
 * @author stefan
 */
public class TraceListCellRenderer extends JLabel implements ListCellRenderer {
    @Override
    public Component getListCellRendererComponent(
            JList list, // the list
            Object value, // value to display
            int index, // cell index
            boolean isSelected, // is the cell selected
            boolean cellHasFocus) // does the cell have focus
    {
        HighlightedComponent highlighted = (HighlightedComponent) value;
        Component component = highlighted.getComponent();
        setText(String.format("%s - %s.", component.getName(), component.getClass().getName())); //NOI18N
        
        setForeground(list.getForeground());
        if (isSelected) {
            setBackground(highlighted.getColor());
        } else {
            setBackground(list.getBackground());
        }
        
        setBorder(BorderFactory.createLineBorder(highlighted.getColor()));
        setEnabled(list.isEnabled());
        setFont(list.getFont());
        setOpaque(true);
        return this;
    }
}
