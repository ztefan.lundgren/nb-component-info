/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.bundletracker.api;

import java.util.*;
import se.softstuff.nb.domain.tracer.MarkerUtilities;

/**
 *
 * @author Stefan
 */
public class BundleRowEngine {
    
    private static final Locale DEFAULT_LOCALE = new Locale("");
    
    private List<String> headers;
    private Map<String, String> brandings;
    private Map<String, Locale> languages;
    public BundleRowEngine(MarkerUtilities.BundlePath bundlePath) {
        headers = new ArrayList<String>(bundlePath.bundleTypes);
        brandings = new HashMap<String, String>();
        languages = new HashMap<String, Locale>();
        init();
    }

    public String getBranding(String header) {
        String branding = brandings.get(header);
        return branding;
    }

    public Locale getLocale(String header) {
        Locale lang = languages.get(header);
        return lang;
    }

    private void init() {
        for (String header : headers) {
            String[] part = header.split("_");
            if(part.length>0){
                if(part[0].length()> 2){
                    brandings.put(header, part[0]);
                    Locale lang = buildLocale(part, 1);
                    languages.put(header, lang);
                } else {
                    Locale lang = buildLocale(part, 0);
                    languages.put(header, lang);
                }
            }
        }
    }

    static Locale buildLocale(String[] part, int beginIndex) {
        Locale result = null;
        if(part == null || part.length - beginIndex == 0){
            result = DEFAULT_LOCALE;
        } else if(part.length - beginIndex == 1){
            result = new Locale(part[beginIndex]);
        } else if(part.length - beginIndex == 2){
            result = new Locale(part[beginIndex], part[beginIndex+1]);
        } else if(part.length - beginIndex >= 3){
            result = new Locale(part[beginIndex], part[beginIndex+1], part[beginIndex+2]);
        }
        return result;
    }
    
    
    
}
