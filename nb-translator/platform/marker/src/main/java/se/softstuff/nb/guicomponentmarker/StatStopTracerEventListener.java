package se.softstuff.nb.guicomponentmarker;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.event.*;
import javax.swing.JLayeredPane;
import javax.swing.JRootPane;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import org.openide.util.Lookup;
import se.softstuff.nb.domain.swing.GlassUtil;
import se.softstuff.nb.domain.tracer.TracerTriggerKeys;
import se.softstuff.nb.domain.tracer.MarkerUtilities;

/**
 *
 * @author Stefan
 */
public class StatStopTracerEventListener implements AWTEventListener {

    private TracerTriggerKeys startTrigger;

    private final TracerManagerImpl tracer;
//    private final Logger log = Logger.getLogger(getClass());

    public StatStopTracerEventListener(TracerManagerImpl tracer) {

        this.tracer = tracer;
    }

    public void setStartTrigger(TracerTriggerKeys startTrigger) {
        this.startTrigger = startTrigger;
    }

    @Override
    public void eventDispatched(AWTEvent event) {

        if (!(event instanceof MouseEvent)) {
            return;
        }

        MouseEvent mouseEvent = (MouseEvent) event;

//        log.info(" event:" + event);
        if (event.getID() == MouseEvent.MOUSE_PRESSED) {
            // fist trigger click - Start tracker and highlighter
            // second click - pause highlighter
            // third click - stop  tracker and highlighter

//           log.debug("triggerd tracking: , mouseEvent:"+mouseEvent);

            if (isStartTriggeredBy(mouseEvent)) {
                if (tracer.isTrackerEnabled()) {
                    tracer.stopTracker();
                } else {
                    tracer.startTrackerAndMark(mouseEvent.getComponent());
                    mouseEvent.consume();
                }
                
            } else if (tracer.isTrackerEnabled() && !isDontMarkThisComponent(mouseEvent)) {

                boolean consume = startTrigger.fireMarkedAndClicked(mouseEvent);
//                if (tracer.isHighlighterPaused()) {
//                    tracer.startTracker();
//                } else {
//                    tracer.pauseHighlighter();
//                }
                if(consume){
                    mouseEvent.consume();
                }
            }

        }

    }

    private boolean isStartTriggeredBy(MouseEvent mouseEvent) {
        if (startTrigger == null) {
            return false;
        }
        return startTrigger.isTriggeredBy(mouseEvent);
    }

    

    private boolean isDontMarkThisComponent(MouseEvent mouseEvent) {
        GlassUtil glassUtil = Lookup.getDefault().lookup(GlassUtil.class);
        Component comp = glassUtil.findComponentUnderGlassPaneAt(mouseEvent);
        if(MarkerUtilities.isDontMarkThisComponent(comp)){
            return true;
        }
        return false;
    }

    
    
}
