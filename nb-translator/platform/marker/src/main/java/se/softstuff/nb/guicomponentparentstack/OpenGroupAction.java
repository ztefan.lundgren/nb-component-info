package se.softstuff.nb.guicomponentparentstack;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.windows.TopComponentGroup;
import org.openide.windows.WindowManager;

public final class OpenGroupAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        TopComponentGroup group = WindowManager.getDefault().findTopComponentGroup("MyGroup"); //NOI18N
        if (group == null) {
            return;
        }
        group.open();
    }
}
