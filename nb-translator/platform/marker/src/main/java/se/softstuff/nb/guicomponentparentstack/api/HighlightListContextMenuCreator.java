package se.softstuff.nb.guicomponentparentstack.api;

import javax.swing.JPopupMenu;
import se.softstuff.nb.domain.tracer.HighlightedComponent;

/**
 * Extension point for adding new functionality to the context menu in the top component stack list
 * 
 * @author stefan
 */
public interface HighlightListContextMenuCreator {

    /**
     * Extension point for adding menu items to the context menu
     * @param contextMenu Add your items to this
     * @param rowSelected
     */
    public void appendContextMenuItem(JPopupMenu contextMenu, HighlightedComponent rowSelected);

}
