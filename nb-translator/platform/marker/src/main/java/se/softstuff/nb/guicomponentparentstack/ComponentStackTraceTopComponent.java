package se.softstuff.nb.guicomponentparentstack;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Iterator;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import org.netbeans.api.settings.ConvertAsProperties;
import org.openide.util.Lookup;
import org.openide.util.LookupListener;
import se.softstuff.nb.domain.tracer.DontMarkThisComponent;
import se.softstuff.nb.domain.tracer.HighlightedComponent;
import se.softstuff.nb.domain.tracer.TracerManager;
import se.softstuff.nb.guicomponentparentstack.api.HighlightListContextMenuCreator;

/**
 * Top component which displays the current components parent stack relations 
 */
@ConvertAsProperties(dtd = "-//se.softstuff.nb.guicomponentparentstack//ComponentStackTrace//EN", //NOI18N
autostore = false)
public final class ComponentStackTraceTopComponent extends TopComponent implements LookupListener, DontMarkThisComponent {

    private static ComponentStackTraceTopComponent instance;

    private static final Logger LOG = Logger.getLogger(ComponentStackTraceTopComponent.class.getName());

    public static final String PREFERRED_ID = "ComponentStackTraceTopComponent"; //NOI18N

    private DefaultListModel listModel;

    private Result<HighlightedComponent> lookupResult;

    private TracerManager manager;

    private boolean ignoreUpdate;

    private boolean inDrag;
    
    private HighlightedComponent markedComponent;

    // keeps track of the stack list selection
    private ListSelectionListener listSelectionHandler = new ListSelectionListener() {

        @Override
        public void valueChanged(ListSelectionEvent e) {
            HighlightedComponent item = (HighlightedComponent) stackList.getSelectedValue();
            if (item != null && manager.isTrackerEnabled()) {
                if(!inDrag) {
                    ignoreUpdate = true; // dont update component list on own lookup event when list selection is changed
                    manager.lit(item.getComponent(), item.getColor());
                    ignoreUpdate = false;
                } else {
                    stackList.setSelectedValue(markedComponent,true);
                }
            }
        }
    };

    // listen for manager events
    private PropertyChangeListener trackerCheckBoxSyncronizer = new PropertyChangeListener() {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (evt.getPropertyName().equals(TracerManager.PROP_TRACKER_ENABLED)) {
                trackerCheckBox.setSelected(manager.isTrackerEnabled());
            }
        }
    };

    // add menu items dynamic from the extention point HighlightContextMenuCreator
    private MouseAdapter popupListener = new MouseAdapter() {

        @Override
        public void mouseDragged(MouseEvent e) {
            inDrag = true;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            mouseReleased(e);
        }

        @Override
        public void mouseReleased(MouseEvent e) {

            if (e.isPopupTrigger()) {
                int locationOfMouse = stackList.locationToIndex(e.getPoint());
                if (locationOfMouse < 0) {
                    return;
                }

                stackList.setSelectedIndex(locationOfMouse);
                HighlightedComponent selectedValue = (HighlightedComponent) stackList.getSelectedValue();

                Component comp = e.getComponent();
                listPopupMenu.removeAll();

                for (HighlightListContextMenuCreator creator : Lookup.getDefault().lookupAll(HighlightListContextMenuCreator.class)) {
                    creator.appendContextMenuItem(listPopupMenu, selectedValue);
                }
                listPopupMenu.show(comp, e.getX(), e.getY());
            }
            inDrag = false;
        }
    };

    public ComponentStackTraceTopComponent() {

        listModel = new DefaultListModel();
        initComponents();
        setName(NbBundle.getMessage(ComponentStackTraceTopComponent.class, "CTL_ComponentStackTraceTopComponent")); //NOI18N
        setToolTipText(NbBundle.getMessage(ComponentStackTraceTopComponent.class, "HINT_ComponentStackTraceTopComponent")); //NOI18N
        stackList.addListSelectionListener(listSelectionHandler);

        manager = (TracerManager) Lookup.getDefault().lookup(TracerManager.class);
        associateLookup(manager.getLookup());

        // add the fancy global shortcut key to the checkbox lable
        trackerCheckBox.setText(NbBundle.getMessage(ComponentStackTraceTopComponent.class,
                "ComponentStackTraceTopComponent.trackerCheckBox.text", //NOI18N
                manager.getShortcutKeyDescription()));
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        listPopupMenu = new javax.swing.JPopupMenu();
        jScrollPane1 = new javax.swing.JScrollPane();
        stackList = new javax.swing.JList();
        trackerCheckBox = new javax.swing.JCheckBox();

        listPopupMenu.setName("listPopupMenu"); // NOI18N

        setName("Form"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        stackList.setModel(listModel);
        stackList.setCellRenderer(new TraceListCellRenderer());
        stackList.setName("stackList"); // NOI18N
        jScrollPane1.setViewportView(stackList);

        org.openide.awt.Mnemonics.setLocalizedText(trackerCheckBox, NbBundle.getMessage(ComponentStackTraceTopComponent.class, "ComponentStackTraceTopComponent.trackerCheckBox.text")); // NOI18N
        trackerCheckBox.setName("trackerCheckBox"); // NOI18N
        trackerCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                trackerCheckBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(trackerCheckBox, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 278, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 347, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(trackerCheckBox))
        );
    }// </editor-fold>//GEN-END:initComponents

    // <editor-fold defaultstate="collapsed" desc="Singleton implementation">
    /**
     * Gets default instance. Do not use directly: reserved for *.settings files only,
     * i.e. deserialization routines; otherwise you could get a non-deserialized instance.
     * To obtain the singleton instance, use {@link #findInstance}.
     */
    public static synchronized ComponentStackTraceTopComponent getDefault() {
        if (instance == null) {
            instance = new ComponentStackTraceTopComponent();
        }
        return instance;
    }

    /**
     * Obtain the ComponentStackTraceTopComponent instance. Never call {@link #getDefault} directly!
     */
    public static synchronized ComponentStackTraceTopComponent findInstance() {
        TopComponent win = WindowManager.getDefault().findTopComponent(PREFERRED_ID);
        if (win == null) {
            Logger.getLogger(ComponentStackTraceTopComponent.class.getName()).warning(
                    "Cannot find " + PREFERRED_ID + " component. It will not be located properly in the window system.");
            return getDefault();
        }
        if (win instanceof ComponentStackTraceTopComponent) {
            return (ComponentStackTraceTopComponent) win;
        }
        Logger.getLogger(ComponentStackTraceTopComponent.class.getName()).warning(
                "There seem to be multiple components with the '" + PREFERRED_ID
                + "' ID. That is a potential source of errors and unexpected behavior.");
        return getDefault();
    }

    // </editor-fold>
    private void trackerCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_trackerCheckBoxActionPerformed

        manager.toggleTracker();

    }//GEN-LAST:event_trackerCheckBoxActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPopupMenu listPopupMenu;
    private javax.swing.JList stackList;
    private javax.swing.JCheckBox trackerCheckBox;
    // End of variables declaration//GEN-END:variables

    @Override
    public int getPersistenceType() {
        return TopComponent.PERSISTENCE_ALWAYS;
    }

    @Override
    public void componentOpened() {
        LOG.fine("ComponentStackTraceTopComponent.componentOpened"); //NOI18N


        manager.addPropertyChangeListener(trackerCheckBoxSyncronizer);
        trackerCheckBox.setSelected(manager.isTrackerEnabled());


        // start listen for lookup result updates
        lookupResult = manager.getLookup().lookupResult(HighlightedComponent.class);
        lookupResult.addLookupListener(this);

        // fill component stack on startup if some component is alredy highlighted
        fillStack();



        stackList.addMouseListener(popupListener);
        stackList.addMouseMotionListener(popupListener);
    }

    @Override
    public void componentClosed() {
        LOG.fine("ComponentStackTraceTopComponent.componentClosed"); //NOI18N
        lookupResult = null;
        manager.removePropertyChangeListener(trackerCheckBoxSyncronizer);
        stackList.removeMouseListener(popupListener);
        stackList.removeMouseMotionListener(popupListener);
    }

    @Override
    public void resultChanged(LookupEvent ev) {
        if (!ignoreUpdate && lookupResult != null) { // dont update while we changed list selection 
            fillStack();
        }
    }

    /**
     * finds and filles the lis with fancy components
     */
    private void fillStack() {
        Iterator<? extends HighlightedComponent> iterator = lookupResult.allInstances().iterator();

        listModel.clear();
        if (iterator.hasNext()) { // take the first
            HighlightedComponent newComponent = iterator.next();
            markedComponent = newComponent;
            while (newComponent != null) {
                listModel.addElement(newComponent);
                newComponent = newComponent.getParent();
            }
        }
        repaint();
    }

    void writeProperties(java.util.Properties p) {
    }

    Object readProperties(java.util.Properties p) {
        if (instance == null) {
            instance = this;
        }
        instance.readPropertiesImpl(p);
        return instance;
    }

    private void readPropertiesImpl(java.util.Properties p) {
    }

    @Override
    protected String preferredID() {
        return PREFERRED_ID;
    }
}
