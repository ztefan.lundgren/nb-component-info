package se.softstuff.nb.guicomponentmarker;

import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.SwingUtilities;
import org.openide.windows.TopComponent;
import se.softstuff.nb.domain.tracer.DontMarkThisComponent;
import se.softstuff.nb.domain.tracer.MarkerUtilities;

/**
 * Stolen an modyfied sample
 * 
 * (Based on code from the Java Tutorial) We must forward at least the mouse
 * drags that started with mouse presses over the check box. Otherwise, when
 * the user presses the check box then drags off, the check box isn't
 * disarmed -- it keeps its dark gray background or whatever its L&F uses to
 * indicate that the button is currently being pressed.
 */
public class MouseEventRedispatcher implements MouseListener, MouseMotionListener {

    private final JPanel glassPane;

    private JMenuBar menuBar;

    private Container contentPane;

    private boolean inDrag = false;
    
    // trigger for redispatching (allows external control)
    private boolean needToRedispatch = true;

    public MouseEventRedispatcher(JPanel glassPane, JRootPane rootPane) {
        this.glassPane = glassPane;
        setRootPane(rootPane);
    }
    
    public void setRootPane(JRootPane rootPane) {
        menuBar = rootPane.getJMenuBar();
        contentPane = rootPane.getContentPane();
    }
       
    
    // We only need to redispatch if we're not visible, but having full control
    // over this might prove handy.

    public void setNeedToRedispatch(boolean need) {
        needToRedispatch = need;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (isToBeRedispatched(e)) {
            redispatchMouseEvent(e);
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        if (isToBeRedispatched(e)) {
            redispatchMouseEvent(e);
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (isToBeRedispatched(e)) {
            redispatchMouseEvent(e);
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if (isToBeRedispatched(e)) {
            redispatchMouseEvent(e);
        }
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if (isToBeRedispatched(e)) {
            redispatchMouseEvent(e);
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (isToBeRedispatched(e)) {
            redispatchMouseEvent(e);
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (isToBeRedispatched(e)) {
            redispatchMouseEvent(e);
            inDrag = false;
        }
    }
    
    private boolean isToBeRedispatched(MouseEvent e){
        
        Component component = getComponentUnderGlass(e);
        return MarkerUtilities.isDontMarkThisComponent(component);
        
//        TopComponent tc = findTopComponentAt(e.getPoint());
//        if(needToRedispatch || tc!=null && tc instanceof DontMarkThisComponent) {
//            return true;
//        }
//        
//        return needToRedispatch;
    }

    private void redispatchMouseEvent(MouseEvent e) {

        boolean inButton = false;
        boolean inMenuBar = false;
        Point glassPanePoint = e.getPoint();
        Component component = null;
        Container container = contentPane;
        Point containerPoint = SwingUtilities.convertPoint(glassPane,
                glassPanePoint, contentPane);                                                                                                              //
        int eventID = e.getID();

        if (containerPoint.y < 0) {
            e.consume();
            return; // giving up trying to get mouse event working in side menu bar items
//      inMenuBar = true;
//      container = menuBar;
//      containerPoint = SwingUtilities.convertPoint(this, glassPanePoint,
//          menuBar);
//      testForDrag(eventID);
        }

        //XXX: If the event is from a component in a popped-up menu,
        //XXX: then the container should probably be the menu's
        //XXX: JPopupMenu, and containerPoint should be adjusted
        //XXX: accordingly.
        component = SwingUtilities.getDeepestComponentAt(container,
                containerPoint.x, containerPoint.y);

        if (component == null) {
            return;
        } else {
            inButton = true;
            testForDrag(eventID);
        }

        if (inMenuBar || inButton || inDrag) {
            Point componentPoint = SwingUtilities.convertPoint(glassPane,
                    glassPanePoint, component);
            component.dispatchEvent(new MouseEvent(component, eventID, e.getWhen(), e.getModifiers(), componentPoint.x,
                    componentPoint.y, e.getClickCount(), e.isPopupTrigger()));
        }
    }

    private void testForDrag(int eventID) {
        if (eventID == MouseEvent.MOUSE_PRESSED) {
            inDrag = true;
        }
    }
    
    public TopComponent findTopComponentAt(Point glasspoint) {
        
        Point containerPoint = SwingUtilities.convertPoint(
                glassPane,
                glasspoint,
                contentPane);
        
        Component comp = SwingUtilities.getDeepestComponentAt(
                    contentPane,
                    containerPoint.x,
                    containerPoint.y);
        
        while(comp!=null){
            if(comp instanceof TopComponent) {
                return (TopComponent)comp;
            }
            comp = comp.getParent();
        }
        return null;

    }

    private Component getComponentUnderGlass(MouseEvent e) {
        Point containerPoint = SwingUtilities.convertPoint(
                e.getComponent(),
                e.getPoint(),
                contentPane);
        
        Component comp = SwingUtilities.getDeepestComponentAt(
                    contentPane,
                    containerPoint.x,
                    containerPoint.y);
        return comp;
    }
}
