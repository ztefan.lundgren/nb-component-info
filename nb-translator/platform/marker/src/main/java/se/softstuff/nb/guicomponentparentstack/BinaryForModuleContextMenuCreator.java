package se.softstuff.nb.guicomponentparentstack;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import org.openide.util.lookup.ServiceProvider;
import se.softstuff.nb.domain.tracer.HighlightedComponent;
import se.softstuff.nb.domain.tracer.MarkerUtilities;
import se.softstuff.nb.guicomponentparentstack.api.HighlightListContextMenuCreator;

/**
 * Shows the binary that the row.component is stored in
 * 
 * @author stefan
 */
@ServiceProvider(service = HighlightListContextMenuCreator.class)
public class BinaryForModuleContextMenuCreator implements HighlightListContextMenuCreator {

    @Override
    public void appendContextMenuItem(JPopupMenu contextMenu, HighlightedComponent row) {
        try {
            String module = MarkerUtilities.findJarFileName(row.getComponent().getClass());
            contextMenu.add(new JMenuItem("Binary: " + module)); //NOI18N
        } catch (Exception ignore) {
        }
    }
}
