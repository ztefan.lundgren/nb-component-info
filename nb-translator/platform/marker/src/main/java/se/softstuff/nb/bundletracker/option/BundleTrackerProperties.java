package se.softstuff.nb.bundletracker.option;

import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Locale;

/**
 * Service provider interface to work with the settings
 * @author stefan
 */
public interface BundleTrackerProperties {
    
    public static final String PROP_SELECTED_LANGUAGES = "selectedLanguages";// NOI18N

    
    List<Locale> getSelectedLanguages();
    
    void setSelecteLanguages(List<Locale> languages);

    void addPropertyChangeListener(PropertyChangeListener l);

    void removePropertyChangeListener(PropertyChangeListener l);

}
