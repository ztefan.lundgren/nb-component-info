package se.softstuff.nb.guicomponentmarker;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.util.Lookup;
import se.softstuff.nb.domain.tracer.TracerManager;

/**
 * Toggles the mouse tracer (on and off)
 * @author stefan
 */
public final class MouseTracerAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        TracerManager manager = (TracerManager) Lookup.getDefault().lookup(TracerManager.class);
        manager.toggleTracker();
    }
}
