package se.softstuff.nb.guicomponentmarker;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import org.apache.log4j.Logger;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.Utilities;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;
import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.WindowManager;
import se.softstuff.nb.domain.tracer.HighlightedComponent;
import se.softstuff.nb.domain.tracer.TracerManager;
import se.softstuff.nb.domain.tracer.TracerTriggerKeys;

/**
 * Start and stops the tracer. Keeps the lookup used to send all component
 * updates
 *
 * @author stefan
 */
@ServiceProvider(service = TracerManager.class)
public class TracerManagerImpl implements TracerManager {

    static final String MENU = "Menu/"; //NOI18N
    static final String TOOLS_MENU = MENU + "Tools/"; //NOI18N
    static final String WINDOWS_MENU = MENU + "Window/"; //NOI18N
    static final String WINDOWS_MENU_GROUP_NAME = "GUI Component info/"; //NOI18N
    static final String HIDDEN_MENU = "_hidden"; //NOI18N
    private final PropertyChangeSupport propertyChangeSupport;
    private final Lookup lookup;
    private final InstanceContent ic;
    private final List<Color> colorStack;
    private transient boolean trackerEnabled;
    private HighlighterGlass highlighter;
    private Result<HighlightedComponent> lookupResult;
    Set<Class> excluded = new HashSet<Class>();
    private boolean paused;
    private final Logger log = Logger.getLogger(getClass());
    private StatStopTracerEventListener statStopListener;

    public TracerManagerImpl() {
        log.debug("TracerManagerImpl()");
        propertyChangeSupport = new PropertyChangeSupport(this);
        colorStack = new ArrayList(Arrays.asList(Color.RED, Color.YELLOW, Color.PINK, Color.CYAN, Color.MAGENTA, Color.BLUE, Color.GREEN));

        ic = new InstanceContent();
        lookup = new AbstractLookup(ic);
        lookupResult = lookup.lookupResult(HighlightedComponent.class);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public Lookup getLookup() {
        return lookup;
    }

    @Override
    public boolean isTrackerEnabled() {
        return trackerEnabled;
    }

    private void setTracerEnabled(boolean enabled) {
        paused = false;
        propertyChangeSupport.firePropertyChange(PROP_TRACKER_ENABLED, trackerEnabled, trackerEnabled = enabled);
    }

    /**
     * starts tracker if stopped or stops it if its started
     */
    @Override
    public synchronized void toggleTracker() {
        if (isTrackerEnabled()) {
            stopTracker();
        } else {
            startTracker();
        }
    }

    @Override
    public synchronized void startTracker() {
        startTrackerAndMark(null);
    }
    @Override
    public synchronized void startTrackerAndMark(final Component comp) {
        paused = false;
        if (trackerEnabled) {
            return; //Tracker is alredy started
        }
        setTracerEnabled(true);


        WindowManager.getDefault().invokeWhenUIReady(new Runnable() {

            @Override
            public void run() {

                log.debug(String.format("Start component tracer")); // NOI18N

                highlighter = new HighlighterGlass();

                highlighter.start();

                lookupResult.addLookupListener(highlighter);

                if(comp!=null){
                    lit(comp, colorStack.get(0));
                }
                WindowManager.getDefault().updateUI();

            }
        });
    }
    
    @Override
    public boolean isHighlighterPaused(){
        return paused;
    }
    
    @Override
    public synchronized void toggleHighlighter() {
        paused = !paused;
    }

    @Override
    public synchronized void pauseHighlighter() {
        paused = true;
    }

    @Override
    public synchronized void resumeHighlighter() {
        paused = false;
    }

    @Override
    public synchronized void stopTracker() {

        if (!trackerEnabled) {
            return;
        }
        log.debug("Stop component tracer"); // NOI18N
        setTracerEnabled(false);

        lookupResult.removeLookupListener(highlighter);

        highlighter.stop();

    }

    /**
     * Creates and puts the HiglightedComponent in the lookup
     *
     * @param color Handle missing or null color by ignoring and put first color
     * insted
     */
    @Override
    public synchronized void lit(Component component, Color color) {
        if (!isTrackerEnabled() || paused) {
            return;
        }
        int index = color != null ? colorStack.indexOf(color) : 0;
        if (index < 0) {
            index = 0; // ignore missing color
        }
        ListIterator<Color> listIterator = colorStack.listIterator(index);
        HighlightedComponent item = HighlightedComponent.create(component, listIterator);
        if (item != null) {

            // Add the new item to the lookup
            ic.set(Arrays.asList(item), null);
        }
    }

    @Override
    public String getShortcutKeyDescription() {
        return findOpenActionAccelerator(MouseTracerAction.class);
    }

    // <editor-fold defaultstate="collapsed" desc="find the action accelerator keys">
    private static String findOpenActionAccelerator(Class action) {
        String clazz = action.getName().replaceAll("\\.", "-") + ".instance"; // NOI18N
        KeyStroke stroke = findKeyStrokeForShortcut(clazz);
        return getAcceleratorText(stroke);
    }

    private static KeyStroke findKeyStrokeForShortcut(String clazz) {
        for (FileObject key : FileUtil.getConfigFile("Shortcuts").getChildren()) { // NOI18N
            Object action = key.getAttribute("originalFile"); // NOI18N
            if (action instanceof String) {
                if (((String) action).endsWith(clazz)) {
                    String keycode = key.getName();
                    KeyStroke stroke = Utilities.stringToKey(keycode);
                    return stroke;
                }
            }
        }
        return null;
    }

    /**
     * converts KeyStroke to readable string format. this can be used to show
     * the accelerator along with the tooltip for toolbar buttons
     *
     * this code is extracted from javax.swing.plaf.basic.BasicMenuItemUI class
     */
    private static String getAcceleratorText(KeyStroke accelerator) {
        if (accelerator == null) {
            return "";// NOI18N
        }
        String acceleratorDelimiter = UIManager.getString("MenuItem.acceleratorDelimiter");// NOI18N
        if (acceleratorDelimiter == null) {
            acceleratorDelimiter = "+";// NOI18N
        }

        String acceleratorText = "";// NOI18N
        if (accelerator != null) {
            int modifiers = accelerator.getModifiers();
            if (modifiers > 0) {
                acceleratorText = KeyEvent.getKeyModifiersText(modifiers);
                acceleratorText += acceleratorDelimiter;
            }

            int keyCode = accelerator.getKeyCode();
            if (keyCode != 0) {
                acceleratorText += KeyEvent.getKeyText(keyCode);
            } else {
                acceleratorText += accelerator.getKeyChar();
            }
        }
        return acceleratorText;
    }
    // </editor-fold>

    public boolean isLoaded() {

        FileObject guiComponent = FileUtil.getConfigFile(TOOLS_MENU + WINDOWS_MENU_GROUP_NAME);
        return guiComponent != null;
    }

    String createShadow(String id) {
        id = id.replace(".", "-"); // NOI18N
        return id + ".shadow"; // NOI18N
    }

    @Override
    public void unload() {

        stopTracker();

        unload(WINDOWS_MENU_GROUP_NAME, FileUtil.getConfigFile(TOOLS_MENU));

    }

    private boolean unload(String preferredId, FileObject menu) {
        if (menu == null) {
            return true;
        }
        if (preferredId.endsWith("/")) {
            preferredId = preferredId.substring(0, preferredId.length() - 1);
        }

        if (menu.getPath().contains(preferredId)) {
            if (!menu.getExt().endsWith(HIDDEN_MENU)) {
                rename(menu, true);
            }
            return true;
        }
        for (FileObject child : menu.getChildren()) {
            if (unload(preferredId, child)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void load() {
        try {
            load(WINDOWS_MENU_GROUP_NAME, FileUtil.getConfigFile(TOOLS_MENU));

        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    private boolean load(String preferredId, FileObject menu) throws IOException {
        if (menu == null) {
            return true;
        }
        if (preferredId.endsWith("/")) {
            preferredId = preferredId.substring(0, preferredId.length() - 1);
        }

        File parent = FileUtil.toFile(menu);
        if (parent == null) {
            return true;
        }
        File normal = FileUtil.normalizeFile(parent);
        FileObject fileObject = FileUtil.toFileObject(new File(normal, preferredId + HIDDEN_MENU));
        if (fileObject == null) {
            return true;
        }
        rename(fileObject, false);
        menu.refresh(true);
        return false;
    }

    private void rename(FileObject file, boolean hidden) {

        if (file != null) {

            FileLock lock = null;
            try {
                lock = file.lock();
                String name = file.getName();
                String ext = null;

                if (file.isFolder() && hidden) {
                    if (!name.endsWith(HIDDEN_MENU)) {
                        name += HIDDEN_MENU;
                    }
                } else if (file.isFolder() && !hidden) {
                    if (name.endsWith(HIDDEN_MENU)) {
                        name = name.substring(0, name.length() - HIDDEN_MENU.length());
                    }
                } else {
                    ext = hidden ? "shadow_hidden" : "shadow";
                }
                file.rename(lock, name, ext);

                file.getParent().refresh(true);
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            } finally {
                if (lock != null) {
                    lock.releaseLock();
                }
            }
        }
    }

    
    @Override
    public void attachTrigger(TracerTriggerKeys startKeys) {
        log.debug("attachTrigger");
        if(statStopListener == null) {
            statStopListener = new StatStopTracerEventListener(this);
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            toolkit.addAWTEventListener(statStopListener, AWTEvent.MOUSE_EVENT_MASK);
        }
        
        statStopListener.setStartTrigger(startKeys);
    }

    @Override
    public void detatchTrigger() {
        log.debug("detatchTrigger");
        if(statStopListener != null){
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            toolkit.removeAWTEventListener(statStopListener);
        }
    }

    
}
