package se.softstuff.nb.guicomponentparentstack;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;


/**
 * Open ComponentStackTraceTopComponent it its not open else it toggle the tracker
 * 
 * @author stefan
 */
public final class OpenAction implements ActionListener {
    
    @Override
    public void actionPerformed(ActionEvent e) {
        TopComponent tc = WindowManager.getDefault().findTopComponent("ComponentStackTraceTopComponent"); //NOI18N
        if(tc!=null) {
            if(!tc.isOpened()){
                tc.open();
                tc.requestActive();
            }
        }
    }
}
