package se.softstuff.nb.bundletracker;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author stefan
 */
public class BundleTableModel extends DefaultTableModel {
    
    private final static int COLUMN_BUNDLE = 1;
    private final static int COLUMN_KEY = 2;
    
    Locale[] languages;
    int firstLanguageColun;
    Map<String, Map<String,Object>> changes = new HashMap<String, Map<String, Object>>();
    
    
    public BundleTableModel(Object[][] data, Object[] columnNames, Locale[] languages) {
        super(data, columnNames);
        this.languages = languages;
        this.firstLanguageColun = columnNames.length - languages.length;
    }
    
    
    public String getBundle(int rowIndex){
        return (String)getValueAt(rowIndex, COLUMN_BUNDLE);
    }
    
    public Locale getLanguage(int colunIndex){
        if(colunIndex<firstLanguageColun || colunIndex-firstLanguageColun > languages.length-1){
            throw new IllegalArgumentException("Column "+colunIndex+" has no language"); // NOI18N
        }
        return languages[colunIndex-firstLanguageColun];
    }
    
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex > 1;
    }

    @Override
    public void setValueAt(Object newValue, int rowIndex, int columnIndex) {
        String bundleId = createBundleId(rowIndex, columnIndex);
        Map<String, Object> bundle = changes.get(bundleId);
        if(bundle==null){
            bundle = new HashMap<String, Object>();
        }
        String key = (String)getValueAt(rowIndex, COLUMN_KEY);
        bundle.put(key, newValue);
                
        super.setValueAt(newValue, rowIndex, columnIndex);
    }
    
    public String createBundleId(int rowIndex, int columnIndex){
        String bundlePath = getBundle(rowIndex);
        Locale language = getLanguage(columnIndex);
        String bundleHash = bundlePath+language.toString();
        return bundleHash;                
    }
    
    
    public boolean isCellChanged(int rowIndex, int columnIndex){
         String bundleId = createBundleId(rowIndex, columnIndex);
        return changes.containsKey(bundleId);
    }
    
}
