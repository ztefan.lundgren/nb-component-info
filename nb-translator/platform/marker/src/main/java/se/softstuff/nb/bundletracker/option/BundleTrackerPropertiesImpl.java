package se.softstuff.nb.bundletracker.option;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import org.openide.util.NbPreferences;
import org.openide.util.lookup.ServiceProvider;

/**
 * ServiceProvider implementation
 * @author stefan
 */
@ServiceProvider(service=BundleTrackerProperties.class)
public class BundleTrackerPropertiesImpl implements BundleTrackerProperties {
    
    public static final String DELIMITER = ";"; // NOI18N
    
    private List<Locale> memory;
    
    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    @Override
    public synchronized List<Locale> getSelectedLanguages() {
        if(memory!=null){
            return memory;
        }
        memory = new ArrayList<Locale>();
        String get = NbPreferences.forModule(BundleTrackerPropertiesImpl.class).get(PROP_SELECTED_LANGUAGES, Locale.getDefault().toString());
        List<String> languages = Arrays.asList(get.split(DELIMITER));

        for (String lang : languages) {
            Locale locale = createLocale(lang.split("_")); // NOI18N
            if(locale!=null){
                memory.add(locale);                        
            }
        }
        return Collections.unmodifiableList(memory) ;
    }

    @Override
    public void setSelecteLanguages(List<Locale> languages) {
        List<Locale> old = memory;
        memory = new ArrayList<Locale>(languages);
        
        StringBuilder sb = new StringBuilder();
        for (Locale language : languages) {
            if (sb.length() > 0) {
                sb.append(DELIMITER);
            }
            sb.append(language.toString());
        }
        NbPreferences.forModule(BundleTrackerPropertiesImpl.class).put(PROP_SELECTED_LANGUAGES, sb.toString());
        
        pcs.firePropertyChange(PROP_SELECTED_LANGUAGES, memory, old);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        pcs.addPropertyChangeListener(l);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        pcs.removePropertyChangeListener(l);
    }
    
    
    private Locale createLocale(String... args) {
        if (args == null) {
            return null;
        }
        switch (args.length) {
            case 1:
                return new Locale(args[0]);
            case 2:
                return new Locale(args[0], args[1]);
            case 3:
                return new Locale(args[0], args[1], args[2]);
            default:
                return null;
        }
    }
}
