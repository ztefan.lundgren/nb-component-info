package se.softstuff.nb.guicomponentmarker;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Collection;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JPanel;
import javax.swing.JRootPane;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import org.openide.util.Lookup;
import org.openide.util.Lookup.Result;
import org.openide.util.LookupEvent;
import org.openide.util.LookupListener;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import se.softstuff.nb.domain.tracer.DontMarkThisComponent;
import se.softstuff.nb.guicomponentmarker.api.HighlightValidator;
import se.softstuff.nb.domain.tracer.HighlightedComponent;
import se.softstuff.nb.domain.tracer.TracerManager;

/**
 * This is the glasspane panel where all the fancy color frames is panted.
 * @author stefan
 */
public class HighlighterGlass extends JPanel implements LookupListener {

    /**
     * Tries to marke all component the mouse hitts
     * Makes sure that no updates is sent during mouse drag.
     * A senario is while draging a component to the bundle list component then no updates can occur.
     */
    class MouseDetector extends MouseAdapter {

        private boolean inDrag;

        @Override
        public void mouseDragged(MouseEvent e) {
            testForDrag(e.getID());
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            testForDrag(e.getID());

            if (!inDrag) {
                Point glassPoint = e.getPoint();
                litPoint(glassPoint);
            }
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            inDrag = false;
        }

        private void testForDrag(int eventID) {
            if (eventID == MouseEvent.MOUSE_PRESSED) {
                inDrag = true;
            }
        }
    }
    private final TracerManager manager;

    private final Collection<HighlightedComponent> highlights;

    private JRootPane rootPane;

    private Collection<? extends HighlightValidator> validators;

    private MouseEventRedispatcher redispatcher;

    private MouseDetector motionTracker;

    private ActiveWindowTracker activeWindowTracker;

    private Component orgGlassPane;

    private KeyListener keyAborter;

    /**
     * Initiates the glasspane dont activates it
     */
    public HighlighterGlass() {
        highlights = new ArrayList<HighlightedComponent>();

        setOpaque(false);

        manager = (TracerManager) Lookup.getDefault().lookup(TracerManager.class);
        validators = Lookup.getDefault().lookupAll(HighlightValidator.class); // set on startup for performens 

        motionTracker = new MouseDetector();
        activeWindowTracker = new ActiveWindowTracker();

        keyAborter = new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    e.consume();
                    manager.stopTracker();
                } else {
                    super.keyPressed(e);
                }
            }
        };

    }

    /**
     * Finds and validates a componet based on its position in the glasspane. 
     * If valid and new its put to the manager for creating the lookup event
     * @param glassPoint 
     */
    private void litPoint(Point glassPoint) {
        Component current = getComponentAtGlasPanePoint(glassPoint);
        TopComponent tc = findTopComponentFor(current);

        boolean isController = isDontMarkThisComponent(current);
//        if (tc != null) {
//            if (tc instanceof DontMarkThisComponent) {
//                isController = true;
//            }
//        }

        if (!isController && isValid(current)) {
            manager.lit(current, null);
            validators = Lookup.getDefault().lookupAll(HighlightValidator.class); // keep validators fresh, performens performens
        }
    }
    
    private boolean isDontMarkThisComponent(Component component){
        if(component == null){
            return false;
        } 
        if(component instanceof DontMarkThisComponent){
            return true;
        }
        return isDontMarkThisComponent(component.getParent());
    }

    /**
     * paints the marked controles with colored border and lable
     */
    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(getBackground());
        g2.fillRect(0, 0, getSize().width, getSize().height);

        synchronized (highlights) { // make it thrad safe
            for (HighlightedComponent highlight : highlights) {
                paintHighlight(g2, highlight);
            }
        }
        g2.dispose();
    }

    /**
     * paints one marked controle with colored border and lable
     */
    private void paintHighlight(Graphics2D g2, HighlightedComponent highlight) {
        if (highlight == null) {
            return;
        }
        g2.setColor(highlight.getColor());

        Component comp = highlight.getComponent();
        Container container = (Container) comp.getParent();
        Rectangle rect = SwingUtilities.convertRectangle(container, comp.getBounds(), this);
        g2.drawRect(rect.x, rect.y, rect.width, rect.height);

        String title = String.format("%s class:%s", comp.getName(), comp.getClass().getName()); //NOI18N
        FontMetrics metrics = g2.getFontMetrics();
        Rectangle titleBounds = metrics.getStringBounds(title, g2).getBounds();
        titleBounds = new Rectangle(rect.x, rect.y - (titleBounds.height + 2), titleBounds.width + 10, titleBounds.height + 2);
        g2.setColor(Color.BLACK);
        g2.fillRect(titleBounds.x, titleBounds.y, titleBounds.width, titleBounds.height);
        g2.setColor(highlight.getColor());
        g2.drawString(title, rect.x + 5, rect.y - 5);
    }

    /**
     * New values i to be found in the lookup
     */
    @Override
    public void resultChanged(LookupEvent ev) {

        if (ev.getSource() != null) {
            Result<HighlightedComponent> lookupResult = (Result<HighlightedComponent>) ev.getSource();
            synchronized (highlights) {
                highlights.clear();
                highlights.addAll(lookupResult.allInstances());
            }
        }
        repaint();
    }

    /**
     * Check if the component is valid to higlight. Using extention point HighlightValidator services
     * @param current the component to validate
     * @return the result of all validators
     */
    private boolean isValid(Component current) {
        for (HighlightValidator validator : validators) {
            if (!validator.validate(current)) {
                return false;
            }
        }
        return true;
    }

    protected Component getComponentAtGlasPanePoint(Point glassPoint) {

        Point containerPoint = SwingUtilities.convertPoint(
                rootPane,
                glassPoint,
                rootPane.getContentPane());

        Component comp = null;

        if (containerPoint.getY() < 0) {
//           // giving up trying to get mouse event working inside menu bar items
//            containerPoint = SwingUtilities.convertPoint(
//                    rootPane.getGlassPane(),
//                    glassPoint,
//                    rootPane.getJMenuBar());
//
//            comp = SwingUtilities.getDeepestComponentAt(
//                    rootPane.getJMenuBar(),
//                    containerPoint.x,
//                    containerPoint.y);
        } else {

            comp = SwingUtilities.getDeepestComponentAt(
                    rootPane.getContentPane(),
                    containerPoint.x,
                    containerPoint.y);
        }
        return comp;
    }

    /**
     * Starts painting and tracking componet for publishing on the lookup.
     * Called after the glass is attatched to the root pane
     */
    void start() {

        JFrame mainWin = (JFrame) WindowManager.getDefault().getMainWindow();
        this.rootPane = mainWin.getRootPane();
        this.orgGlassPane = mainWin.getGlassPane();
        Color base = orgGlassPane.getBackground();
        Color background = new Color(base.getRed(), base.getGreen(), base.getBlue(), 0);
        setBackground(background);


        JPanel glass = this;
        redispatcher = new MouseEventRedispatcher(glass, rootPane);
        activeWindowTracker.glueGlassPane(mainWin, glass);

        activeWindowTracker.addPropertyChangeListener(new PropertyChangeListener() {

            @Override
            public void propertyChange(PropertyChangeEvent evt) {
                if (evt.getPropertyName().equals(ActiveWindowTracker.PROP_ACTIVEWINDOW)) {
                    rootPane = activeWindowTracker.getRootPaneContainer().getRootPane();
                    redispatcher.setRootPane(rootPane);
                }
            }
        });

        addMouseMotionListener(motionTracker);
        addMouseListener(motionTracker);
        addMouseMotionListener(redispatcher);
        addMouseListener(redispatcher);
        mainWin.addKeyListener(keyAborter);


        rootPane.setGlassPane(this);
        
        setVisible(true);

        // lit at startup before first mouse move
        Point point = MouseInfo.getPointerInfo().getLocation();
        SwingUtilities.convertPointFromScreen(point, getParent());
        litPoint(point);
    }

    /** 
     * Stops tracking compnents
     */
    void stop() {

        removeMouseMotionListener(motionTracker);
        removeMouseListener(motionTracker);
        removeMouseMotionListener(redispatcher);
        removeMouseListener(redispatcher);

        activeWindowTracker.stop();

        setVisible(false);

        JFrame mainWin = (JFrame) WindowManager.getDefault().getMainWindow();
        mainWin.removeKeyListener(keyAborter);
        mainWin.getRootPane().setGlassPane(orgGlassPane);
    }

    /**
     * Tries to find a top component in the parent stack of a component
     */
    private static TopComponent findTopComponentFor(Component comp) {
        if (comp == null) {
            return null;
        } else if (comp instanceof TopComponent) {
            return (TopComponent) comp;
        }
        return findTopComponentFor(comp.getParent());
    }
     
}
