package se.softstuff.nb.bundletracker.api;

import java.util.Locale;

/**
 * Used as an extention point for a table language cell
 * @author stefan
 */
public interface BundleEditorService {
    
    /**
     * @return true if the cell is to be editable
     */
    public boolean isEditable();
    
    /**
     * Called when a cell is edited
     * @param newValue value in or from previous service provider
     * @return the result value in the cell. remember this service provider is called in a chain, only the last result counts
     */
    public String setValue(BundleRowNode row, Locale language, String newValue);
    
}
