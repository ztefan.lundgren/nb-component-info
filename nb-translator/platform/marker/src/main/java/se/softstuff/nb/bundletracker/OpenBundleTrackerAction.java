package se.softstuff.nb.bundletracker;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public final class OpenBundleTrackerAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        TopComponent tc = WindowManager.getDefault().findTopComponent("BundleTrackerTopComponent"); // NOI18N
        if (tc!=null && !tc.isOpened()) {
            tc.open();
            tc.requestActive();
        }
    }
}
