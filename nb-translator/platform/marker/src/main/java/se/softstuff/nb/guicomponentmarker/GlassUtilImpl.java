package se.softstuff.nb.guicomponentmarker;

import java.awt.Component;
import java.awt.Container;
import java.awt.Point;
import java.awt.event.MouseEvent;
import javax.swing.JRootPane;
import javax.swing.RootPaneContainer;
import javax.swing.SwingUtilities;
import org.openide.util.lookup.ServiceProvider;
import se.softstuff.nb.domain.swing.GlassUtil;

/**
 *
 * @author stefan
 */
@ServiceProvider(service=GlassUtil.class)
public class GlassUtilImpl implements GlassUtil {
    
    @Override
    public Component findComponentUnderGlassPaneAt(MouseEvent mouseEvent) {
         return findComponentUnderGlassPaneAt(mouseEvent.getComponent(), mouseEvent.getPoint());
     }
    @Override
    public Component findComponentUnderGlassPaneAt(Component top, Point onComp) {
        //        Point p = mouseEvent.getLocationOnScreen();
        
        JRootPane rootPane = SwingUtilities.getRootPane(top);
        
        Component c;

        if( top instanceof HighlighterGlass) {
            HighlighterGlass glass = (HighlighterGlass)top;
            c = glass.getComponentAtGlasPanePoint(onComp);
//            Container contentPane = rootPane.getContentPane();
//            Point convertPoint = SwingUtilities.convertPoint(top, onComp, contentPane);
//            c = contentPane.findComponentAt(convertPoint);
        } else if (top instanceof RootPaneContainer ) {
            c = ((RootPaneContainer) top).getLayeredPane().findComponentAt(
                    SwingUtilities.convertPoint(top, onComp, ((RootPaneContainer) top).getLayeredPane()));
        } else {
            c = ((Container) top).findComponentAt(onComp);
        }
        

        return c;
    }
}
