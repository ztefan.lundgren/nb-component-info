/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.bundletracker.api;

import java.io.*;
import java.util.Locale;
import java.util.jar.*;
import java.util.zip.ZipEntry;
import org.openide.util.editable.LocaleIterator;
import org.openide.util.lookup.ServiceProvider;

/**
 *
 * @author Stefan
 */
@ServiceProvider(service = BundleEditorService.class)
public class BundleEditorServiceImpl implements BundleEditorService {

    @Override
    public boolean isEditable() {
        return true;
    }

    @Override
    public String setValue(BundleRowNode row, Locale language, String newValue) {
//        EditableResourceBundle bundle = NbBundleExtended.getBundle(row.getBaseName(), language);
//        bundle.put(row.getBundleKey(), newValue);
//        bundle.persist();

//        ZipFile zip;
//        ZipEntry entry = zip.getEntry("");
//    JarFile jar;
//    jar.getJarEntry("").g
        return newValue;
    }
    // "file:/C:/work/guicomponentinfo~svn/GuiComponentInfoDemo/trunk/application/target/guicomponentinfodemo/guicomponentinfodemo/modules/se-softstuff-nb-GuiComponentInfoDemo-TopComponet.jar!/se/softstuff/nb/guicomponentinfodemo/"

//    void persist(BundleRowNode row, Locale language) {
////        try {
//            String jarFile = MarkerUtilities.findJarFile(row.getMarkedComponent());
//            String resource = MarkerUtilities.findResoursePath(row.getMarkedComponent());
//            EditableResourceBundle bundle = NbBundleExtended.getBundle(row.getBaseName(), language);
//
////            JarOutputStream out = new JarOutputStream(new FileOutputStream("C://temp"))
////            JarFile jar = new JarFile(jarFile);
////            jar.getJarEntry(jarFile).
//            
//            
//            for (String changedKey : bundle.getOrginalKeySet()) {
//                String originalValue = bundle.getOrginalString(changedKey);
//                String newValue = bundle.getString(changedKey);
//
////                InputStream inpFFFUUUCCKKut = findBundleInputStream(jar, resource, language);
////                
////                bundle.getProps().store(output);
//            }
//
//
//
//
////        } catch (IOException ex) {
////            Exceptions.printStackTrace(ex);
////        }
//
//    }

    InputStream findBundleInputStream(JarFile jar, String resource, Locale lang) throws IOException {
        String brandingToken = null;
        LocaleIterator locales = new LocaleIterator(brandingToken, lang);
        for (String token : locales) {
            String bundleFile = String.format("%s/%sBundle.properties", resource, token);
            ZipEntry entry = jar.getEntry(bundleFile);
            if (entry != null) {
                return jar.getInputStream(entry);
            }
        }
        return null;
    }
    public static int BUFFER_SIZE = 10240;

    protected void createJarArchive(File archiveFile, File[] tobeJared) {
        try {
            byte buffer[] = new byte[BUFFER_SIZE];
            // Open archive file
            FileOutputStream stream = new FileOutputStream(archiveFile);
            JarOutputStream out = new JarOutputStream(stream, new Manifest());

            for (int i = 0; i < tobeJared.length; i++) {
                if (tobeJared[i] == null || !tobeJared[i].exists()
                        || tobeJared[i].isDirectory()) {
                    continue; // Just in case...
                }
                System.out.println("Adding " + tobeJared[i].getName());

                // Add archive entry
                JarEntry jarAdd = new JarEntry(tobeJared[i].getName());
                jarAdd.setTime(tobeJared[i].lastModified());
                out.putNextEntry(jarAdd);

                // Write file to archive
                FileInputStream in = new FileInputStream(tobeJared[i]);
                while (true) {
                    int nRead = in.read(buffer, 0, buffer.length);
                    if (nRead <= 0) {
                        break;
                    }
                    out.write(buffer, 0, nRead);
                }
                in.close();
            }

            out.close();
            stream.close();
            System.out.println("Adding completed OK");
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Error: " + ex.getMessage());
        }
    }

    
}
