/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openide.util.editable;

import org.openide.util.editable.BrandedLocale;

/**
 *
 * @author stefan
 */
public class BrandedLocaleRendrer {

    private final BrandedLocale locale;
    private final String defaultLocaleText;

    public BrandedLocaleRendrer(BrandedLocale locale, String defaultLocaleText) {
        this.locale = locale;
        this.defaultLocaleText = defaultLocaleText;
    }

    public String getDefaultLocaleText() {
        return defaultLocaleText;
    }
    
    public String getBranding(){
        return locale.getBranding();
    }

    @Override
    public String toString() {
        return locale.getBrandedDisplayName(defaultLocaleText);
    }
    
    
}
