package org.openide.util.editable;

import org.openide.util.editable.BrandedLocale;
import java.util.Arrays;
import java.util.Locale;

/**
 *
 * @author Stefan
 */
public class BrandedLocaleBuilder {
    private String branding;
    private String language;
    private String country;
    private String variant;

    public void setBranding(String branding) {
        this.branding = setEmptyToNull( branding );
    }

    public void setCountry(String country) {
        this.country = setEmptyToNull( country );
    }

    public void setLanguage(String language) {
        this.language = setEmptyToNull( language );
    }

    public void setVariant(String variant) {
        this.variant = setEmptyToNull( variant );
    }
    
    String setEmptyToNull(String value){
        if(value == null || value.trim().isEmpty()){
            return null;
        }
        return value;
    }
    
    /**
     * @param filename Bundle_[branding]_[lang]_[country]_[variant]
     */
    public static BrandedLocale parse(String filename){
        
        int fileExt = filename.lastIndexOf(".");
        if(fileExt != -1){
            filename = filename.substring(0, fileExt);
        }
        String[] parts = filename.split("_");
        
        BrandedLocaleBuilder builder = new BrandedLocaleBuilder();
        
        int langColumn = 1;
        
        if(parts.length > langColumn ) {
            if(parts[langColumn].length() != 2){
                builder.setBranding(parts[1]);
                langColumn = 2;
            }        
        }
        
        if(parts.length >= langColumn+1){
            builder.setLanguage(parts[langColumn]);
        }        
        if(parts.length >= langColumn+2){
            builder.setCountry(parts[langColumn+1]);
        }
        if(parts.length >= langColumn+3){
            builder.setVariant(parts[langColumn+2]);
        }
        
        if(parts.length > langColumn+3){
            throw new IllegalArgumentException("Unknown bundle format: "+Arrays.toString(parts));
        }
        return builder.create();
    }
    
    public BrandedLocale create() {
        
        Locale locale = null;
        
        if(language == null ){
            locale = BrandedLocale.DEFAULT_FALLBACK;
        } else if( country == null ) {
            locale = new Locale(language);
        } else if( variant == null ) {
            locale = new Locale(language, country);
        } else {
            locale = new Locale(language, country, variant);
        }
        
        BrandedLocale result = new BrandedLocale(branding, locale);
                
        return result;
    }
    
    
    public static String createBundle(BrandedLocale locale, boolean addExt) {
        StringBuilder sb = new StringBuilder("Bundle");
        if(locale.hasBranding()){
            sb.append("_").append(locale.getBranding());
        }
        if(locale.hasLanguage()){
            sb.append("_").append(locale.getLanguage());
        }
        if(locale.hasCountry()){
            sb.append("_").append(locale.getCountry());
        }
        if(locale.hasVariant()){
            sb.append("_").append(locale.getVariant());
        }
        if(addExt){
            sb.append(".properties");
        }
        return sb.toString();
    }
}
