/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.openide.util.editable;

import java.util.*;
import java.util.logging.Logger;
import org.openide.util.Lookup;

/**
 *
 * @author Stefan
 */
public class NbBundle {

    public static final NbResourceBundler STANDARD = new NbBundleImpl();
    private static NbResourceBundler bundler = STANDARD;
    private static final Logger LOGGER = Logger.getLogger(NbBundle.class.getName());


    public static void setBundler(NbResourceBundler bundler) {
        if(bundler == null){
            bundler = STANDARD;
        }
        NbBundle.bundler = bundler;
    }
    
    public static void setStandardBundler() {
        setBundler(STANDARD);
    }

    public static NbResourceBundler getBundler() {
        return bundler;
    }

    
    
    /**
     * Get the current branding token.
     *
     * @return the branding, or
     * <code>null</code> for none
     */
    public static String getBranding() {
        return bundler.getBranding();
    }

    /**
     * Set the current branding token. The permitted format, as a regular
     * expression:
     * <pre>[a-z][a-z0-9]*(_[a-z][a-z0-9]*)*</pre> <p class="nonnormative"> This
     * is normally only called by NetBeans startup code and unit tests.
     * Currently the branding may be specified by passing the
     * <code>--branding</code> command-line option to the launcher. </p>
     *
     * @param bt the new branding, or
     * <code>null</code> to clear
     * @throws IllegalArgumentException if in an incorrect format
     */
    public static void setBranding(String bt) throws IllegalArgumentException {
        if (bt != null && !bt.matches("[a-z][a-z0-9]*(_[a-z][a-z0-9]*)*")) { // NOI18N
            throw new IllegalArgumentException("Malformed branding token: " + bt); // NOI18N
        }
        bundler.setBranding(bt);
    }

    /**
     * Finds a localized and/or branded string in a bundle.
     *
     * @param clazz the class to use to locate the bundle (see {@link #getBundle(Class)}
     * for details)
     * @param resName name of the resource to look for
     * @return the string associated with the resource
     * @throws MissingResourceException if either the bundle or the string
     * cannot be found
     */
    public static String getMessage(Class<?> clazz, String resName, Object... param)
            throws MissingResourceException {
        
        String format = getBundle(clazz).getString(resName);
        if(param == null || param.length == 0){
            return format;
        }
        String result = java.text.MessageFormat.format(format, param);
        return result;
    }
    
    
    
    
    public static ResourceBundle getBundle(Class<?> clazz) {
        String baseName = findName(clazz);
        return getBundle(baseName, Locale.getDefault(), getLoader());
    }

    public static ResourceBundle getBundle(Class<?> clazz, Locale locale) {
        String baseName = findName(clazz);
        return getBundle(baseName, locale, getLoader());
    }

    public static ResourceBundle getBundle(String baseName, Locale locale) {
        return getBundle(baseName, locale, getLoader());
    }

    public static ResourceBundle getBundle(String baseName, Locale locale, ClassLoader loader) {
        return bundler.getBundle(baseName, locale, loader);
    }

    
    /**
     * @return default class loader which is used, when we don't have any other
     * class loader. (in function getBundle(String), getLocalizedFile(String),
     * and so on...
     */
    private static ClassLoader getLoader() {
        ClassLoader c = Lookup.getDefault().lookup(ClassLoader.class);

        return (c != null) ? c : ClassLoader.getSystemClassLoader();
    }

    /**
     * Finds package name for given class
     */
    private static String findName(Class<?> clazz) {
        String pref = clazz.getName();
        int last = pref.lastIndexOf('.');

        if (last >= 0) {
            pref = pref.substring(0, last + 1);

            return pref + "Bundle"; // NOI18N
        } else {
            // base package, search for bundle
            return "Bundle"; // NOI18N
        }
    }
    
    public static Iterator<String> getLocalizingSuffixes() {
        return org.openide.util.NbBundle.getLocalizingSuffixes();
    }

    
}
