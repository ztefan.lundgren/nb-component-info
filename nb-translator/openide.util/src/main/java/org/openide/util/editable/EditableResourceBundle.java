package org.openide.util.editable;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.*;

/**
 *
 * @author Stefan
 */
public final class EditableResourceBundle extends ResourceBundle {

    public static final String PROP_EDIT = "edit";
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private final String brandingToken;
    private final Locale locale;
    private final EditableProperties props;
    private final Map<String, String> orginal;
    private final String resourceBaseName;
    private final String bundleFile;
    private Vector<String> keys;

    public EditableResourceBundle(String brandingToken, String bundleFile, String resourceBaseName, Locale locale, Map<String, String> propsFromFile) {
        super();
        this.brandingToken = brandingToken;
        this.bundleFile = bundleFile;

        this.resourceBaseName = resourceBaseName;
        this.locale = locale;

        this.orginal = propsFromFile;
        this.props = new EditableProperties(false);
        props.putAll(propsFromFile);

        refreshKeyElements();
    }
    
    public void merge(Map<String, String> toAdd){
        props.putAll(toAdd);
    }

    private void refreshKeyElements() {
        keys = new Vector<String>(props.keySet());
    }

    public String getBrandingToken() {
        return brandingToken;
    }

    @Override
    public boolean containsKey(String key) {
        return props.containsKey(key);
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    public String getResourceBaseName() {
        return resourceBaseName;
    }

    public String getBundleFile() {
        return bundleFile;
    }

    @Override
    protected Object handleGetObject(String key) {
        String value = props.get(key);
        return value;
    }

    @Override
    public Enumeration<String> getKeys() {
        return keys.elements();
    }

    public String getOrginalString(String key) {
        return (String) orginal.get(key);
    }

    public Set<String> getOrginalKeySet() {
        return (Set) orginal.keySet();
    }

    public String put(String bundleKey, String newValue) {
        String oldValue = (String) props.put(bundleKey, newValue);

        if (oldValue == null) {
            refreshKeyElements();
        }
        EditablePropertyChangeEvent event = new EditablePropertyChangeEvent(this, PROP_EDIT, oldValue, newValue, bundleKey, bundleFile);
        pcs.firePropertyChange(event);

        return oldValue;

    }
    private String identifyer;

    public String getIdentifyer() {
        return identifyer;
    }

    public void setIdentifyer(String identifyer) {
        this.identifyer = identifyer;
    }

    public static String createIdentifyer(String brandingToken, String baseName, Locale locale, ClassLoader loader, boolean useFallback) {
        return String.format("%s_%s_%s_%s_%s", baseName, brandingToken, locale, loader.getClass().getName(), useFallback);
    }

    public synchronized void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);

    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }
    
    public static class EditablePropertyChangeEvent extends PropertyChangeEvent {

        private final String bundleKey;
        private final String bundleFile;
        private EditablePropertyChangeEvent(EditableResourceBundle source, String propertyName, String oldValue, String newValue, String bundleKey, String bundleFile) {
            super(source, propertyName, oldValue, newValue);
            this.bundleKey = bundleKey;
            this.bundleFile = bundleFile;
        }

        public String getBundleFile() {
            return bundleFile;
        }

        public String getBundleKey() {
            return bundleKey;
        }

        @Override
        public String getNewValue() {
            return (String)super.getNewValue();
        }

        @Override
        public String getOldValue() {
            return (String)super.getOldValue();
        }

        @Override
        public String toString() {
            return "EditablePropertyChangeEvent{" + "bundleKey=" + bundleKey + ", bundleFile=" + bundleFile + '}';
        }
        
        
    
    }
}
