package se.softstuff.nb.domain.tracer;

import java.awt.Component;
import java.io.IOException;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * <pre>
 * Splitts a string like this
 * "file:/C:/work/guicomponentinfo~svn/GuiComponentInfoDemo/trunk/application/target/guicomponentinfodemo/guicomponentinfodemo/modules/se-softstuff-nb-GuiComponentInfoDemo-TopComponet.jar!/se/softstuff/nb/guicomponentinfodemo/"
 * </pre
 *

 *
 * @author stefan
 */
public class MarkerUtilities {

    
    private MarkerUtilities() {
    }

    /**
     * Finds the binary for a class
     *
     * @param clazz to find for
     * @return the binary, empty or set but not null
     */
    public static String findJarFileName(Class clazz) {
        String binary = ""; // NOI18N
        try {
            if (clazz == null || clazz.getResource("") != null) { // NOI18N
                String path = clazz.getResource("").getPath(); // NOI18N
                binary = locateJarFileName(path);

            }
        } catch (Exception ignore) {
            Logger.getLogger(MarkerUtilities.class.getName()).log(Level.FINE, "Cant find binary for " + clazz, ignore);  // NOI18N
        }
        return binary;
    }

    public static String findJarFile(Class clazz) {
        String binary = ""; // NOI18N
        try {
            if (clazz == null || clazz.getResource("") != null) { // NOI18N
                binary = clazz.getResource("").getPath(); // NOI18N
                binary = locateJarFile(binary);
            }
        } catch (Exception ignore) {
            Logger.getLogger(MarkerUtilities.class.getName()).log(Level.FINE, "Cant find binary for " + clazz, ignore);  // NOI18N
        }
        return binary;
    }

    public static String findResoursePath(Class clazz) {
        String binary = ""; // NOI18N
        try {
            if (clazz == null || clazz.getResource("") != null) { // NOI18N
                binary = clazz.getResource("").getPath(); // NOI18N
                binary = locateResourcePath(binary);
            }
        } catch (Exception ignore) {
            Logger.getLogger(MarkerUtilities.class.getName()).log(Level.FINE, "Cant find binary for " + clazz, ignore);  // NOI18N
        }
        return binary;
    }
    
    
    

    public static class BundlePath {

        public final Set<String> bundleResoursePaths;
        public final Set<String> bundleTypes;
        public final String jarPath;
        public final String jarName;

        public BundlePath(String jarPath, String jarName, Collection<String> bundleResoursePaths, Collection<String> bundleTypes) {
            this.bundleResoursePaths = Collections.unmodifiableSet(new HashSet<String>(bundleResoursePaths));
            this.bundleTypes = Collections.unmodifiableSet(new HashSet<String>(bundleTypes));
            this.jarPath = jarPath;
            this.jarName = jarName;
        }
    }

    public static BundlePath findAllBundlePaths(Class clazz) {

        if (clazz == null || clazz.getResource("") != null) { // NOI18N
            String binaryPath = clazz.getResource("").getPath(); // NOI18N
            String jarPath = locateJarFile(binaryPath);
            String jarName = locateJarFileName(binaryPath);
            String resource = locateResourcePath(binaryPath);
            List<String> bundleResoursePaths = locateAllBundlesInResource(jarPath, resource);
            List<String> bundleTypes = locateBundleTypes(bundleResoursePaths);
            BundlePath result = new BundlePath(jarPath, jarName, bundleResoursePaths, bundleTypes);
            return result;
        }
        return null;
    }
    
    public static String locateModuleRef(String filePath) {
        if(filePath == null){
            return null;
        }
        String module = locateJarFileName(filePath);
        int end = module.lastIndexOf(".");
        if(end > 0){
            module = module.substring(0, end);
        }
        return module;
    }

    public static String locateJarFileName(String path) {
        path = locateJarFile(path);
        path = path.substring(path.lastIndexOf("/") + 1); // NOI18N
        return path;
    }

    public static String locateJarFile(String path) {
        int start = "file:/".length();//NOI18N
        int end = path.indexOf("!");//NOI18N
        if(end == -1){
            end = path.length();
        }
        path = path.substring(start, end);
        return path;
    }

    public static String locateResourcePath(String path) {
        int start = path.indexOf("!/");//NOI18N
        if(start==-1){
            start = 0;
        } else {
            start += 2;
        }
        int end = path.lastIndexOf("/") + 1;//NOI18N
        if (end == -1 || end < start) {
            end = path.length() - 1;
        }
        path = path.substring(start, end);
        return path;
    }
    
    public static String locateResourceFile(String path) {
        int start = path.indexOf("!/") + 2;//NOI18N
        path = path.substring(start);
        return path;
    }

    
    public static String getBundleBrandingAndLocaleString(String bundleRef) {
        Pattern p = Pattern.compile(".*Bundle((_)?(.*)).properties");
        Matcher matcher = p.matcher(bundleRef);
        boolean matchFound = matcher.find();
        String lang = null;
        if (matchFound) {
            lang = matcher.group(3);
        }
        return lang;
    }
    
    static List<String> locateAllBundlesInResource(String jarPath, String resource) {
        List<String> bundles = new ArrayList<String>();
        try {

            JarFile jar = new JarFile(jarPath);
            Enumeration<JarEntry> entries = jar.entries();
            while (entries.hasMoreElements()) {
                String name = entries.nextElement().getName();
                if (doResourceMatchSelectedBunlde(name, resource)) {
                    bundles.add(name);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(MarkerUtilities.class.getName()).log(Level.FINE, String.format("Cant find bundles for %s in %s", resource, jarPath), ex);  // NOI18N
        } finally {
            return bundles;
        }
    }

    
    static boolean doResourceMatchSelectedBunlde(String name, String resource) {
        name = name.toLowerCase();
        resource = resource.toLowerCase()+"bundle";
        if(name.startsWith(resource) && name.endsWith(".properties")) {
            return true;
        }
        return false;
    }
    
    static List<String> locateBundleTypes(List<String> bundleResoursePaths) {
        List<String> types = new ArrayList<String>();
        for (String bundle : bundleResoursePaths) {
            String lowerBundle = bundle.toLowerCase();
            int begin = lowerBundle.lastIndexOf("bundle")+"bundle".length();
            if(isCharUnderscore(begin, lowerBundle)) {
                begin++;
            }
            int end = lowerBundle.lastIndexOf(".properties");
           
            if( end != -1 && begin <= end && end < lowerBundle.length()) {
                types.add(bundle.substring(begin, end));
            }
        }
        return types;
    }
    
    private static boolean isCharUnderscore(int begin, String bundle) {
        return begin>0 && begin<bundle.length() && bundle.charAt(begin) == '_';
    }
    
    public static boolean isDontMarkThisComponent(Component component) {
        if (component == null) {
            return false;
        }
        if (component instanceof DontMarkThisComponent) {
            return true;
        }

        return isDontMarkThisComponent(component.getParent());
    }
    
}
