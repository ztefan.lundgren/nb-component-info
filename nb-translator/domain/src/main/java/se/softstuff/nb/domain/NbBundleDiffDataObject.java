package se.softstuff.nb.domain;

import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import javax.swing.Icon;
import org.apache.log4j.Logger;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.nodes.CookieSet;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.util.lookup.Lookups;
import org.openide.util.lookup.ProxyLookup;
import org.openide.windows.TopComponent;
import se.softstuff.nb.domain.node.TranslateProjectNode;
import se.softstuff.nb.domain.tracer.StartTracerCookie;
import se.softstuff.nb.domain.tracer.StopTracerCookie;
import se.softstuff.nb.domain.tracer.TracerManager;
import se.softstuff.nb.domain.xml.NbBundleDiffXmlHandler;

public class NbBundleDiffDataObject extends MultiDataObject {

    public static final String MIME_TYPE = "text/nb-bundle-diff+xml";
    private final Logger log = Logger.getLogger(getClass());
    private NbBundleDiff content;
    private StartTracerSupport startTrackerSupport;
    private StopTracerSupport stopTracerSupport;
    private FileLock lock;
//    private InstanceContent ic = new InstanceContent();
//    private Lookup lookup;
    private TranslateProjectNode node;

    public NbBundleDiffDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
        super(pf, loader);
        
        
        registerEditor(MIME_TYPE, true);
        
    }

    public NbBundleDiff getContent() {
        if(content == null){

//                lock = takePrimaryFileLock();
                content = read();
                content.setFilename(getName());
                content.addPropertyChangeListener(new ChangeTracker());
            CookieSet cookies = getCookieSet();

                cookies.add(new Saver());

        }
        
        return content;
    }

    @Override
    protected int associateLookup() {
        return 1;
    }

    @Override
    protected Node createNodeDelegate() {
        return super.createNodeDelegate();
    }

    public TranslateProjectNode createProjectNode() {
//        if(lookup == null){
//            lookup = new AbstractLookup(ic);
//            ic.add(getContent());
//            ic.add(this);
        if(node == null){
            Lookup lookup = new ProxyLookup(getLookup(), Lookups.singleton(getContent()));
//        }
            node = new TranslateProjectNode(lookup);
        }
        return node;
    }

    @MultiViewElement.Registration(displayName = "#LBL_NbBundleDiff_EDITOR",
    iconBase = "se/softstuff/nb/domain/nb_translate.png",
    mimeType = "text/nb-bundle-diff+xml",
    persistenceType = TopComponent.PERSISTENCE_ONLY_OPENED,
    preferredID = "NbBundleDiff",
    position = 1000)
    @Messages("LBL_NbBundleDiff_EDITOR=Source")
    public static MultiViewEditorElement createEditor(Lookup lkp) {
        return new MultiViewEditorElement(lkp);
    }

    private NbBundleDiff read() {

        log.trace("getContent begin");
        InputStream inputStream = null;
        InputStreamReader reader = null;
        try {
            inputStream = getPrimaryFile().getInputStream();
            reader = new InputStreamReader(inputStream, NbBundleDiffXmlHandler.UTF8);
            NbBundleDiff sot = NbBundleDiffXmlHandler.fromXml(reader);
            log.trace("getContent end");
            return sot;
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            log.error("failde read sot", ex);
            throw new RuntimeException(ex);
        } finally {
            try {
                inputStream.close();
            } catch (IOException ignore) {
            }
            try {
                reader.close();
            } catch (IOException ex) {
                Exceptions.printStackTrace(ex);
            }
        }
    }

    private void tracer(boolean on) {
        if (on) {
            getCookieSet().assign(StartTracerCookie.class);
            getCookieSet().assign(StopTracerCookie.class, stopTracerSupport);
        } else {
            getCookieSet().assign(StartTracerCookie.class, startTrackerSupport);
            getCookieSet().assign(StopTracerCookie.class);

        }
    }

    private TracerManager getTracer() {
        return Lookup.getDefault().lookup(TracerManager.class);
    }

    private static final Icon ICON = ImageUtilities.loadImageIcon("se/softstuff/nb/domain/save.png", true);
 
//    private class MySavable extends AbstractSavable implements Icon {
//        MySavable() {
//            register();
//        }
// 
//        @Override
//        protected String findDisplayName() {
//            return getName();
//        }
//
//        @Override
//        protected void handleSave() throws IOException {
//            
//            doSave();
//        }
// 
//
//        @Override
//        public boolean equals(Object obj) {
//            if (obj instanceof MySavable) {
//                MySavable m = (MySavable)obj;
//                return findDisplayName().equals(m.findDisplayName());
//            }
//            return false;
//        }
//
//        @Override
//        public int hashCode() {
//            return NbBundleDiffDataObject.this.hashCode();
//        }
//
//        @Override
//        public void paintIcon(Component c, Graphics g, int x, int y) {
//            ICON.paintIcon(c, g, x, y);
//        }
//
//        @Override
//        public int getIconWidth() {
//            return ICON.getIconWidth();
//        }
//
//        @Override
//        public int getIconHeight() {
//            return ICON.getIconHeight();
//        }
//        
//        public void doSave() throws IOException {
//            Writer writer = null;
//                      
//            
//            Entry primaryEntry = getPrimaryEntry();
//            boolean locked = primaryEntry.isLocked();
//            FileLock lock = takePrimaryFileLock();
//            FileObject file = primaryEntry.getFile();
//            
//            try {
//                OutputStream output = file.getOutputStream(lock);
//                writer = new OutputStreamWriter(output, NbBundleDiffXmlHandler.UTF8);
//                NbBundleDiffXmlHandler.toXml(getContent(), output);
//                setModified(false);
//
//            } finally {
//                lock.releaseLock();
//                try {
//                    if (writer != null) {
//                        writer.close();
//                    }
//                } catch (IOException ignore) {
//                }
//            }
//
//        }
// 
//    } 
    
    private class Saver implements SaveCookie {

        @Override
        public void save() throws IOException {
            Writer writer = null;
                      
            
            Entry primaryEntry = getPrimaryEntry();
            boolean locked = primaryEntry.isLocked();
            FileLock lock = takePrimaryFileLock();
            FileObject file = primaryEntry.getFile();
            
            try {
                OutputStream output = file.getOutputStream(lock);
                writer = new OutputStreamWriter(output, NbBundleDiffXmlHandler.UTF8);
                NbBundleDiffXmlHandler.toXml(getContent(), output);
                setModified(false);

            } finally {
                lock.releaseLock();
                try {
                    if (writer != null) {
                        writer.close();
                    }
                } catch (IOException ignore) {
                }
            }

        }
    }

    private class ChangeTracker implements PropertyChangeListener {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            log.debug("setModified(TRUE)  ChangeTracker.propertyChange: "+evt+" ");
            NbBundleDiffDataObject.this.setModified(true);
//            if (getLookup().lookup(MySavable.class) == null) {
//                ic.add(new MySavable());
//            }
        }
    }

    private class StartTracerSupport implements StartTracerCookie {

        @Override
        public void startAndMark(Component comp) {
            tracer(true);
            getTracer().startTrackerAndMark(comp);
        }
    }

    private class StopTracerSupport implements StopTracerCookie {

        @Override
        public void stop() {
            tracer(false);
            getTracer().stopTracker();
        }
    }

    

}
