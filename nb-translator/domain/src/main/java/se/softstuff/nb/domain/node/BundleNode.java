/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.domain.node;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Properties;
import org.openide.ErrorManager;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import se.softstuff.nb.domain.NbBundleDiff;

/**
 *
 * @author Stefan
 */
public class BundleNode extends AbstractNode {

    private String resource;
    private String moduleName;
    
    public BundleNode(String resource, String moduleName, Lookup lookup) {
        super(Children.LEAF, lookup);
//        super(Children.create(new BundleChildFactory(resource, moduleName, lookup), true), lookup);
        setDisplayName(resource);
        this.resource = resource;
        this.moduleName = moduleName;
    }
    
    @Override
    protected Sheet createSheet() {

        NbBundleDiff diff = getLookup().lookup(NbBundleDiff.class);
        Properties changedProps = diff.getChangedResource(moduleName).get(resource);
        Properties originalProps = diff.getOriginalResource(moduleName).get(resource);
        
        Sheet sheet = Sheet.createDefault();
        
        Sheet.Set changedSet = createSet("changes");
        Sheet.Set originalSet = createSet("original");
        
        sheet.put(changedSet);
        sheet.put(originalSet);

        for(String key : changedProps.stringPropertyNames()) {                
            changedSet.put(new MyPropertySupport(changedProps, key, true));
        }
        
        for(String key : originalProps.stringPropertyNames()) {                
            originalSet.put(new MyPropertySupport(originalProps, key, false));
        }

        return sheet;

    }
    
    public static Sheet.Set createSet(String name) {
        Sheet.Set ps = new Sheet.Set();
        ps.setName(name);
        ps.setDisplayName(NbBundle.getMessage(BundleNode.class, "BundleNode.Set."+name+".title"));
        ps.setShortDescription(NbBundle.getMessage(BundleNode.class, "BundleNode.Set."+name+".hint"));

        return ps;
    }
    
    private class MyPropertySupport extends PropertySupport.ReadWrite<String>{
        Properties props;
        String key;
        boolean canWrite;
        public MyPropertySupport(Properties props, String key, boolean canWrite) {
            super(key, String.class, key, key);
            this.props = props;
            this.key = key;
            this.canWrite=canWrite;
        }
        
        
        @Override
        public String getValue() throws IllegalAccessException, InvocationTargetException {
            return props.getProperty(key);
        }

        @Override
        public void setValue(String val) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
            props.put(key, val);
        }
        
        public boolean canWrite() {
            return canWrite;
        }
        
    }
    
}
