package se.softstuff.nb.domain.tracer;

/**
 * Implement this interface to exclude tracking form a component
 * @author stefan
 */
public interface DontMarkThisComponent {
    
}
