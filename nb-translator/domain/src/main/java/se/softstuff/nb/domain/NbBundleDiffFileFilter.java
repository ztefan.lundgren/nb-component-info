package se.softstuff.nb.domain;

import java.io.File;
import javax.swing.filechooser.FileFilter;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

/**
 *
 * @author stefan
 */
public class NbBundleDiffFileFilter extends FileFilter {
    
    @Override
    public boolean accept(File f) {
        if( f.getName().endsWith(".xml") ){
                FileObject fobject = FileUtil.toFileObject(f);
                String mimeType = fobject.getMIMEType();
                boolean accepted = NbBundleDiffDataObject.MIME_TYPE.equals(mimeType);
                return accepted;
        }
        return false;
    }

    @Override
    public String getDescription() {
        return "*.xml";
    }
}
