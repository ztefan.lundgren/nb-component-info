/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.domain.node;

import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.Lookup;
import se.softstuff.nb.domain.NbBundleDiff;

/**
 *
 * @author Stefan
 */
public class TranslateProjectNode extends AbstractNode {
    
    public TranslateProjectNode(Lookup lookup){
        super(Children.create(new TranslateChildFactory(lookup), true), lookup);
        NbBundleDiff diff = lookup.lookup(NbBundleDiff.class);
        setDisplayName(diff.getFilename());
    }
}
