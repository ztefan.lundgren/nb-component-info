/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.domain.node;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import se.softstuff.nb.domain.NbBundleDiff;

/**
 *
 * @author Stefan
 */
class ModuleChildFactory extends ChildFactory<String> {

    private final String moduleName;
    private final Lookup lookup;
            
    public ModuleChildFactory(String moduleName, Lookup lookup) {
        this.moduleName = moduleName;
        this.lookup = lookup;
    }

    @Override
    protected boolean createKeys(List<String> toPopulate) {
        Set<String> resources = lookup.lookup(NbBundleDiff.class).getChangedResource(moduleName).keySet();
        toPopulate.addAll(resources);
        return true;
    }
    
    @Override
    protected Node createNodeForKey(String resource) {
        NbBundleDiff proj = lookup.lookup(NbBundleDiff.class);
        return new BundleNode(resource, moduleName, Lookups.singleton(proj));
    }

    
    
    
}
