package se.softstuff.nb.domain.tracer;

/**
 *
 * @author Stefan
 */
public interface StopTracerCookie {
    
    void stop();    
}
