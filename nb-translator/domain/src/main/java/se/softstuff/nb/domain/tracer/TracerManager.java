package se.softstuff.nb.domain.tracer;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.io.IOException;
import org.openide.util.Lookup;

/**
 * Start and stops the tracer. Keeps the lookup used to send all component updates
 * 
 * @author stefan
 */
public interface TracerManager extends Lookup.Provider {

    public static final String TRACER_GROUP_NAME = "TrackerGroup"; // NOI18N

    public static final String PROP_TRACKER_ENABLED = "TrackerEnabled"; // NOI18N

    public void addPropertyChangeListener(PropertyChangeListener listener);

    public void removePropertyChangeListener(PropertyChangeListener listener);

    public boolean isTrackerEnabled();

    public void stopTracker();

    public void startTracker();
    
    public void startTrackerAndMark(Component comp);
    
    public boolean isHighlighterPaused();
    
    public void toggleHighlighter();
            
    public void pauseHighlighter();
    
    public void resumeHighlighter();

    /**
     * starts tracker if stopped or stops it if its started
     */
    public void toggleTracker();

    /**
     * Creates and puts the HiglightedComponent in the lookup
     * @param color Handle missing or null color by ignoring and put first color insted
     */
    public void lit(Component current, Color color);

    /**
     * @return the Shortcut for toggling the marker
     */
    public String getShortcutKeyDescription();

    public boolean isLoaded();

    public void unload();

    public void load();
    
    public void attachTrigger(TracerTriggerKeys startTriggerKeys);
    
    public void detatchTrigger();

    
}
