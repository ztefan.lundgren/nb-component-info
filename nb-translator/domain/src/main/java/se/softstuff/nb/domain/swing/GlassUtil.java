package se.softstuff.nb.domain.swing;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseEvent;

/**
 *
 * @author stefan
 */
public interface GlassUtil {
    
    public Component findComponentUnderGlassPaneAt(MouseEvent mouseEvent);
     
    public Component findComponentUnderGlassPaneAt(Component top, Point onComp);
}
