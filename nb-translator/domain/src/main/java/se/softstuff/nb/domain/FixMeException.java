package se.softstuff.nb.domain;

/**
 *
 * @author stefan
 */
public class FixMeException extends RuntimeException {

    public FixMeException(String message) {
        super(message);
    }

    public FixMeException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
