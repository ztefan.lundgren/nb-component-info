package se.softstuff.nb.domain;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import se.softstuff.nb.domain.tracer.MarkerUtilities;

public class NbBundleDiff {

    public static final String PROP_VERSION = "version";

    public static final String PROP_RESOURCE_ORIGINAL = "resourceOriginal";

    public static final String PROP_RESOURCE_CHANGES = "resourceChanges";

    private String xmlns = "nb-bundle-diff";

    private int version;

    private List<ProjectBundleInfo> knownProjects;

    private Map<String, Map<String, Properties>> original;

    private Map<String, Map<String, Properties>> changes;

    private transient String filename;
    
    private transient final PropertyChangeSupport propertyChangeSupport;

    public NbBundleDiff() {
        knownProjects = new ArrayList<ProjectBundleInfo>();
        original = new HashMap<String, Map<String, Properties>>();
        changes = new HashMap<String, Map<String, Properties>>();
        propertyChangeSupport = new PropertyChangeSupport(this);
    }

    public String getXmlns() {
        return xmlns;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        int oldVersion = this.version;
        this.version = version;
        propertyChangeSupport.firePropertyChange(PROP_VERSION, oldVersion, version);
    }

    public List<ProjectBundleInfo> getKnownProjects() {
        return knownProjects;
    }

    public void setKnownProjects(Collection<ProjectBundleInfo> projectInfo) {
        this.knownProjects.clear();
        knownProjects.addAll(projectInfo);

    }

    public Map<String, ProjectBundleInfo> getKnownProjectMap() {
        Map<String, ProjectBundleInfo> ref2project = new HashMap<String, ProjectBundleInfo>();
        for (ProjectBundleInfo known : knownProjects) {
            ref2project.put(known.getModuleRef(), known);
        }
        return ref2project;
    }
    
     public Set<String> getOriginalModulesNames() {
        return original.keySet();
    }

    public Map<String, Map<String, Properties>> getOriginal() {
        return original;
    }

    public Map<String, Properties> getOriginalResource(String module) {
        return original.get(module);
    }

    public void setOriginal(Map<String, Map<String, Properties>> original) {
        this.original = original;
    }

    public Set<String> getChangedModulesNames() {
        return changes.keySet();
    }

    public Map<String, Properties> getChangedResource(String module) {
        return changes.get(module);
    }

    public String addResourceOriginal(String module, String resouce, String key, String value) {
        return addResource(module, resouce, key, value, false);
    }

    public String addResourceChange(String module, String resouce, String key, String value) {

        return addResource(module, resouce, key, value, true);
    }

    private String addResource(String module, String resouce, String key, String value, boolean changedMap) {

        Map<String, Map<String, Properties>> map = changedMap ? changes : original;
        String propName = changedMap ? PROP_RESOURCE_CHANGES : PROP_RESOURCE_ORIGINAL;

        if (!map.containsKey(module)) {
            map.put(module, new HashMap<String, Properties>());
        }
        Map<String, Properties> resourceChanges = map.get(module);

        if (!resourceChanges.containsKey(resouce)) {
            resourceChanges.put(resouce, new Properties());
        }

        Properties bundle = resourceChanges.get(resouce);
        String oldValue = (String) bundle.put(key, value);

        propertyChangeSupport.firePropertyChange(propName, null, resourceChanges);
        return oldValue;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public boolean contiansChangesFor(String module, String resources) {
        Map<String, Properties> resourceChanges = getChangedResource(module);
        return resourceChanges != null && resourceChanges.containsKey(resources);
    }

    public boolean contiansOriginalFor(String module, String resources) {
        Map<String, Properties> resource = getOriginalResource(module);
        return resource != null && resource.containsKey(resources);
    }

    public Collection<? extends ProjectBundleInfo> getChangedProjects() {

        Map<String, ProjectBundleInfo> ref2project = getKnownProjectMap();


        List<String> modules = new ArrayList<String>(changes.keySet());
        List<ProjectBundleInfo> result = new ArrayList<ProjectBundleInfo>(modules.size());
        Collections.sort(modules);
        for (String moduleRef : modules) {
            ProjectBundleInfo project = ref2project.get(moduleRef);
            if (project == null) {
                project = new ProjectBundleInfo(moduleRef);
            }
            result.add(project);
        }
        return result;
    }

    public Set<String> getChangedLanguages() {
        Set<String> languages = new HashSet<String>();



        for (Map<String, Properties> bundles : changes.values()) {
            for (String bundleRef : bundles.keySet()) {
                String lang = MarkerUtilities.getBundleBrandingAndLocaleString(bundleRef);
                if (lang != null) {
                    languages.add(lang);
                }
            }
        }
        return languages;
    }

    

    public String getFilename() {
        return filename;
    }

    void setFilename(String filename) {
        this.filename = filename;
    }

    public String getOriginalValue(String module, String resource, String key, String defaultValue) {
        Map<String, Properties> originalResource = getOriginalResource(module);
        if(originalResource==null || !originalResource.containsKey(resource)){
            return defaultValue;
        }
        return originalResource.get(resource).getProperty(key, defaultValue);
    }
    
    public String getChangedValue(String module, String resource, String key, String defaultValue) {
        Map<String, Properties> changedResource = getChangedResource(module);
        if(changedResource==null || !changedResource.containsKey(resource)){
            return getOriginalValue(module, resource, key, defaultValue);
        }
        return changedResource.get(resource).getProperty(key, defaultValue);
    }
    
}
