package se.softstuff.nb.domain.xml;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author Stefan
 */
public class DiffChangesConverter implements Converter {

    private static final String UTF8 = "UTF8";
    
    @Override
    public boolean canConvert(Class clazz) {
        return AbstractMap.class.isAssignableFrom(clazz);
    }

    @Override
    public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
        AbstractMap<String, Map<String, Properties>> moduleMap = (AbstractMap<String, Map<String, Properties>>) value;
        for (Map.Entry<String, Map<String, Properties>> moduleEntry : moduleMap.entrySet()) {
            writer.startNode(moduleEntry.getKey());
            Map<String, Properties> map = moduleEntry.getValue();
            for (Map.Entry<String, Properties> entry : map.entrySet()) {
                writer.startNode(entry.getKey());
                for (Map.Entry<Object, Object> change : entry.getValue().entrySet()) {
                    String key = marshalKey((String) change.getKey());
                    writer.startNode(key);
                    writer.setValue(encode(change.getValue()));
                    writer.endNode();
                }
                writer.endNode();
            }
            writer.endNode();
        }
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
        Map<String, Map<String, Properties>> modules = new HashMap<String, Map<String, Properties>>();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String module = reader.getNodeName();
            Map<String, Properties> map = new HashMap<String, Properties>();
            modules.put(module, map);
            while (reader.hasMoreChildren()) {
                reader.moveDown();
                String resource = reader.getNodeName();
                Properties changes = new Properties();
                map.put(resource, changes);
                while (reader.hasMoreChildren()) {
                    reader.moveDown();
                    String key = unmarshalKey(reader.getNodeName());
                    changes.put(key, decode(reader.getValue()));
                    reader.moveUp();
                }
                reader.moveUp();
            }
            reader.moveUp();
        }
        return modules;
    }

    private String encode(Object value) {
        try {
            return URLEncoder.encode(value.toString(), UTF8);
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }

    private String decode(String value) {
        try {
            return URLDecoder.decode(value, UTF8);
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex);
        }
    }
    
    
    String marshalKey(String key) {
        key = key.replaceAll("/", "__fslash__");
        return key;
    }

    String unmarshalKey(String key) {
        key = key.replaceAll("__fslash__", "/");
        return key;
    }
    
}
