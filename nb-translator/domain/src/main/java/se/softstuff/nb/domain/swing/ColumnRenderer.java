package se.softstuff.nb.domain.swing;

/**
 *
 * @author stefan
 */
public abstract class ColumnRenderer<ROW,VALUE> {

    public abstract String headerText();
    
    public String headerTooltip(){
        return null;
    }
    public abstract VALUE render(ROW row);
    
    
    public boolean isCellEditable(ROW row) {
        return false;
    }

    public void setValueAt(ROW row, VALUE aValue) {
    }
    
}
