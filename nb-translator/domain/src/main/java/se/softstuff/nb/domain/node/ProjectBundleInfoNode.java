package se.softstuff.nb.domain.node;

import java.beans.IntrospectionException;
import org.openide.nodes.BeanNode;
import se.softstuff.nb.domain.ProjectBundleInfo;

/**
 *
 * @author stefan
 */
public class ProjectBundleInfoNode extends BeanNode<ProjectBundleInfo> {
    
    public ProjectBundleInfoNode(ProjectBundleInfo project) throws IntrospectionException{
        super(project);
        setDisplayName(project.getDisplayName());      
    }
}
