package se.softstuff.nb.domain.tracer;

import java.awt.Component;

/**
 *
 * @author Stefan
 */
public interface StartTracerCookie {
    
//    void start();
    
    void startAndMark(Component comp);
    
}
