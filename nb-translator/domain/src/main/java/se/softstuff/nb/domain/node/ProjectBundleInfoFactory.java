package se.softstuff.nb.domain.node;

import java.beans.IntrospectionException;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import se.softstuff.nb.domain.NbBundleDiff;
import se.softstuff.nb.domain.ProjectBundleInfo;

/**
 *
 * @author stefan
 */
public class ProjectBundleInfoFactory extends ChildFactory<ProjectBundleInfo> {
    
    private NbBundleDiff translation;
    public ProjectBundleInfoFactory(NbBundleDiff translation) {
        this.translation = translation;
    }

    @Override
    protected boolean createKeys(List<ProjectBundleInfo> toPopulate) {
        toPopulate.addAll(translation.getChangedProjects());
        return true;
    }

    @Override
    protected Node createNodeForKey(ProjectBundleInfo key) {
        ProjectBundleInfoNode node = null;
        try {
            node = new ProjectBundleInfoNode(key);
        } catch (IntrospectionException ex) {
            Exceptions.printStackTrace(ex);
        }
        return node;
    }
    
}
