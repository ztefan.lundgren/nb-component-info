/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.domain.tracer;

import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.prefs.Preferences;

/**
 *
 * @author Stefan
 */
public class TracerTriggerKeys {
       
    public static final String PROP_SHIFT = "shift";
    public static final String PROP_CTRL = "ctrl";
    public static final String PROP_ALT = "alt";
    public static final String PROP_MOUSEBUTTON = "mouseButton";
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    
    private boolean shift;
    private int mouseButton;
    private boolean ctrl;
    private boolean alt;
    
    public boolean isShift() {
        return shift;
    }

    public void setShift(boolean shift) {
        boolean oldShift = this.shift;
        this.shift = shift;
        propertyChangeSupport.firePropertyChange(PROP_SHIFT, oldShift, shift);
    }
    

    public boolean isCtrl() {
        return ctrl;
    }

    public void setCtrl(boolean ctrl) {
        boolean oldCtrl = this.ctrl;
        this.ctrl = ctrl;        
        propertyChangeSupport.firePropertyChange(PROP_CTRL, oldCtrl, ctrl);

    }

    

    public boolean isAlt() {
        return alt;
    }

    public void setAlt(boolean alt) {
        boolean oldAlt = this.alt;
        this.alt = alt;
        propertyChangeSupport.firePropertyChange(PROP_ALT, oldAlt, alt);
    }


    public int getMouseButton() {
        return mouseButton;
    }

    public void setMouseButton(int mouseButton) {
        int oldMouseButton = this.mouseButton;
        this.mouseButton = mouseButton;
        propertyChangeSupport.firePropertyChange(PROP_MOUSEBUTTON, oldMouseButton, mouseButton);
    }

    public int getMouseButtonIndex(){
        return mouseButton - MouseEvent.BUTTON1;
    }
    
    public void setMouseButtonIndex(int mouseButtonIndex){
        setMouseButton(mouseButtonIndex + MouseEvent.BUTTON1);
    }

    
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public TracerTriggerKeys() {
        this(true, true, false, MouseEvent.BUTTON1);
    }

    public TracerTriggerKeys(boolean shift, boolean ctrl, boolean alt, int mouseButton) {
        this.shift = shift;
        this.ctrl = ctrl;
        this.alt = alt;
        this.mouseButton = mouseButton;
    }
    
    public TracerTriggerKeys(Preferences pref, String prefix){
        shift = pref.getBoolean(prefix+PROP_SHIFT, true);
        ctrl = pref.getBoolean(prefix+PROP_CTRL, true);
        alt = pref.getBoolean(prefix+PROP_ALT, false);
        mouseButton = pref.getInt(prefix+PROP_MOUSEBUTTON, MouseEvent.BUTTON1);
    }

    public boolean isTriggeredBy(MouseEvent me) {
        boolean triggered = me.isAltDown() == alt && me.isControlDown() == ctrl && me.isShiftDown() == shift && me.getButton() == mouseButton;
        return triggered;
    }
    
    public boolean isModyfiersPressed(MouseEvent me) {
        return me.isAltDown() == alt && me.isControlDown() == ctrl && me.isShiftDown() == shift;
    }

    
    public void persist(Preferences pref, String prefix) {
        pref.putBoolean(prefix+PROP_SHIFT, shift);
        pref.putBoolean(prefix+PROP_CTRL, ctrl);
        pref.putBoolean(prefix+PROP_ALT, alt);
        pref.putInt(prefix+PROP_MOUSEBUTTON, mouseButton);
    }

    public boolean fireMarkedAndClicked(MouseEvent mouseEvent) {
        return false;
    }
    
    
    
    
}
