/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.domain.node;

import java.awt.Event;
import java.util.Date;
import org.openide.ErrorManager;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.PropertySupport;
import org.openide.nodes.Sheet;
import org.openide.util.Lookup;

/**
 *
 * @author Stefan
 */
public class ModuleNode extends AbstractNode {

    private String moduleName;

    public ModuleNode(String moduleName, Lookup lookup) {
        super(Children.create(new ModuleChildFactory(moduleName,lookup), true), lookup);
        setDisplayName(moduleName);
        this.moduleName =moduleName;
    }
    
    
//    @Override
//    public PasteType getDropType(Transferable t, final int action, int index) {
//        final Node dropNode = NodeTransfer.node( t, 
//                DnDConstants.ACTION_COPY_OR_MOVE+NodeTransfer.CLIPBOARD_CUT );
//        if( null != dropNode ) {
//            Project project = dropNode.getLookup().lookup(Project.class);
//            
//            if(project != null){
//                return new PasteType() {
//                    public Transferable paste() throws IOException {
////                        getChildren().add( new Node[] { new MovieNode(movie) } );
////                        if( (action & DnDConstants.ACTION_MOVE) != 0 ) {
////                            dropNode.getParentNode().getChildren().remove( new Node[] {dropNode} );
////                        }
//                        return null;
//                    }
//                };
//            }
//            System.out.println(dropNode);
//            
////            final Movie movie = (Movie)dropNode.getLookup().lookup( Movie.class );
////            if( null != movie  && !this.equals( dropNode.getParentNode() )) {
////                return new PasteType() {
////                    public Transferable paste() throws IOException {
////                        getChildren().add( new Node[] { new MovieNode(movie) } );
////                        if( (action & DnDConstants.ACTION_MOVE) != 0 ) {
////                            dropNode.getParentNode().getChildren().remove( new Node[] {dropNode} );
////                        }
////                        return null;
////                    }
////                };
////            }
//        }
//        return null;
//    }
    
}
