package se.softstuff.nb.domain.swing;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author stefan
 */
public class DynamicTableModel<E> extends AbstractTableModel {

    private CopyOnWriteArrayList<E> rows;

    private List<ColumnRenderer<E, ?>> columns;

    public DynamicTableModel() {
        rows = new CopyOnWriteArrayList<E>();
        columns = new LinkedList<ColumnRenderer<E, ?>>();
    }

    public void addColumn(ColumnRenderer<E, ?>... column) {
        columns.addAll(Arrays.asList(column));
        fireTableStructureChanged();
    }

    public void removeColumn(ColumnRenderer<E, ?> column) {
        columns.remove(column);
        fireTableStructureChanged();
    }

    public void removeColumn(int index) {
        columns.remove(index);
        fireTableStructureChanged();
    }

    public void removeAllColumns() {
        columns.clear();
        fireTableStructureChanged();
    }

    public void addRow(E row) {
        rows.add(row);
        int rowNr = rows.size();
        fireTableRowsInserted(rowNr, rowNr);
    }

    public void addRows(E... row) {
        addRows(Arrays.asList(row));
    }

    public void addRows(Collection<? extends E> rowsToAdd) {
        int from = rows.size();
        rows.addAll(rowsToAdd);
        int to = rows.size();
        fireTableRowsInserted(from, to);
    }

    public void setRows(Collection<? extends E> rowsToAdd) {
        int old = rows.size();
        rows.clear();
        rows.addAll(rowsToAdd);
        int to = rows.size();

//        if(old > 0){
            fireTableRowsDeleted(0, old);
//        }
//        if(to > 0){
            fireTableRowsInserted(0, to);
//        }
    }

    public void removeRow(E row) {
        int firstRow = rows.size();
        rows.remove(row);
        fireTableRowsDeleted(firstRow, firstRow);
    }

    public List<E> getRows() {
        return rows;
    }

    @Override
    public int getRowCount() {
        return rows.size();
    }

    @Override
    public int getColumnCount() {
        return columns.size();
    }

    @Override
    public String getColumnName(int column) {
        return columns.get(column).headerText();
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        ColumnRenderer<E, ?> renderer = columns.get(columnIndex);
        return getRendererValueClass(renderer);
    }

    private Class<?> getRendererValueClass(ColumnRenderer<E, ?> renderer) {
        try {

            Type genericSuperclass = renderer.getClass().getGenericSuperclass();

            if (genericSuperclass instanceof ParameterizedType) {
                ParameterizedType parameterizedSuper = (ParameterizedType) genericSuperclass;
                if (parameterizedSuper.getRawType().equals(ColumnRenderer.class)) {
                    Type outputType = parameterizedSuper.getActualTypeArguments()[1];
                    return (Class<?>) (outputType instanceof Class ? outputType : null);
                }
            }


        } catch (Exception ignore) {
        }
        return String.class;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        E row = rows.get(rowIndex);
        ColumnRenderer column = columns.get(columnIndex);
        Object value = column.render(row);
        return value;
    }
    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        ColumnRenderer column = columns.get(columnIndex);
        E row = rows.get(rowIndex);
        return column.isCellEditable(row);
    }
    @Override
    public void setValueAt(Object newValue, int rowIndex, int columnIndex) {
        ColumnRenderer column = columns.get(columnIndex);
        E row = rows.get(rowIndex);
        column.setValueAt(row, newValue);
    }

    public E getRow(int selectedRow) {
        if(selectedRow<0 || selectedRow > rows.size()-1){
            return null;
        }
        return rows.get(selectedRow);
    }

    public void clear() {
        rows.clear();
    }
}
