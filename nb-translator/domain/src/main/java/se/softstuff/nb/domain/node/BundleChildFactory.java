/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.domain.node;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.Lookup;
import org.openide.util.lookup.Lookups;
import se.softstuff.nb.domain.NbBundleDiff;

/**
 *
 * @author Stefan
 */
class BundleChildFactory extends ChildFactory<String> {

    private String resource;
    private String moduleName;
    private Lookup lookup;
    
    public BundleChildFactory(String resource, String moduleName, Lookup lookup) {
        this.resource = resource;
        this.moduleName = moduleName;
        this.lookup = lookup;
    }

    @Override
    protected boolean createKeys(List<String> toPopulate) {
        NbBundleDiff project = lookup.lookup(NbBundleDiff.class);
        Map<String, Properties> resourceChanges = project.getChangedResource(moduleName);
        Properties diff = resourceChanges.get(resource);
        toPopulate.addAll( diff.stringPropertyNames() );
        return true;
    }

    @Override
    protected Node createNodeForKey(String property ) {
        return new PropertyNode(property, resource, moduleName, lookup);
    }
    
}
