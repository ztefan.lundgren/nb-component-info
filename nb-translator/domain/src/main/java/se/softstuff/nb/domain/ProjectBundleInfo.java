package se.softstuff.nb.domain;

/**
 *
 * @author stefan
 */
public class ProjectBundleInfo {
    
    private String rootDir;
    private String type;
    private String version;
    private String name;
    private String displayName;
    private String moduleRef;

    public ProjectBundleInfo() {
    }
    
    public ProjectBundleInfo(String moduleRef) {
        this.moduleRef = moduleRef;
    }

    public String getRootDir() {
        return rootDir;
    }

    public void setRootDir(String rootDir) {
        this.rootDir = rootDir;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getModuleRef() {
        return moduleRef;
    }

    public void setModuleRef(String moduleRef) {
        this.moduleRef = moduleRef;
    }
    
    
    
}
