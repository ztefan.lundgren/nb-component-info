package se.softstuff.nb.domain.tracer;

import java.awt.Color;
import java.awt.Component;
import java.util.ListIterator;
import java.util.Random;
import java.util.logging.Logger;
import org.openide.windows.TopComponent;

/**
 * The keeper of the component and colour 
 * @author stefan
 */
public class HighlightedComponent {

    private static final Logger LOG = Logger.getLogger(HighlightedComponent.class.getName());

    private static final Random random = new Random();

    private final Color color;

    private final Component component;

    private HighlightedComponent parent;

    private HighlightedComponent(Component component, Color color) {
        this.color = color;
        this.component = component;
    }

    public static HighlightedComponent create(Component component, ListIterator<Color> colorStack) {

        if (component == null) {
            return null;
        }

        if (component.getName() == null) {
            component.setName("N/A");
        }
        
        Color color;
        if (!colorStack.hasNext()) {
            color = new Color(random.nextInt());
            colorStack.add(color);
        } else {
            color = colorStack.next();
        }
        HighlightedComponent highlightedComponent = new HighlightedComponent(component, color);

        if (!(component instanceof TopComponent)) {
            highlightedComponent.setParent(create(component.getParent(), colorStack));
        }
        return highlightedComponent;
    }

    public Color getColor() {
        return color;
    }

    public Component getComponent() {
        return component;
    }

    /**
     * the HighlightedComponent object for component.getParent object
     * @return the keeper of the colour
     */
    public HighlightedComponent getParent() {
        return parent;
    }

    private void setParent(HighlightedComponent parent) {
        this.parent = parent;
    }
}
