package se.softstuff.nb.domain.xml;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author Stefan
 */
public class DiffChangesConverterTest {
    
    private DiffChangesConverter instance;
    @Before
    public void setUp() {
        instance = new DiffChangesConverter();
    }

    @Test
    public void testMarshalKey() {
        String clean = "foo.bar";
        assertEquals(clean, instance.marshalKey(clean));
        assertEquals("foo__fslash__bar", instance.marshalKey("foo/bar"));
    }

    @Test
    public void testUnmarshalKey() {
        String clean = "foo.bar";
        assertEquals(clean, instance.marshalKey(clean));
        assertEquals("foo/bar", instance.unmarshalKey("foo__fslash__bar"));
    }
}
