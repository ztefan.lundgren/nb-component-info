/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.domain;

import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.Set;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Stefan
 */
public class NbBundleDiffTest {
    
    
    @Before
    public void setUp() {
    }
    
    @Test
    public void getChangedLanguages() {
        
        NbBundleDiff instance = new NbBundleDiff();
        instance.addResourceChange(
                "se-databyran-prosang-client-donor-administration", 
                "se.databyran.prosang.client.donor.administration.information.g.Bundle_sv_SE.properties",
                "DonorDetailsPanel.nextDonationTypeLabel.text",
                "Kallas till:");
        
        instance.addResourceChange(
                "se-databyran-prosang-client-donor-administration", 
                "se.databyran.prosang.client.donor.administration.information.g.Bundle.properties",
                "DonorDetailsPanel.nextDonationTypeLabel.text",
                "Next donation:");
        
        instance.addResourceChange(
                "se-databyran-prosang-client-donor-administration", 
                "se.databyran.prosang.client.donor.administration.information.g.SQL.properties",
                "DonorDetailsPanel.nextDonationTypeLabel.text",
                "Next donation:");
        Set<String> changedLanguages = instance.getChangedLanguages();
        assertEquals(2,changedLanguages.size());
        assertTrue(changedLanguages.contains("sv_SE"));
        assertTrue(changedLanguages.contains(""));
        
        
    }
    
    /**
     * Test of getVersion method, of class NbBundleDiff.
     */
//    @Test
//    public void testGetVersion() {
//        System.out.println("getVersion");
//        NbBundleDiff instance = new NbBundleDiff();
//        int expResult = 0;
//        int result = instance.getVersion();
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setVersion method, of class NbBundleDiff.
//     */
//    @Test
//    public void testSetVersion() {
//        System.out.println("setVersion");
//        int version = 0;
//        NbBundleDiff instance = new NbBundleDiff();
//        instance.setVersion(version);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of addResourceChange method, of class NbBundleDiff.
//     */
//    @Test
//    public void testAddResourceChange() {
//        System.out.println("addResourceChange");
//        String module = "";
//        String resouce = "";
//        String key = "";
//        String value = "";
//        NbBundleDiff instance = new NbBundleDiff();
//        String expResult = "";
//        String result = instance.addResourceChange(module, resouce, key, value);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of addPropertyChangeListener method, of class NbBundleDiff.
//     */
//    @Test
//    public void testAddPropertyChangeListener() {
//        System.out.println("addPropertyChangeListener");
//        PropertyChangeListener listener = null;
//        NbBundleDiff instance = new NbBundleDiff();
//        instance.addPropertyChangeListener(listener);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of removePropertyChangeListener method, of class NbBundleDiff.
//     */
//    @Test
//    public void testRemovePropertyChangeListener() {
//        System.out.println("removePropertyChangeListener");
//        PropertyChangeListener listener = null;
//        NbBundleDiff instance = new NbBundleDiff();
//        instance.removePropertyChangeListener(listener);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
}
