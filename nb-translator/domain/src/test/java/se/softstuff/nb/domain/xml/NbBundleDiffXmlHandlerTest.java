/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.domain.xml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import org.junit.*;
import se.softstuff.nb.domain.NbBundleDiff;
import static org.junit.Assert.*;
import se.softstuff.nb.domain.ProjectBundleInfo;

/**
 *
 * @author Stefan
 */
public class NbBundleDiffXmlHandlerTest {
    
    private NbBundleDiff testSot;
    private static String testXml;
    private String module = "se-softstuff-nb-GuiComponentInfoDemo-TopComponet";
    
    @BeforeClass
    public static void before() throws IOException, URISyntaxException{
        testXml = fileAsText("refXmlFile.xml");
    }
    
    @Before
    public void setUp() {

        ProjectBundleInfo info = new ProjectBundleInfo();
        info.setRootDir("C:/Users/stefan.DB/Documents/NetBeansProjects/AlfProject/alf");
        info.setName("se.databyran.alf_alf_nbm_1.0-SNAPSHOT");
        info.setDisplayName("alf NetBeans Module");
        info.setModuleRef("se-databyran-alf-alf");
        info.setType("nbm");
        info.setVersion("1.0-SNAPSHOT");
        
        ProjectBundleInfo parent = new ProjectBundleInfo();
        parent.setRootDir("C:/Users/stefan.DB/Documents/NetBeansProjects/AlfProject");
        parent.setName("se.databyran.alf_AlfProject_pom_1.0-SNAPSHOT");
        parent.setDisplayName("AlfProject - NB App Parent");
        parent.setModuleRef("");
        parent.setType("pom");
        parent.setVersion("1.0-SNAPSHOT");

        
        testSot = new NbBundleDiff();
        testSot.setVersion(123);
        testSot.addResourceOriginal(module,"se.softstuff.nb.translate.Bundle_sv_SE.properties", "NbBundleEditXmlHandlerTest.title", "översättaren");
        testSot.addResourceChange(module,"se.softstuff.nb.translate.Bundle_sv_SE.properties", "NbBundleEditXmlHandlerTest.title", "translator");
        
        
        testSot.setKnownProjects(Arrays.asList( info, parent ));
        
        

    }

    @Test
    public void toXml() throws URISyntaxException, IOException {
        String xml = NbBundleDiffXmlHandler.toXml(testSot);
//        writeToFile("testSot.xml", xml);
        assertEquals(testXml, xml);
    }
    
    
    @Test
    public void fromXml() {
        NbBundleDiff sot = NbBundleDiffXmlHandler.fromXml(testXml);
        assertEquals(testSot.getVersion(), sot.getVersion());
        assertEquals(1, testSot.getChangedModulesNames().size());
        assertEquals(testSot.getChangedResource(module).size(), sot.getChangedResource(module).size());
    }

    private static void writeToFile(String fileName, String text) throws URISyntaxException, IOException{
//        URI uri = getClass().getResource(fileName).toURI();
//        File file = new File(uri);
        PrintWriter pw = new PrintWriter("c:/temp/"+fileName, "UTF-8");
        pw.print(text);
        pw.flush();
        pw.close();
    }
    private static String fileAsText(String fileInResource) throws FileNotFoundException, IOException, URISyntaxException {
        URI uri = NbBundleDiffXmlHandlerTest.class.getResource(fileInResource).toURI();
        File file = new File(uri);
        StringBuilder sb = new StringBuilder();
        
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
        String line = null;
        while( (line = br.readLine()) != null){
            if(sb.length() > 0){
                sb.append("\n");
            }
            sb.append(line);
        }
        br.close();
        return sb.toString();
    }
}
