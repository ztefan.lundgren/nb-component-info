/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.domain.tracer;

import se.softstuff.nb.domain.tracer.MarkerUtilities;
import java.util.Arrays;
import java.util.List;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Stefan
 */
public class MarkerUtilitiesTest {
    
    String path;
    
    @Before
    public void setUp() {
        path = "file:/C:/work/guicomponentinfo~svn/GuiComponentInfoDemo/trunk/application/target/guicomponentinfodemo/guicomponentinfodemo/modules/se-softstuff-nb-GuiComponentInfoDemo-TopComponet.jar!/se/softstuff/nb/guicomponentinfodemo/Bundle_soft_sv_SE.properties";
    }

    
    @Test
    public void locateModuleRef(){
        String expResult = "se-softstuff-nb-GuiComponentInfoDemo-TopComponet";
        String result = MarkerUtilities.locateModuleRef(path);
        assertEquals(expResult, result);
    }
    /**
     * Test of findJarFileName method, of class MarkerUtilities.
     */
    @Test
    public void locateJarFileName() {
        System.out.println("findJarFileName");
        String expResult = "se-softstuff-nb-GuiComponentInfoDemo-TopComponet.jar";
        String result = MarkerUtilities.locateJarFileName(path);
        assertEquals(expResult, result);
    }

    /**
     * Test of findJarFile method, of class MarkerUtilities.
     */
    @Test
    public void locateJarFile() {
        System.out.println("findJarFile");
        String expResult = "C:/work/guicomponentinfo~svn/GuiComponentInfoDemo/trunk/application/target/guicomponentinfodemo/guicomponentinfodemo/modules/se-softstuff-nb-GuiComponentInfoDemo-TopComponet.jar";
        String result = MarkerUtilities.locateJarFile(path);
        assertEquals(expResult, result);
    }

    /**
     * Test of findResourseInBinary method, of class MarkerUtilities.
     */
    @Test
    public void locateResourcePath() {
        String expResult = "se/softstuff/nb/guicomponentinfodemo/";
        String result = MarkerUtilities.locateResourcePath(path);
        assertEquals(expResult, result);
    }
    
    @Test
    public void locateResourcePath_notFromJar() {
        String expResult = "se/softstuff/nb/guicomponentinfodemo/";
        String result = MarkerUtilities.locateResourcePath("se/softstuff/nb/guicomponentinfodemo/Bundle_soft_sv_SE.properties");
        assertEquals(expResult, result);
    }
    
    @Test
    public void locateResourceFile(){
        String expResult = "se/softstuff/nb/guicomponentinfodemo/Bundle_soft_sv_SE.properties";
        String result = MarkerUtilities.locateResourceFile(path);
        assertEquals(expResult, result);
    }
    
    @Test
    public void getBundleBrandingAndLocaleString(){
        String expected = "soft_sv_SE";
        String actual = MarkerUtilities.getBundleBrandingAndLocaleString(path);
        assertEquals(expected, actual);
    }
    
    @Test
    public void doResourceMatchSelectedBunlde() {
        
        String resource = "se/softstuff/nb/guicomponentinfodemo/";
        
        String name = "se/softstuff/nb/guicomponentinfodemo/Bundle.properties";        
        assertTrue(MarkerUtilities.doResourceMatchSelectedBunlde(name, resource));
        
        name = "se/softstuff/nb/guicomponentinfodemo/Bundle_en_US.properties";
        assertTrue(MarkerUtilities.doResourceMatchSelectedBunlde(name, resource));
        
        name = "se/softstuff/nb/guicomponentinfodemo/Bundle_gurkan_en_US.properties";
        assertTrue(MarkerUtilities.doResourceMatchSelectedBunlde(name, resource));
        
        name = "se/softstuff/nb/Bundle.properties";
        assertFalse(MarkerUtilities.doResourceMatchSelectedBunlde(name, resource));
        
        name = "se/softstuff/nb/guicomponentinfodemo/Foo.class";
        assertFalse(MarkerUtilities.doResourceMatchSelectedBunlde(name, resource));
    }
    
    
    @Test
    public void locateAllBundlesInResource(){
//        String jarPath = "C:/Temp/se-softstuff-nb-GuiComponentInfoDemo-TopComponet.jar";
//        String resource = "se/softstuff/nb/guicomponentinfodemo/";
//        
//        List<String> bundles = MarkerUtilities.locateAllBundlesInResource(jarPath, resource);
//        
//        assertEquals(2, bundles.size());
//        assertTrue(bundles.contains("se/softstuff/nb/guicomponentinfodemo/Bundle.properties"));
//        assertTrue(bundles.contains("se/softstuff/nb/guicomponentinfodemo/Bundle_sv_SE.properties"));
    }
    
    @Test
    public void locateBundleTypes() {
        List<String> bundles = Arrays.asList("se/softstuff/nb/guicomponentinfodemo/Bundle.properties", "se/softstuff/nb/guicomponentinfodemo/Bundle_sv_SE.properties", "se/softstuff/nb/guicomponentinfodemo/Bundle_branding_sv_SE.properties");
        List<String> bundleTypes = MarkerUtilities.locateBundleTypes(bundles);
        
        assertEquals(3, bundleTypes.size());
        assertTrue(bundleTypes.contains(""));
        assertTrue(bundleTypes.contains("sv_SE"));
        assertTrue(bundleTypes.contains("branding_sv_SE"));
        
        
    }
}
