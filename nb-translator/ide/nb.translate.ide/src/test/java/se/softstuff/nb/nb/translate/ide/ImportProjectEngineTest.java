package se.softstuff.nb.nb.translate.ide;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author stefan
 */
public class ImportProjectEngineTest {
    
    public ImportProjectEngineTest() {
    }
    
    @Before
    public void setUp() {
    }

    @Test
    public void convertRefToFilePath() {
        String ref = "se.databyran.prosang.client.donor.administration.information.g.Bundle_sv_SE.properties";
        String expected = "se/databyran/prosang/client/donor/administration/information/g/Bundle_sv_SE.properties";
        String path = ImportProjectEngine.convertRefToFilePath(ref);
        assertEquals(expected, path);
    }
}
