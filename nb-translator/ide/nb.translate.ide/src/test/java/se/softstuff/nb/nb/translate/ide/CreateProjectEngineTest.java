package se.softstuff.nb.nb.translate.ide;

import java.util.Locale;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.openide.util.editable.BrandedLocale;

/**
 *
 * @author Stefan
 */
public class CreateProjectEngineTest {
    
    private CreateProjectEngine instance;
    
    @Before
    public void setUp() {
        instance = new CreateProjectEngine();
    }

    
    @Test
    public void testCreateModuleIdentifyer() {
        String module = instance.createModuleIdentifyer("se.databyran.alf_branding_nbm_1.0-SNAPSHOT");
        assertEquals("se-databyran-alf-branding", module); 
   }
    
    @Test
    public void testCreateModuleType() {
        String module = instance.createModuleType("se.databyran.alf_branding_nbm_1.0-SNAPSHOT");
        assertEquals("nbm", module); 
   }
    
    @Test
    public void testCreateModuleVersion() {
        String module = instance.createModuleVersion("se.databyran.alf_branding_nbm_1.0-SNAPSHOT");
        assertEquals("1.0-SNAPSHOT", module); 
   }
    
    @Test
    public void testCreateResourceIdentifyer(){
        String resourceRoot = "C:/work/AlfProject/alf/src/main/resources";
        String bundle = "C:/work/AlfProject/alf/src/main/resources/se/databyran/alf/Bundle_foo_sv_SE.properties";
        String actual = instance.createResourceIdentifyer(bundle, resourceRoot);
        String expected = "se.databyran.alf.Bundle_foo_sv_SE.properties";
        assertEquals(expected,actual);
        
        bundle = "C:\\work\\AlfProject\\alf\\src\\main\\resources\\se\\databyran\\alf\\Bundle_foo_sv_SE.properties";
        actual = instance.createResourceIdentifyer(bundle, resourceRoot);
        assertEquals(expected,actual);
    }
    
    @Test
    public void testExpectedLocale(){
        
        assertTrue("default accept all", instance.expectedLocale(""));
        assertTrue("default accept all", instance.expectedLocale("_foo_bar"));
        
        instance.getLocales().add(new BrandedLocale("foo", Locale.UK));
        
        assertFalse("not accepted", instance.expectedLocale(""));
        assertFalse("not accepted", instance.expectedLocale("_foo_bar"));
        
        assertTrue("accepted", instance.expectedLocale("_foo_en_GB"));
        
    }
}