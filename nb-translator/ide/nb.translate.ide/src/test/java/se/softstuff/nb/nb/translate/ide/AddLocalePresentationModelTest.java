package se.softstuff.nb.nb.translate.ide;

import se.softstuff.nb.nb.translate.ide.AddLocalePresentationModel;
import org.openide.util.editable.BrandedLocale;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import org.junit.*;
import static org.junit.Assert.*;

/**
 *
 * @author Stefan
 */
public class AddLocalePresentationModelTest {
    
    
    private AddLocalePresentationModel instance;
    @Before
    public void setUp() {
        instance = new AddLocalePresentationModel();
    }
    
    

    
    @Test
    public void testValidate() {
        
        assertFalse("default no language", instance.isCountryEnable());
        assertFalse("default no language", instance.isVariantEnable());
        assertFalse("default no language", instance.isValidToAdd());
        
        instance.setLanguage("s");        
        assertFalse("not a valid language", instance.isCountryEnable());
        assertFalse("not a valid language", instance.isVariantEnable());
        assertFalse("not a valid language", instance.isValidToAdd());
        
        instance.setLanguage("see");        
        assertFalse("not a valid language", instance.isCountryEnable());
        assertFalse("not a valid language", instance.isVariantEnable());
        assertFalse("not a valid language", instance.isValidToAdd());
        
        instance.setLanguage("se");        
        assertTrue("got a valid language", instance.isCountryEnable());
        assertFalse("not a valid country", instance.isVariantEnable());
        assertTrue("got a valid language", instance.isValidToAdd());
        
        instance.setCountry("S");
        assertTrue("got a valid language", instance.isCountryEnable());
        assertFalse("not a valid country", instance.isVariantEnable());
        assertFalse("not a valid country", instance.isValidToAdd());
        
        instance.setCountry("SVV");
        assertTrue("got a valid language", instance.isCountryEnable());
        assertFalse("not a valid country", instance.isVariantEnable());
        assertFalse("not a valid country", instance.isValidToAdd());
        
        instance.setCountry("SE");
        assertTrue("got a valid language", instance.isCountryEnable());
        assertTrue("got a valid country", instance.isVariantEnable());
        assertTrue("got a valid country", instance.isValidToAdd());
    }
    
    @Test
    public void testHasNullInCopySources(){
        BrandedLocale locale = new BrandedLocale();
        List<BrandedLocale> copy = Arrays.asList(locale);
        
        instance.init(copy);
        assertEquals("has one source", 1, instance.getLanguagesInSource().size());
        assertSame(instance.getLanguagesInSource().iterator().next(), locale);
    }
    
}
