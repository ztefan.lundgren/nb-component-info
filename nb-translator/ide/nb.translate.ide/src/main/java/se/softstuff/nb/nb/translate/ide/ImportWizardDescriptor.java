package se.softstuff.nb.nb.translate.ide;

/**
 *
 * @author stefan
 */
public class ImportWizardDescriptor {
    public static final String FILE = "file";
    public static final String TRANSLATION_PROJECT = "translationProject";
    public static final String SOURCE_PROJECTS = "sourceProject";
    public static final String LOCALES = "locales";
}
