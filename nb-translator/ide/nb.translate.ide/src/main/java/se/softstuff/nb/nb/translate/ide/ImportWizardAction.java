package se.softstuff.nb.nb.translate.ide;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;
import java.util.Map;
import java.util.Set;
import org.netbeans.api.project.Project;
import org.openide.DialogDisplayer;
import org.openide.WizardDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import se.softstuff.nb.domain.NbBundleDiff;

@ActionID(
    category = "Tools/NbTranslate",
id = "se.softstuff.nb.nb.translate.ide.ImportWizardAction")
@ActionRegistration(
    displayName = "#CTL_ImportWizardAction")
@ActionReference(path = "Menu/Tools/NbTranslate", position = -100)
//@Messages("CTL_ImportWizardAction=Import translate project")
public final class ImportWizardAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        WizardDescriptor wiz = new WizardDescriptor(new ImportWizardIterator());
        // {0} will be replaced by WizardDescriptor.Panel.getComponent().getName()
        // {1} will be replaced by WizardDescriptor.Iterator.name()
        wiz.setTitleFormat(new MessageFormat("{0} ({1})"));
        wiz.setTitle("...dialog title...");
        if (DialogDisplayer.getDefault().notify(wiz) == WizardDescriptor.FINISH_OPTION) {
            
            NbBundleDiff translation = (NbBundleDiff)wiz.getProperty(ImportWizardDescriptor.TRANSLATION_PROJECT);
            Set<String> language = (Set<String>)wiz.getProperty(ImportWizardDescriptor.LOCALES);
            Map<String,Project> sourceProjects = (Map<String,Project>)wiz.getProperty(ImportWizardDescriptor.SOURCE_PROJECTS);
            ImportProjectEngine importer = new ImportProjectEngine(translation, language, sourceProjects);
            
            importer.start();
        }
    }
}
