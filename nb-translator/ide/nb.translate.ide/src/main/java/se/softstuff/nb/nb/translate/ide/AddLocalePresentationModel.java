package se.softstuff.nb.nb.translate.ide;

import org.openide.util.editable.BrandedLocaleBuilder;
import org.openide.util.editable.BrandedLocale;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import org.openide.util.NbBundle;

/**
 *
 * @author Stefan
 */
public class AddLocalePresentationModel implements PropertyChangeListener{

    public static final String PROP_BRANDING = "branding";
    public static final String PROP_COUNTRY = "country";
    public static final String PROP_LANGUAGE = "language";
    public static final String PROP_VARIANT = "variant";
    public static final String PROP_VARIANTENABLE = "variantEnable";
    public static final String PROP_COUNTRYENABLE = "countryEnable";
    public static final String PROP_VALIDTOADD = "validToAdd";
    public static final String PROP_LANGUAGES_IN_SOURCE = "languagesInSource";
    public static final String PROP_COPY_FROM = "copyFrom";
    public static final String PROP_COPY_FROM_SOURCES = "copyFromSources";
    public static final String PROP_LOCALES = "locales";
    private static final String SEPARATOR = "_";
    private String branding;
    private String country;
    private String language;
    private String variant;
    private boolean variantEnable;
    private boolean countryEnable;
    private boolean validToAdd;
    private List<BrandedLocale> languagesInSource; 
    private List<BrandedLocale> locales;

    public AddLocalePresentationModel() {
        propertyChangeSupport.addPropertyChangeListener((PropertyChangeListener)this);
        locales = new ArrayList<BrandedLocale>();
    }

    
    void init(Collection<BrandedLocale> languages) {
        setLanguagesInSource(new ArrayList<BrandedLocale>(languages));
        setLocales(new ArrayList<BrandedLocale>(languages));
    }

    public List<BrandedLocale> getLanguagesInSource() {
        return languagesInSource;
    }

    public void setLanguagesInSource(List<BrandedLocale> languagesInSource) {
        List<BrandedLocale> oldLanguagesInSource = this.languagesInSource;
        this.languagesInSource = languagesInSource;
        propertyChangeSupport.firePropertyChange(PROP_LANGUAGES_IN_SOURCE, oldLanguagesInSource, languagesInSource);
    }

    public boolean isValidToAdd() {
        return validToAdd;
    }

    public void setValidToAdd(boolean validToAdd) {
        boolean oldValidToAdd = this.validToAdd;
        this.validToAdd = validToAdd;
        propertyChangeSupport.firePropertyChange(PROP_VALIDTOADD, oldValidToAdd, validToAdd);
    }


    public List<BrandedLocale> getLocales() {
        return locales;
    }

    public void setLocales(List<BrandedLocale> locales) {
        List<BrandedLocale> old = this.locales;
        this.locales = locales;
        propertyChangeSupport.firePropertyChange(PROP_LOCALES, old, locales); 
    }
   
    public boolean isCountryEnable() {
        return countryEnable;
    }

    public void setCountryEnable(boolean countryEnable) {
        boolean oldCountryEnable = this.countryEnable;
        this.countryEnable = countryEnable;
        propertyChangeSupport.firePropertyChange(PROP_COUNTRYENABLE, oldCountryEnable, countryEnable);
    }

    public boolean isVariantEnable() {
        return variantEnable;
    }

    public void setVariantEnable(boolean variantEnable) {
        boolean oldVariantEnable = this.variantEnable;
        this.variantEnable = variantEnable;
        propertyChangeSupport.firePropertyChange(PROP_VARIANTENABLE, oldVariantEnable, variantEnable);
    }

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public String getBranding() {
        return branding;
    }

    public void setBranding(String branding) {
        String oldBranding = this.branding;
        this.branding = branding;
        propertyChangeSupport.firePropertyChange(PROP_BRANDING, oldBranding, branding);
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        String oldLanguage = this.language;
        this.language = language;
        propertyChangeSupport.firePropertyChange(PROP_LANGUAGE, oldLanguage, language);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        String oldCountry = this.country;
        this.country = country;
        propertyChangeSupport.firePropertyChange(PROP_COUNTRY, oldCountry, country);
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        String oldVariant = this.variant;
        this.variant = variant;
        propertyChangeSupport.firePropertyChange(PROP_VARIANT, oldVariant, variant);
    }
    
    

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        
        propertyChangeSupport.removePropertyChangeListener(this);
        
        validateCountry();
        validateVariant();
        validateValidToAdd();
        
        propertyChangeSupport.addPropertyChangeListener(this);
    }

    private void validateVariant() {
        setVariantEnable(countryEnable && country != null && country.trim().length() == 2);
        if(!isVariantEnable()){
            setVariant("");
        }
    }

    private void validateCountry() {
        setCountryEnable(language != null && language.trim().length() == 2);
        if(!isCountryEnable()){
            setCountry("");
            setVariant("");
        }
    }

    private void validateValidToAdd() {
        boolean enable = countryEnable; // language is ok
        
        if(enable && !country.trim().isEmpty()){ // country is not empty
            enable = country.trim().length() == 2; // is country ok
        }
        
        setValidToAdd(enable);
        
    }


    
    
    
    BrandedLocale addToList(){
        BrandedLocaleBuilder builder = new BrandedLocaleBuilder();
        builder.setBranding(branding);
        builder.setLanguage(language);
        builder.setCountry(country);
        builder.setVariant(variant);
        
        BrandedLocale toAdd = builder.create();
        if(locales.contains(toAdd)){
            return null;
        }
        
        locales.add(toAdd);
        
        setBranding("");//NOI18N
        setLanguage("");//NOI18N
        setCountry("");//NOI18N
        setVariant(""); //NOI18N
        return toAdd;
    }
    
}
