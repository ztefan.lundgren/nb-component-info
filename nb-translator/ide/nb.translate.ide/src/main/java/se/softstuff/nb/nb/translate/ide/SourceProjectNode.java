package se.softstuff.nb.nb.translate.ide;

import java.awt.Image;
import javax.swing.Action;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectInformation;
import org.netbeans.api.project.ProjectUtils;
import org.openide.actions.DeleteAction;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.ImageUtilities;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author stefan
 */
public class SourceProjectNode extends AbstractNode {

    public SourceProjectNode(Project project) {
        super(Children.LEAF, Lookups.singleton(project));

      setDisplayName(getProjectInformation().getDisplayName());
        setName(getProjectInformation().getName());
    }

    Project getProject() {
        return getLookup().lookup(Project.class);
    }

    private ProjectInformation getProjectInformation() {
        return ProjectUtils.getInformation(getProject());
    }

    @Override
    public Image getIcon(int type) {
        return ImageUtilities.icon2Image(getProjectInformation().getIcon());
    }

    @Override
    public Image getOpenedIcon(int type) {
        return getIcon(type);
    }
    
    @Override
    public Action[] getActions(boolean context) {
        return new Action[]{
                    SystemAction.get(DeleteAction.class)};
    }
    
    @Override
    public boolean canDestroy() {
        return true;
    }
}
