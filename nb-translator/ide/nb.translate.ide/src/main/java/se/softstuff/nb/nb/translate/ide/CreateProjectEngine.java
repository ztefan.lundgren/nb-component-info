package se.softstuff.nb.nb.translate.ide;

import org.openide.util.editable.BrandedLocaleBuilder;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.log4j.Logger;
import org.netbeans.api.java.project.JavaProjectConstants;
import org.netbeans.api.project.*;
import org.openide.WizardDescriptor;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.util.EditableProperties;
import org.openide.util.editable.BrandedLocale;
import se.softstuff.nb.domain.NbBundleDiff;
import se.softstuff.nb.domain.ProjectBundleInfo;
import se.softstuff.nb.domain.xml.NbBundleDiffXmlHandler;

/**
 *
 * @author Stefan
 */
public class CreateProjectEngine {

    final static String BUNDLE_REGEXP = "bundle.*\\.properties";
    
    final static String MODULE_PATH_DEVICER = "_";

    static CreateProjectEngine create(WizardDescriptor wiz) {
        Set<Project> projects = (Set<Project>) wiz.getProperty(CreateWizardDescriptor.PROJECTS);
        List<BrandedLocale> locales = (List<BrandedLocale>) wiz.getProperty(CreateWizardDescriptor.LOCALES);
        String destination = (String) wiz.getProperty(CreateWizardDescriptor.DESTINATION);

        return new CreateProjectEngine(projects, locales, destination);
    }
    private Logger log = Logger.getLogger(CreateProjectEngine.class);

    private Set<Project> projects;

    private List<BrandedLocale> locales;

    private String destination;

    private NbBundleDiff export;

    public CreateProjectEngine(Set<Project> projects, List<BrandedLocale> locales, String destination) {
        this.projects = projects;
        this.locales = locales;
        this.destination = destination;
    }

    List<BrandedLocale> getLocales() {
        return locales;
    }
    
    

    // for testability
    CreateProjectEngine() {
        this(new HashSet<Project>(), new ArrayList<BrandedLocale>(), ".");
    }

    public void start() {

        export = new NbBundleDiff();
        export.setVersion(1);
        createProjectToBinnaryMap(export, projects);

        for (Project project : projects) {
            extractOriginalBundleValues(project);
        }
        createExportFile();
    }

    private void traverseResources(String resourceRoot, FileObject file, Project project, SourceGroup resources) {
        if (isBundleFile(file) && expectedLocale(file.getName())) {
            extract(resourceRoot, file, project);
        }
        for (FileObject child : file.getChildren()) {
            traverseResources(resourceRoot, child, project, resources);
        }
    }

    static boolean isBundleFile(FileObject file) {
        return file.getNameExt().toLowerCase().matches(BUNDLE_REGEXP);
    }
    
    boolean expectedLocale(String fileName) {
        if(locales.isEmpty()){
            return true;
        }
        for(BrandedLocale locale : locales) {
            if(fileName.endsWith(locale.getFilePart())) {
                return true;
            }
        }
        return false;
    }

    private void extract(String resourceRoot, FileObject file, Project project) {
        ProjectInformation information = ProjectUtils.getInformation(project);
        String name = information.getName();

        String module = createModuleIdentifyer(name);

        String resource = createResourceIdentifyer(file.getPath(), resourceRoot);
        BrandedLocale locale = BrandedLocaleBuilder.parse(file.getName());

        EditableProperties props = new EditableProperties(true);

        try {
            props.load(file.getInputStream());
        } catch (IOException ex) {
            throw new RuntimeException("failed to load properties from file: " + file.getPath(), ex);
        }
        for (Entry<String, String> entry : props.entrySet()) {
            export.addResourceOriginal(module, resource, entry.getKey(), entry.getValue());
        }
    }

    private FileObject getDefaultLocaleFile(FileObject file, BrandedLocale defaltLocale) {
        return file.getParent().getFileObject(BrandedLocaleBuilder.createBundle(defaltLocale, true));
    }

    private boolean containBrother(FileObject file, BrandedLocale defaltLocale) {
        String defaultResource = BrandedLocaleBuilder.createBundle(defaltLocale, true);
        for (FileObject brother : file.getParent().getChildren()) {
            if (brother.getNameExt().equals(defaultResource)) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param projectName expects project name eq
     * "se.databyran.alf_branding_nbm_1.0-SNAPSHOT"
     * @return runtime module namen "se-databyran-alf-branding" in a xml correct
     * format   "se.databyran.prosang.components_prosang-util_jar_2011.7-SNAPSHOT" "se.databyran.prosang.server_ejb_ejb_2011.7-SNAPSHOT"
     */
    String createModuleIdentifyer(String projectName) {
//        int end = projectName.lastIndexOf("_nbm");
//        if (end == -1) {
//            end = projectName.lastIndexOf("_jar");
//        }
//        if (end == -1) {
//            end = projectName.lastIndexOf("_ejb");
//        }
//        if (end == -1) {
//            end = projectName.lastIndexOf("_war");
//        }
        int end = projectName.lastIndexOf(MODULE_PATH_DEVICER);
        String exclVersion = projectName.substring(0, end);
        end = exclVersion.lastIndexOf(MODULE_PATH_DEVICER);
        if (end == -1) {
            log.error("Failed to get module identifyer from: " + projectName);
            return "";
        }
        
        
        
        String module = projectName.substring(0, end);
        module = module.replaceAll("\\.", "-");
        module = module.replaceAll("_", "-");
        return module;
    }
    
    String createModuleType(String projectName) {
        int end = projectName.lastIndexOf(MODULE_PATH_DEVICER);
        String exclVersion = projectName.substring(0, end);
        int begin = exclVersion.lastIndexOf(MODULE_PATH_DEVICER)+1;
        String type = projectName.substring(begin, end);
        return type;
    }
    
    String createModuleVersion(String projectName) {
        int begin = projectName.lastIndexOf(MODULE_PATH_DEVICER)+1;
        String version = projectName.substring(begin);
        return version;
    }

    String createResourceIdentifyer(String bundlePath, String resourceRoot) {
        String resource = bundlePath.substring(resourceRoot.length() + 1);
        resource = resource.replaceAll("/", ".");
        resource = resource.replaceAll("\\\\", ".");
        return resource;
    }

    private void extractOriginalBundleValues(Project project) {
        Sources sources = ProjectUtils.getSources(project);
        SourceGroup[] sourceGroups = sources.getSourceGroups(JavaProjectConstants.SOURCES_TYPE_RESOURCES);

        for (SourceGroup resources : sourceGroups) {
            traverseResources(resources.getRootFolder().getPath(), resources.getRootFolder(), project, resources);
        }
    }

    private void createExportFile() throws RuntimeException {
        try {
            File exportFile = new File(destination);
            exportFile.createNewFile();
            FileObject exportFileObject = FileUtil.toFileObject(exportFile);
            OutputStream outputStream = exportFileObject.getOutputStream();
//            OutputStreamWriter writer = new OutputStreamWriter(outputStream, NbBundleDiffXmlHandler.UTF8);
            NbBundleDiffXmlHandler.toXml(export, outputStream);
            outputStream.close();
        } catch (IOException ex) {
            throw new RuntimeException("failed to create export file " + destination, ex);
        }
    }

    private void createProjectToBinnaryMap(NbBundleDiff export, Set<Project> projects) {
        
        List<ProjectBundleInfo> projectInfos = new ArrayList<ProjectBundleInfo>(projects.size());
        for (Project project : projects) {
            ProjectInformation information = ProjectUtils.getInformation(project);
            String projectId = information.getName();
            ProjectBundleInfo projectInfo = new ProjectBundleInfo();
            
            projectInfo.setRootDir(project.getProjectDirectory().getPath());
            projectInfo.setName(information.getName());
            projectInfo.setDisplayName(information.getDisplayName());
            projectInfo.setModuleRef(createModuleIdentifyer(projectId));
            projectInfo.setType(createModuleType(projectId));
            projectInfo.setVersion(createModuleVersion(projectId));
            
            projectInfos.add(projectInfo);
        }
        export.setKnownProjects(projectInfos);
    }
}
