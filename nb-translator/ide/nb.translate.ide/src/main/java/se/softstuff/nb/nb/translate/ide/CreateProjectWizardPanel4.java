package se.softstuff.nb.nb.translate.ide;

import org.openide.util.editable.BrandedLocale;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.swing.event.ChangeListener;
import org.netbeans.api.project.Project;
import org.openide.WizardDescriptor;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;

public class CreateProjectWizardPanel4 implements WizardDescriptor.Panel<WizardDescriptor> {
    public static final String NEW_LINE = "<BR/>";

    /**
     * The visual component that displays this panel. If you need to access the
     * component from this class, just use getComponent().
     */
    private CreateProjectVisualPanel4 component;

    // Get the visual component for the panel. In this template, the component
    // is kept separate. This can be more efficient: if the wizard is created
    // but never displayed, or not all panels are displayed, it is better to
    // create only those which really need to be visible.
    @Override
    public CreateProjectVisualPanel4 getComponent() {
        if (component == null) {
            component = new CreateProjectVisualPanel4();
        }
        return component;
    }

    @Override
    public HelpCtx getHelp() {
        // Show no Help button for this panel:
        return HelpCtx.DEFAULT_HELP;
        // If you have context help:
        // return new HelpCtx("help.key.here");
    }

    @Override
    public boolean isValid() {
        // If it is always OK to press Next or Finish, then:
        return true;
        // If it depends on some condition (form filled out...) and
        // this condition changes (last form field filled in...) then
        // use ChangeSupport to implement add/removeChangeListener below.
        // WizardDescriptor.ERROR/WARNING/INFORMATION_MESSAGE will also be useful.
    }

    @Override
    public void addChangeListener(ChangeListener l) {
    }

    @Override
    public void removeChangeListener(ChangeListener l) {
    }

    @Override
    public void readSettings(WizardDescriptor wiz) {
        // use wiz.getProperty to retrieve previous panel state
        Set<Project> projects = (Set<Project>)wiz.getProperty(CreateWizardDescriptor.PROJECTS);
        List<BrandedLocale> locales = (List<BrandedLocale>)wiz.getProperty(CreateWizardDescriptor.LOCALES);
        String destination = (String)wiz.getProperty(CreateWizardDescriptor.DESTINATION);
        
        
        StringBuilder sb = new StringBuilder("<html>"); //NOI18N
        sb.append(NbBundle.getMessage(CreateProjectWizardPanel4.class, "CreateProjectWizardPanel4.projects", projects.size())).append(NEW_LINE).append(NEW_LINE); //NOI18N
        sb.append(NbBundle.getMessage(CreateProjectWizardPanel4.class, "CreateProjectWizardPanel4.locales")).append(NEW_LINE); //NOI18N
        for(BrandedLocale lang : locales) {
            sb.append("&nbsp;&nbsp;&nbsp;&nbsp;").append(lang); //NOI18N
            sb.append(NEW_LINE);
        }
        sb.append(NEW_LINE);
        sb.append(NbBundle.getMessage(CreateProjectWizardPanel4.class, "CreateProjectWizardPanel4.destination", destination)); //NOI18N
        
        component.setSummary(sb.toString());
    }

    @Override
    public void storeSettings(WizardDescriptor wiz) {
        // use wiz.putProperty to remember current panel state
    }
}
