package se.softstuff.nb.nb.translate.ide;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.apache.log4j.Logger;
import org.netbeans.api.java.project.JavaProjectConstants;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.netbeans.api.queries.FileEncodingQuery;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.util.Exceptions;
import se.softstuff.nb.domain.NbBundleDiff;
import se.softstuff.nb.domain.ProjectBundleInfo;
import se.softstuff.nb.domain.tracer.MarkerUtilities;

/**
 *
 * @author stefan
 */
public class ImportProjectEngine {
    private static final String KEY_VALUE_DELIMETER = "=";

    private Logger log = Logger.getLogger(ImportProjectEngine.class);
    
    private final NbBundleDiff translation;
    private final Set<String> language;
    private final Map<String, Project> sourceProjects;

    ImportProjectEngine(NbBundleDiff translation, Set<String> language, Map<String, Project> sourceProjects) {
        this.translation = translation;
        this.language = language;
        this.sourceProjects = sourceProjects;
    }

    public void start() {
        log.info("Begin import translations");
        
        Set<String> modules = translation.getChangedModulesNames();
        for(String moduleRef : modules) {
            ProjectBundleInfo knownInfo = translation.getKnownProjectMap().get(moduleRef);
            Project project = sourceProjects.get(knownInfo.getName());
            if(project==null){
                log.debug(String.format("Ignore project %s, project was not open",moduleRef));
                continue;
            }
            
            Map<String, Properties> changedResource = translation.getChangedResource(moduleRef);
            
            for(String bundleRef : changedResource.keySet()) {
                String lang = MarkerUtilities.getBundleBrandingAndLocaleString(bundleRef);
                if(lang == null && !language.contains(lang)){
                    log.debug(String.format("Ignore bundle %s, not allowed language",bundleRef));
                    continue;    
                }
                Properties original = getOriginal(moduleRef, bundleRef);
                Properties changes = changedResource.get(bundleRef);
                
                if(changes.isEmpty()){
                    log.debug(String.format("Ignore bundle %s, no changes was detected",bundleRef));
                    continue;  
                }
                importDiff(bundleRef, changes, original ,project);                
            }
            
        }
    }

    private Properties getOriginal(String moduleRef, String bundleRef) {
        Map<String, Properties> originalResource = translation.getOriginalResource(moduleRef);
        if(originalResource != null) {
            Properties original = originalResource.get(bundleRef);
            return original;
        }
        return new Properties();
    }

    private void importDiff(String bundleRef, Properties changes, Properties original, Project project) {
        
        FileObject bundleFile = findFile(bundleRef, project);
        if(bundleFile == null){
            log.error(String.format("Failed to find file for bundleRef:%s failed to update %s values", bundleRef, changes.size()));
        }
        FileLock lock = null;
        try {
            lock = bundleFile.lock();
        } catch (IOException ex) {
            log.error(String.format("Failed to lock bundle file: %s",bundleFile.getPath()));
            return;
        }
        
        try {
            Charset encoding = FileEncodingQuery.getEncoding(bundleFile);
            List<String> lines = bundleFile.asLines(encoding.name());
            PrintStream out = new PrintStream(bundleFile.getOutputStream(lock),false, encoding.name());
            try {
                for(String line : lines){
                    int eq = line.indexOf(KEY_VALUE_DELIMETER);
                    if(eq == -1){
                        out.println(line);
                    }
                    String key = line.substring(0,eq);
                    String value = line.substring(eq+1);

                    if(changes.containsKey(key)){
                        value = changes.getProperty(key);
                    }
                    out.print(key);
                    out.print(KEY_VALUE_DELIMETER);
                    out.println(value);
                }
            }finally{
                out.flush();
                out.close();
            }
        } catch (IOException ex) {
            lock.releaseLock();
        }
    }
    
    private FileObject findFile(String bundleRef, Project project){
        
        String relativeFilePath = convertRefToFilePath(bundleRef);
        
        Sources sources = ProjectUtils.getSources(project);
        
        SourceGroup[] sourceGroups = sources.getSourceGroups(JavaProjectConstants.SOURCES_TYPE_RESOURCES);

        for (SourceGroup resources : sourceGroups) {
            FileObject fileObject = resources.getRootFolder().getFileObject(relativeFilePath);
            if(fileObject != null) {
                return fileObject;
            }
        }
        return null;
    }

    static String convertRefToFilePath(String bundleRef) {
        
        int lastDot = bundleRef.lastIndexOf(".");
        String replace = bundleRef.replace(".", "/");
        StringBuilder path = new StringBuilder(replace);
        path.setCharAt(lastDot, '.');
        return path.toString();
    }
    
}
