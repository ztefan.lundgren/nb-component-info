package se.softstuff.nb.nb.translate.ide;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.openide.DialogDisplayer;
import org.openide.WizardDescriptor;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.util.NbBundle;

// An example action demonstrating how the wizard could be called from within
// your code. You can move the code below wherever you need, or register an action:
// @ActionID(category="...", id="se.softstuff.nb.nb.tranlate.ide.CreateProjectWizardAction")
// @ActionRegistration(displayName="Open CreateProject Wizard")
// @ActionReference(path="Menu/Tools", position=...)
@ActionID(category = "Tools/NbTranslate",
id = "se.softstuff.nb.nb.translate.ide.CreateProjectWizardAction")
@ActionRegistration(displayName = "#CTL_CreateProjectWizardAction")
@ActionReferences({
    @ActionReference(path = "Menu/Tools/NbTranslate", position = 0)
})
//@NbBundle.Messages("CTL_CreateProjectWizardAction=Create NbTranslate project")
public final class CreateProjectWizardAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        
        for(Project proj : OpenProjects.getDefault().getOpenProjects()){
            if(!OpenProjects.getDefault().isProjectOpen(proj)){
                return;
            }
        }
        
        
        List<WizardDescriptor.Panel<WizardDescriptor>> panels = new ArrayList<WizardDescriptor.Panel<WizardDescriptor>>();
        panels.add(new CreateProjectWizardPanel1());
        panels.add(new CreateProjectWizardPanel2());
        panels.add(new CreateProjectWizardPanel3());
        panels.add(new CreateProjectWizardPanel4());
        String[] steps = new String[panels.size()];
        for (int i = 0; i < panels.size(); i++) {
            Component c = panels.get(i).getComponent();
            // Default step name to component name of panel.
            steps[i] = c.getName();
            if (c instanceof JComponent) { // assume Swing components
                JComponent jc = (JComponent) c;
                jc.putClientProperty(WizardDescriptor.PROP_CONTENT_SELECTED_INDEX, i);
                jc.putClientProperty(WizardDescriptor.PROP_CONTENT_DATA, steps);
                jc.putClientProperty(WizardDescriptor.PROP_AUTO_WIZARD_STYLE, true);
                jc.putClientProperty(WizardDescriptor.PROP_CONTENT_DISPLAYED, true);
                jc.putClientProperty(WizardDescriptor.PROP_CONTENT_NUMBERED, true);
            }
        }
        WizardDescriptor wiz = new WizardDescriptor(new WizardDescriptor.ArrayIterator<WizardDescriptor>(panels));
        // {0} will be replaced by WizardDesriptor.Panel.getComponent().getName()
        wiz.setTitleFormat(new MessageFormat("{0}"));
        wiz.setTitle(NbBundle.getMessage(CreateProjectWizardAction.class, "CreateProjectWizardAction.wizard.title"));
        if (DialogDisplayer.getDefault().notify(wiz) == WizardDescriptor.FINISH_OPTION) {
            
            
            CreateProjectEngine engine = CreateProjectEngine.create(wiz);
            engine.start();
            
        }
    }
}
