package se.softstuff.nb.nbdevbundlefinder;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;

/**
 *
 * @author stefan
 */
public class BundleDiffTest {
    

    @Test
    public void replaseSlaches() {
        assertEquals("se.softstuff.Bundle.properties", "se\\softstuff/Bundle.properties".replaceAll(BundleDiff.TITLE_REPLACE_REGEXP, "\\."));
    }
}
