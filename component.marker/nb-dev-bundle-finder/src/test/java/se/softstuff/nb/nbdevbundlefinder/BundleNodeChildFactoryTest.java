package se.softstuff.nb.nbdevbundlefinder;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author stefan
 */
public class BundleNodeChildFactoryTest {
    

    @Test
    public void matchBundle() {
        assertTrue("no match", "bundle.properties".matches(ProjectNodeChildFactory.BUNDLE_REGEXP));
        assertTrue("no match", "bundle_fnurr.properties".matches(ProjectNodeChildFactory.BUNDLE_REGEXP));
        assertTrue("no match", "bundle_fnurr_en.properties".matches(ProjectNodeChildFactory.BUNDLE_REGEXP));
        assertTrue("no match", "bundle_fnurr_en_UK.properties".matches(ProjectNodeChildFactory.BUNDLE_REGEXP));
    }
}
