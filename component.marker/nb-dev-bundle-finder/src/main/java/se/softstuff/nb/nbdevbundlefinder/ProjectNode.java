package se.softstuff.nb.nbdevbundlefinder;

import java.awt.Image;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.Action;
import org.netbeans.api.java.project.JavaProjectConstants;
import org.netbeans.api.project.*;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.ImageUtilities;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author stefan
 */
public class ProjectNode extends AbstractNode {
    
    public ProjectNode(Project project){
        super(Children.create(new ProjectNodeChildFactory(project),true), Lookups.singleton(project));
        ProjectInformation info = getProjectInformation();
        
        setDisplayName(info.getDisplayName());
    }

    @Override
    public Action[] getActions(boolean context) {
        List<Action> actions = new ArrayList<Action>(Arrays.asList(super.getActions(context)));
        actions.add(new OpenProjectAction(getLookup()));
        return actions.toArray(new Action[0]);
    }
    
    @Override
    public Image getIcon(int type) {
        return ImageUtilities.icon2Image(getProjectInformation().getIcon());
    }
    
    @Override
    public Image getOpenedIcon(int type) {
        return getIcon(type);
    }
    
    private Project getProject(){
        return getLookup().lookup(Project.class);
    }
    
    private ProjectInformation getProjectInformation() {
        return ProjectUtils.getInformation(getProject());
    }
    
    
    
}
