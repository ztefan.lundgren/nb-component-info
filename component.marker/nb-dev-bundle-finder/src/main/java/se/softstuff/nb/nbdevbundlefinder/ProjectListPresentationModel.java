package se.softstuff.nb.nbdevbundlefinder;

import java.beans.PropertyChangeSupport;
import se.softstuff.nb.domain.NbBundleDiffDataObject;

/**
 *
 * @author Stefan
 */
public class ProjectListPresentationModel {
    
    public static final String PROP_BUNDLE_DIFF = "bundleDiff";
    
    private NbBundleDiffDataObject bundleDiff;
    
    private transient final PropertyChangeSupport propertyChangeSupport;

    public ProjectListPresentationModel() {
        propertyChangeSupport = new PropertyChangeSupport(this);
    }
    
    
    public NbBundleDiffDataObject getBundleDiff() {
        return bundleDiff;
    }

    public void setBundleDiff(NbBundleDiffDataObject bundleDiff) {
        NbBundleDiffDataObject oldBundleDiff = this.bundleDiff;
        this.bundleDiff = bundleDiff;
        propertyChangeSupport.firePropertyChange(PROP_BUNDLE_DIFF, oldBundleDiff, bundleDiff);
    }
    
}
