package se.softstuff.nb.nbdevbundlefinder;

import java.beans.PropertyVetoException;
import java.io.File;
import java.util.*;
import java.util.concurrent.ExecutionException;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.JFileChooser;
import javax.swing.SwingWorker;
import javax.swing.text.DefaultEditorKit;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.netbeans.api.project.*;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.api.settings.ConvertAsProperties;
import org.netbeans.spi.project.SubprojectProvider;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.explorer.view.BeanTreeView;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.TopComponent;
import org.openide.util.NbBundle.Messages;
import org.netbeans.modules.project.uiapi.Utilities;
import org.netbeans.spi.project.ui.support.ProjectChooser;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.util.lookup.ProxyLookup;
import se.softstuff.nb.domain.NbBundleDiffDataObject;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(dtd = "-//se.softstuff.nb.nbdevbundlefinder//ProjectList//EN",
autostore = false)
@TopComponent.Description(preferredID = "ProjectListTopComponent",
//iconBase="SET/PATH/TO/ICON/HERE", 
persistenceType = TopComponent.PERSISTENCE_NEVER)
@TopComponent.Registration(mode = "editor", openAtStartup = true)
@ActionID(category = "Window", id = "se.softstuff.nb.nbdevbundlefinder.ProjectListTopComponent")
@ActionReference(path = "Menu/Window" /*
 * , position = 333
 */)
@TopComponent.OpenActionRegistration(displayName = "#CTL_ProjectListAction",
preferredID = "ProjectListTopComponent")
@Messages({
    "CTL_ProjectListAction=ProjectList",
    "CTL_ProjectListTopComponent=ProjectList Window",
    "HINT_ProjectListTopComponent=This is a ProjectList window"
})
public final class ProjectListTopComponent extends TopComponent implements ExplorerManager.Provider {

    private ExplorerManager sourceManager;
    private ExplorerManager diffManager;
    private Lookup lookup;
    private Action copyAction;
    private FilterdProjectFactory mappedProjects;

    public ProjectListTopComponent() {
        initComponents();
        setName(Bundle.CTL_ProjectListTopComponent());
        setToolTipText(Bundle.HINT_ProjectListTopComponent());

        sourceManager = new ExplorerManager();
        ActionMap map = getActionMap();
        copyAction = ExplorerUtils.actionCopy(sourceManager);
        map.put(DefaultEditorKit.copyAction, copyAction);
        map.put(DefaultEditorKit.cutAction, ExplorerUtils.actionCut(sourceManager));
        map.put(DefaultEditorKit.pasteAction, ExplorerUtils.actionPaste(sourceManager));
        map.put("delete", ExplorerUtils.actionDelete(sourceManager, true)); // or false

        Lookup sourceLookup = ExplorerUtils.createLookup(sourceManager, map);

        diffManager = new ExplorerManager();
        ActionMap diffMap = getActionMap();
        diffMap.put(DefaultEditorKit.copyAction, ExplorerUtils.actionCopy(diffManager));
        diffMap.put(DefaultEditorKit.cutAction, ExplorerUtils.actionCut(diffManager));
        diffMap.put(DefaultEditorKit.pasteAction, ExplorerUtils.actionPaste(diffManager));
        diffMap.put("delete", ExplorerUtils.actionDelete(diffManager, true)); // or false

        Lookup diffLookup = ExplorerUtils.createLookup(diffManager, diffMap);

        lookup = new ProxyLookup(sourceLookup, diffLookup);

        associateLookup(lookup);

       
    }
    
    public Lookup getLookup() {
        return lookup;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        model = new se.softstuff.nb.nbdevbundlefinder.ProjectListPresentationModel();
        projectTree = new BeanTreeView();
        jPanel2 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        hasBundlesCheckBox = new javax.swing.JCheckBox();
        hasDiffCheckBox = new javax.swing.JCheckBox();
        jButton1 = new javax.swing.JButton();
        openProjectsButton = new javax.swing.JButton();
        diffTree = new BeanTreeView();
        translateProjectTextField = new javax.swing.JTextField();
        translateProjectButton = new javax.swing.JButton();

        org.openide.awt.Mnemonics.setLocalizedText(jButton2, "Clear ");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Filters"));

        org.openide.awt.Mnemonics.setLocalizedText(hasBundlesCheckBox, "Has Bundles");
        hasBundlesCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                hasBundlesCheckBoxActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(hasDiffCheckBox, "Has Diff");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(hasBundlesCheckBox, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE)
            .addComponent(hasDiffCheckBox, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(hasBundlesCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(hasDiffCheckBox)
                .addGap(0, 15, Short.MAX_VALUE))
        );

        org.openide.awt.Mnemonics.setLocalizedText(jButton1, "Add project");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(openProjectsButton, "Add open projects");
        openProjectsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openProjectsButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(openProjectsButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(openProjectsButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.openide.awt.Mnemonics.setLocalizedText(translateProjectButton, "Translate project");
        translateProjectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                translateProjectButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(projectTree)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(244, 244, 244)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(diffTree, javax.swing.GroupLayout.PREFERRED_SIZE, 312, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(translateProjectTextField)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(translateProjectButton)))
                .addContainerGap(179, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(93, 93, 93)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(diffTree)
                    .addComponent(projectTree, javax.swing.GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(translateProjectTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(translateProjectButton)))
                .addContainerGap(20, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        JFileChooser projectChooser = ProjectChooser.projectChooser();
        projectChooser.setCurrentDirectory(new File("C:/work/prosang"));
        projectChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        projectChooser.showOpenDialog(null);
        final File projectToBeOpenedFile = projectChooser.getSelectedFile();
        if (projectToBeOpenedFile == null) {
            return;
        }

        final ProgressHandle progress = ProgressHandleFactory.createHandle("Loading projects");
        SwingWorker worker = new SwingWorker<List<Project>, Object>() {

            @Override
            protected List<Project> doInBackground() throws Exception {
                progress.start();
                List<Project> projects = new ArrayList<Project>();
                FileObject projectToBeOpened = FileUtil.toFileObject(projectToBeOpenedFile);
                Project project = ProjectManager.getDefault().findProject(projectToBeOpened);

                if (project != null) {
                    projects.add(project);

                    SubprojectProvider subProvider = project.getLookup().lookup(SubprojectProvider.class);
                    if (subProvider != null) {
                        Set<? extends Project> subprojects = subProvider.getSubprojects();
                        projects.addAll(subprojects);
                    }
                }
                return projects;
            }

            @Override
            protected void done() {
                try {
                    mappedProjects.add(get());
                } catch (InterruptedException ex) {
                    Exceptions.printStackTrace(ex);
                } catch (ExecutionException ex) {
                    Exceptions.printStackTrace(ex);
                }
                progress.finish();
            }
        };
        worker.execute();


    }//GEN-LAST:event_jButton1ActionPerformed

    private void openProjectsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openProjectsButtonActionPerformed
        Project[] openProjects = OpenProjects.getDefault().getOpenProjects();
        this.mappedProjects.add(openProjects);
    }//GEN-LAST:event_openProjectsButtonActionPerformed

    private void hasBundlesCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_hasBundlesCheckBoxActionPerformed
        mappedProjects.setFilterHasBundle(hasBundlesCheckBox.isSelected());
    }//GEN-LAST:event_hasBundlesCheckBoxActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        mappedProjects.removeAll();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void translateProjectButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_translateProjectButtonActionPerformed

        JFileChooser projectChooser = new JFileChooser();
        projectChooser.setCurrentDirectory(new File("C:/work/prosang"));
        projectChooser.setFileFilter(new MimeTypeFileFilter(NbBundleDiffDataObject.MIME_TYPE, "translate project"));
        projectChooser.showOpenDialog(this);
        final File projectToBeOpenedFile = projectChooser.getSelectedFile();
        if (projectToBeOpenedFile == null) {
            return;
        }
        FileObject toFileObject = FileUtil.toFileObject(projectToBeOpenedFile);
        try {
            DataObject data = DataObject.find(toFileObject);
            NbBundleDiffDataObject translateDiffs = (NbBundleDiffDataObject) data;
            model.setBundleDiff(translateDiffs);
            fillDiffTree();

        } catch (DataObjectNotFoundException ex) {
            Exceptions.printStackTrace(ex);
        }


    }//GEN-LAST:event_translateProjectButtonActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane diffTree;
    private javax.swing.JCheckBox hasBundlesCheckBox;
    private javax.swing.JCheckBox hasDiffCheckBox;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private se.softstuff.nb.nbdevbundlefinder.ProjectListPresentationModel model;
    private javax.swing.JButton openProjectsButton;
    private javax.swing.JScrollPane projectTree;
    private javax.swing.JButton translateProjectButton;
    private javax.swing.JTextField translateProjectTextField;
    // End of variables declaration//GEN-END:variables

    @Override
    public void componentOpened() {
         fillSourceTree();

        fillDiffTree();
    }

    @Override
    public void componentClosed() {
        // TODO add custom code on component closing
    }
    
    

    @Override
    public ExplorerManager getExplorerManager() {
        return diffManager;
    }
    

    
    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings
    }

    void readProperties(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version
    }

    private void fillSourceTree() {
        try {
            this.mappedProjects = new FilterdProjectFactory();
            Node rootNode = new AbstractNode(mappedProjects);
            rootNode.setDisplayName("Projects");

            sourceManager.setRootContext(rootNode);
            sourceManager.setSelectedNodes(new Node[]{rootNode});

        } catch (PropertyVetoException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    private void fillDiffTree() {

        NbBundleDiffDataObject bundleDiff = model.getBundleDiff();
        if (bundleDiff != null) {
            diffManager.setRootContext(bundleDiff.createProjectNode());
        }

    }
}
