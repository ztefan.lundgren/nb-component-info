package se.softstuff.nb.nbdevbundlefinder;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.openide.util.Lookup;

/**
 *
 * @author stefan
 */
public class OpenProjectAction extends AbstractAction {

    
    private Lookup lookup;

    public OpenProjectAction(Lookup lookup) {
        super("Open project");
        this.lookup = lookup;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        Project project = lookup.lookup(Project.class);
        OpenProjects.getDefault().open(new Project[]{project}, false);
    }
    
    
}
