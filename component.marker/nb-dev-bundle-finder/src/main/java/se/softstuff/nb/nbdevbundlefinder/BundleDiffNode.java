package se.softstuff.nb.nbdevbundlefinder;

import javax.swing.Action;
import org.openide.actions.EditAction;
import org.openide.actions.OpenAction;
import org.openide.cookies.EditCookie;
import org.openide.cookies.OpenCookie;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.actions.SystemAction;
import org.openide.util.lookup.AbstractLookup;
import org.openide.util.lookup.InstanceContent;

/**
 *
 * @author stefan
 */
public class BundleDiffNode extends AbstractNode {

    private final InstanceContent lookupContents;

    public BundleDiffNode() {
        this(new InstanceContent());
    }

    private BundleDiffNode(InstanceContent ic) {
        super(Children.LEAF, new AbstractLookup(ic));
        this.lookupContents = ic;
    }

    public BundleDiffNode(BundleDiff diff) {
        this();
        lookupContents.add(diff);

        try {
            
            setDisplayName(diff.getTitle());
            DataObject data = DataObject.find(diff.getBundleFile());
            OpenCookie openCookie = data.getCookie(OpenCookie.class);
            EditCookie editCookie = data.getCookie(EditCookie.class);
            lookupContents.add(openCookie);
            lookupContents.add(editCookie);

        } catch (DataObjectNotFoundException ex) {
            Exceptions.printStackTrace(ex);
        }
    }

    @Override
    public Action[] getActions(boolean context) {
        Action[] action = new Action[]{
            SystemAction.get(OpenAction.class),
            SystemAction.get(EditAction.class)
        };
        return action;
    }
    
    @Override 
    public Action getPreferredAction() { 
        return SystemAction.get(OpenAction.class);
    }

    public final BundleDiff getDiff() {
        return getLookup().lookup(BundleDiff.class);
    }
}
