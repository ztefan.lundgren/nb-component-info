/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.nbdevbundlefinder;

import java.io.File;
import javax.swing.filechooser.FileFilter;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;

/**
 *
 * @author Stefan
 */
public class MimeTypeFileFilter extends FileFilter{
    
    private final String mimeType;
    private final String description;

    public MimeTypeFileFilter(String mimeType, String description) {
        this.mimeType = mimeType;
        this.description = description;
    }
    
    @Override
    public boolean accept(File toTest) {
        FileObject fo = FileUtil.toFileObject(toTest);
        String mimeTypeToTest = FileUtil.getMIMEType(fo);
        return mimeType.equals( mimeTypeToTest );
    }

    @Override
    public String getDescription() {
        return description;
    }
    
}
