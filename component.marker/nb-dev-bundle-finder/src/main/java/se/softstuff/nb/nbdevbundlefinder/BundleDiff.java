package se.softstuff.nb.nbdevbundlefinder;

import java.io.Serializable;
import org.netbeans.api.project.SourceGroup;
import org.openide.filesystems.FileObject;

/**
 *
 * @author stefan
 */
public class BundleDiff implements Serializable{

    private static final long serialVersionUID = -5688706983158855738L;
    static final String TITLE_REPLACE_REGEXP = "\\\\|/";
    
    private FileObject bundleFile;
    private SourceGroup resources;
    
    public BundleDiff() {
        
    }
    public BundleDiff(FileObject bundleFile, SourceGroup resources) {
        this.bundleFile = bundleFile;
        this.resources = resources;        
    }

    public FileObject getBundleFile() {
        return bundleFile;
    }
    
    
    
    
    public String getTitle(){
        String resDir = resources.getRootFolder().getPath();
        String bundleFilePath = bundleFile.getPath();
        String bundleResource = bundleFilePath.substring(resDir.length()+1);
        return bundleResource.replaceAll(TITLE_REPLACE_REGEXP, "\\.");
    }
    
}
