package se.softstuff.nb.nbdevbundlefinder;

import java.util.List;
import org.netbeans.api.java.project.JavaProjectConstants;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.openide.filesystems.FileObject;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;
import org.openide.util.lookup.Lookups;

/**
 *
 * @author stefan
 */
public class ProjectNodeChildFactory extends ChildFactory<BundleDiff> {

    final static String BUNDLE_REGEXP = "bundle.*\\.properties";

    private Project project;

    public ProjectNodeChildFactory(Project project) {
        this.project = project;
    }

    @Override
    protected boolean createKeys(List<BundleDiff> toPopulate) {


        Sources sources = ProjectUtils.getSources(project);
        SourceGroup[] sourceGroups = sources.getSourceGroups(JavaProjectConstants.SOURCES_TYPE_RESOURCES);

        for (SourceGroup resources : sourceGroups) {
            populate(resources.getRootFolder(), toPopulate, resources);
        }
        return true;
    }

    @Override
    protected Node createNodeForKey(BundleDiff key) {
        return new BundleDiffNode(key);
    }

    private void populate(FileObject file, List<BundleDiff> toPopulate, SourceGroup resources) {
        if (isBundleFile(file)) {
            toPopulate.add(new BundleDiff(file, resources));
        }
        for (FileObject child : file.getChildren()) {
            populate(child, toPopulate, resources);
        }
    }
    
    static boolean isBundleFile(FileObject file){
        return file.getNameExt().toLowerCase().matches(BUNDLE_REGEXP);
    }

}
