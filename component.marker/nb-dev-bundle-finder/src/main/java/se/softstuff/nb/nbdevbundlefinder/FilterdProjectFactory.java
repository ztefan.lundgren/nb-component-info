package se.softstuff.nb.nbdevbundlefinder;

import java.util.*;
import org.netbeans.api.java.project.JavaProjectConstants;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ProjectUtils;
import org.netbeans.api.project.SourceGroup;
import org.netbeans.api.project.Sources;
import org.openide.filesystems.FileObject;
import org.openide.nodes.Children;
import org.openide.nodes.FilterNode;
import org.openide.nodes.Node;

/**
 *
 * @author stefan
 */
public class FilterdProjectFactory extends Children.Keys<Project> {

    private boolean filterHasBundle;

    private Set<Project> mapped = new HashSet<Project>();

    public boolean isFilterHasBundle() {
        return filterHasBundle;
    }

    public void setFilterHasBundle(boolean filterHasBundle) {
        this.filterHasBundle = filterHasBundle;
        setKeys(Collections.EMPTY_LIST);
        setKeys(mapped);
    }

    public void add(Project... toAdd) {
        add(Arrays.asList(toAdd));
    }
    
    public void add(Collection<? extends Project> toAdd) {
        mapped.addAll(toAdd);
        setKeys(mapped);
        addNotify();
    }

    @Override
    protected Node[] createNodes(Project project) {
        Node original = new ProjectNode(project);
        if (accept(original)) {
            Project project2 = original.getLookup().lookup(Project.class);
            Node node = new NodeProxy(original);

            return new Node[]{node};
        }
        return new Node[0];
    }

    void removeAll() {
        mapped.clear();
        setKeys(mapped);
    }

    class NodeProxy extends FilterNode {

        public NodeProxy(Node original) {
            super(original, new ProxyChildren(original));
        }
        // add your specialized behavior here...
    }

    class ProxyChildren extends FilterNode.Children {

        public ProxyChildren(Node owner) {
            super(owner);
        }

        @Override
        protected Node copyNode(Node original) {
            return new NodeProxy(original);
        }

        @Override
        protected Node[] createNodes(Node object) {
            List<Node> result = new ArrayList<Node>();

            for (Node node : super.createNodes(object)) {
//                if (accept(node)) {
                    result.add(node);
//                }
            }

            return result.toArray(new Node[0]);
        }
    }

    private boolean accept(Node node) {
        Project project = node.getLookup().lookup(Project.class);
        if (project == null) {
            return true;
        }
        Sources sources = ProjectUtils.getSources(project);
        SourceGroup[] sourceGroups = sources.getSourceGroups(JavaProjectConstants.SOURCES_TYPE_RESOURCES);

        for (SourceGroup resources : sourceGroups) {
            if (containsForBundles(resources.getRootFolder())) {
                return true;
            }
        }
        return !filterHasBundle;
    }

    private boolean containsForBundles(FileObject file) {
        if (file.isData() && ProjectNodeChildFactory.isBundleFile(file)) {
            return true;
        }
        for (FileObject child : file.getChildren()) {
            if (containsForBundles(child)) {
                return true;
            }
        }
        return false;
    }
}
