package se.softstuff.nb.translate;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.io.IOException;
import org.apache.log4j.Logger;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.*;
import org.openide.util.Exceptions;
import org.openide.util.editable.NbBundle;

/**
 *
 * @author Stefan
 */
public class SoftNbTranslateManager implements PropertyChangeListener, VetoableChangeListener, FileChangeListener {
    
    
    
    private static SoftNbTranslateManager instance;
    
    public static SoftNbTranslateManager create(SoftNbTranslateDataObject data){
        if(instance != null){
            throw new IllegalStateException("SoftNbTranslateManager is alreade created, call exit first");
        }
        return instance = new SoftNbTranslateManager(data);
    }    
    
    public static void exit(){
        if(instance != null){
           instance.stop();
        }
        instance = null;
    }
    
    private final Logger log = Logger.getLogger(getClass());
    
    private final SoftNbTranslateDataObject data;
    
    private SoftNbTranslate sot;
    
    private TranslateBundler bundler;
    
    private boolean running;

    public SoftNbTranslateManager(SoftNbTranslateDataObject data) {
        this.data = data;
    }
    
    public boolean isRunning() {
        return running;
    }
    
    public synchronized void start(){
        try {
            if(running){
                throw new IllegalStateException("manager is already running");
            }
            log.debug("start begin");
            FileLock lock = data.getPrimaryFile().lock();
            try {
                data.addPropertyChangeListener(this);
                data.addVetoableChangeListener(this);
                data.getPrimaryFile().addFileChangeListener(this);
                
                sot = data.getContent();
                
                attacheBundler();
                running = true;
            } finally {
                lock.releaseLock();
            }
            log.debug("start done");
        } catch (IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
    
    public synchronized void saveToFile() throws IOException {
        data.getCookie(SaveCookie.class).save();
    }
    
    public synchronized void stop() {
        
        log.debug("stop begin");
        
        releaseBundler();
        
        data.removePropertyChangeListener(this);
        data.removeVetoableChangeListener(this);
        data.getPrimaryFile().removeFileChangeListener(this);
        running = false;
        log.debug("start done");
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        log.debug("propertyChange "+evt);
    }

    @Override
    public void vetoableChange(PropertyChangeEvent evt) throws PropertyVetoException {
        log.debug("vetoableChange "+evt);
    }

    @Override
    public void fileFolderCreated(FileEvent fe) {
        log.debug("fileFolderCreated "+fe);
    }

    @Override
    public void fileDataCreated(FileEvent fe) {
        log.debug("fileDataCreated "+fe);
    }

    @Override
    public void fileChanged(FileEvent fe) {
        log.debug("fileChanged "+fe);
    }

    @Override
    public void fileDeleted(FileEvent fe) {
        log.debug("fileDeleted "+fe);
    }

    @Override
    public void fileRenamed(FileRenameEvent fe) {
        log.debug("fileRenamed "+fe);
    }

    @Override
    public void fileAttributeChanged(FileAttributeEvent fe) {
        log.debug("fileAttributeChanged "+fe);
    }

    private void attacheBundler() {
        
        bundler = new TranslateBundler(sot);
        NbBundle.setBundler(bundler);
    }

    private void releaseBundler() {
        bundler = null;
        NbBundle.setStandardBundler();
    }
    
    
}
