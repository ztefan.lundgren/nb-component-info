package se.softstuff.nb.translate;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.*;

public class SoftNbTranslate {

    public static final String PROP_VERSION = "version";
    
    public static final String PROP_RESOURSE_FILTER = "resourceFilter";
    
    public static final String PROP_RESOURCE_CHANGES = "resourceChanges";
    
    private int version;
    
    private List<String> resourceFilter;
    
    private Map<String, Properties> resourceChanges;

    private transient final PropertyChangeSupport propertyChangeSupport;
    

    public SoftNbTranslate() {
        resourceFilter = new ArrayList<String>();
        resourceChanges = new TreeMap<String, Properties>();
        propertyChangeSupport = new PropertyChangeSupport(this);
    }
    
    

    public List<String> getResourceFilter() {
        return resourceFilter;
    }

    public void setResourceFilter(List<String> resourseFilter) {
        List<String> oldResourseFilter = this.resourceFilter;
        this.resourceFilter = resourseFilter;
        propertyChangeSupport.firePropertyChange(PROP_RESOURSE_FILTER, oldResourseFilter, resourseFilter);
    }

    public void addResourceFilter(String ... newFilter) {
        resourceFilter.addAll(Arrays.asList(newFilter));
        propertyChangeSupport.firePropertyChange(PROP_RESOURSE_FILTER, null, resourceFilter);
    }
    
    public void removeResourceFilter(String filter) {
        if(resourceFilter.remove(filter)) {
            propertyChangeSupport.firePropertyChange(PROP_RESOURSE_FILTER, filter, null);
        }
    }
    
    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        int oldVersion = this.version;
        this.version = version;
        propertyChangeSupport.firePropertyChange(PROP_VERSION, oldVersion, version);
    }
    
    public Map<String, Properties> getResourceChanges() {
        return resourceChanges;
    }

    public void setResourceChanges(Map<String, Properties> resourceChanges) {
        Map<String, Properties> oldChanges = this.resourceChanges;
        this.resourceChanges = resourceChanges;
        propertyChangeSupport.firePropertyChange(PROP_RESOURCE_CHANGES, oldChanges, resourceChanges);
    }
    
    public String addResourceChange(String resouce, String key, String value) {
        if(!resourceChanges.containsKey(resouce)){
            resourceChanges.put(resouce, new Properties());
        }
        Properties bundle = resourceChanges.get(resouce);
        String oldValue = (String)bundle.put(key, value);
        propertyChangeSupport.firePropertyChange(PROP_RESOURCE_CHANGES, null, resourceChanges);
        return oldValue;
    }
    
    public Properties getResourceChanges(String resouce) {
        return resourceChanges.get(resouce);
    }
    
    public boolean contiansChangesFor(String resouce) {
        return resourceChanges.containsKey(resouce);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    

}
