package se.softstuff.nb.translate;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import org.apache.log4j.Logger;
import org.openide.filesystems.FileUtil;
import org.openide.util.editable.EditableNbResourceBundler;
import org.openide.util.Exceptions;
import org.openide.util.editable.EditableProperties;
import org.openide.util.editable.EditableResourceBundle;
import org.openide.util.editable.LocaleIterator;

/**
 *
 * @author Stefan
 */
public class TranslateBundler implements EditableNbResourceBundler {

    
    private final Logger log = Logger.getLogger(getClass());
    
    private String brandingToken;
    
    private final ConcurrentMap<String, EditableResourceBundle> cache;
    
    private final SoftNbTranslate sot;
    
    private final SotUpdater sotUpdater;

    public TranslateBundler(SoftNbTranslate sot) {
        this.sot = sot;
        sotUpdater = new SotUpdater();
        cache = new ConcurrentHashMap<String, EditableResourceBundle>();
    }
    
    
    @Override
    public String getBranding() {
        return brandingToken;
    }

    @Override
    public void setBranding(String brandingToken) throws IllegalArgumentException {
        this.brandingToken = brandingToken;
    }
    

    @Override
    public EditableResourceBundle getBundle(String baseName, Locale locale, ClassLoader loader) throws MissingResourceException {
        return getBundle(brandingToken, baseName, locale, loader);
    }
    
    
    @Override
    public EditableResourceBundle getBundle(String branding, String baseName, Locale locale, ClassLoader loader) {
        
        return getEditableBundle(branding, baseName, locale, loader, true);
    }

    @Override
    public EditableResourceBundle getBundle(String branding, String baseName, Locale locale, ClassLoader loader, boolean useFallback) {
        return getEditableBundle(branding, baseName, locale, loader, useFallback);
    }


    EditableResourceBundle getEditableBundle(String brandingToken, String baseName, Locale locale, ClassLoader loader, boolean useFallback) {
        String key = EditableResourceBundle.createIdentifyer(baseName, brandingToken, locale, loader, useFallback);
        EditableResourceBundle bundle;
        if (!cache.containsKey(key)) {

            bundle = loadBundle(brandingToken, baseName, locale, loader, useFallback);
            bundle.setIdentifyer(key);
            cache.putIfAbsent(key, bundle);
            if (log.isDebugEnabled()) {
                log.debug( String.format("Added bundle to cache, key: %s", key));
            }
        }
        bundle = cache.get(key);

        return bundle;
    }

    public boolean clearCache(EditableResourceBundle bundle) {
        String key = bundle.getIdentifyer();
        if (log.isDebugEnabled()) {
                log.debug( String.format("Clear cache, key: %s", key));
        }
        return cache.remove(key) != null;
    }

    /**
     * Load a resource bundle (without caching).
     *
     * @param baseName the base name of the bundle, e.g.
     * <samp>org.netbeans.modules.foo.Bundle</samp>
     * @param preferredLocale the locale to use
     * @param loader a class loader to search in
     * @return a resource bundle (locale- and branding-merged), or null if not
     * found
     */
    EditableResourceBundle loadBundle(String brandingToken, String baseName, Locale preferredLocale, ClassLoader loader, boolean useFallback) {
        String sname = baseName.replace('.', '/');
        Iterator<String> it = new LocaleIterator(brandingToken, preferredLocale);
        LinkedList<String> langSuffix = new LinkedList<String>();

        if (useFallback) {
            while (it.hasNext()) {
                langSuffix.addFirst(it.next());
            }
        } else {
            String suffix = (brandingToken == null ? "" : brandingToken) + preferredLocale;
            if (!suffix.isEmpty()) {
                suffix = "_" + suffix;
            }
            langSuffix.add(suffix);
        }
        Locale locale = preferredLocale;
        EditableProperties propsFromFile = new EditableProperties(false);

        URL lastUrl = null;
        for (String suffix : langSuffix) {
            String res = String.format("%s%s.properties", sname, suffix);

            // #49961: don't use getResourceAsStream; catch all errors opening it
            URL url = loader != null ? loader.getResource(res) : ClassLoader.getSystemResource(res);

            if (url != null) {
//                locale = LocaleCache.getInstance().findLocale(suffix);
                lastUrl = url;

                //System.err.println("Loading " + res);
                try {
                    // #51667: but in case we are in USE_DEBUG_LOADER mode, use gRAS (since getResource is not overridden)
                    InputStream is = url.openStream();

                    try {
                        propsFromFile.load(is);
                    } finally {
                        is.close();
                    }
                } catch (IOException e) {
                    Exceptions.attachMessage(e, "While loading: " + res); // NOI18N
                    log.warn("failde while loading "+url, e);

                    return null;
                }
            } else if (suffix.length() == 0) {
                // No base *.properties. Try *.class.
                // Note that you may not mix *.properties w/ *.class this way.
//                return loadBundleClass(name, sname, locale, langSuffix, loader);
            }
        }
        
        
        String bundleFile = lastUrl!=null ? lastUrl.getFile() : null;
        
        
        EditableResourceBundle ebundle = new EditableResourceBundle(brandingToken, bundleFile, baseName, locale, propsFromFile);
        if(bundleFile != null){
            String resources = extractResourceKey(bundleFile);
            if(sot.contiansChangesFor(resources)) {
                Map savedChanges = sot.getResourceChanges(resources);            
                ebundle.merge(savedChanges);
            }
        }
        ebundle.addPropertyChangeListener(sotUpdater);
        return ebundle;
    }
    
    private String extractResourceKey(String path) {
            int start = path.indexOf("!/") + 2;//NOI18N
            String jar = path.substring(0,start);
            start = jar.lastIndexOf("modules")+"modules".length()+1;
            String ref = path.substring(start);
            ref = ref.replaceAll("!/", "_");
            ref = ref.replaceAll("/", ".");
            
            return ref;
        
        }
    
    private class SotUpdater implements PropertyChangeListener {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            EditableResourceBundle.EditablePropertyChangeEvent event = (EditableResourceBundle.EditablePropertyChangeEvent)evt;
            String resources = extractResourceKey(event.getBundleFile());
            sot.addResourceChange(resources, event.getBundleKey(), event.getNewValue());
            EditableResourceBundle bundle = (EditableResourceBundle)event.getSource();
            cache.clear();
        }
        
        
    }

    
}
