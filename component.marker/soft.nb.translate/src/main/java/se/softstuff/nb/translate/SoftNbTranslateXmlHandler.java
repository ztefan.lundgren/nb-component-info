package se.softstuff.nb.translate;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;
import java.io.*;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;
import java.util.Map.Entry;

/**
 *
 * @author Stefan
 */
public class SoftNbTranslateXmlHandler {
    
    private static final String UTF8 = "UTF8";

    static String toXml(SoftNbTranslate sot) {
        StringWriter out = new StringWriter();
        toXml(sot, out);
        String xml = out.toString();
         System.out.println(xml);
        return xml;
    }
    static void toXml(SoftNbTranslate sot, Writer out) {
        
        XStream xstream = buildXStream();
        xstream.toXML(sot, out);
        
    }
    
    static void toXml(SoftNbTranslate sot, OutputStream out) {
        
        XStream xstream = buildXStream();
        xstream.toXML(sot, out);
        
    }

    static SoftNbTranslate fromXml(String xml) {
        Reader reader = new StringReader(xml);
        return fromXml(reader);
    }
    static SoftNbTranslate fromXml(Reader xml) {
        XStream xstream = buildXStream();
        SoftNbTranslate sot = (SoftNbTranslate)xstream.fromXML(xml, new SoftNbTranslate());
        return sot;
    }
    
    
    private static XStream buildXStream() {
        XStream xstream = new XStream(new DomDriver());
        xstream.alias("sot", SoftNbTranslate.class);
        xstream.registerConverter(new ChangesConverter());
        return xstream;
    }

    public static class ChangesConverter implements Converter {

        @Override
        public boolean canConvert(Class clazz) {
            return AbstractMap.class.isAssignableFrom(clazz);
        }

        @Override
        public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
            AbstractMap<String, Properties> map = (AbstractMap<String, Properties>) value;
            for (Entry<String, Properties> entry : map.entrySet()) {
                writer.startNode(entry.getKey());
                for(Entry<Object,Object> change : entry.getValue().entrySet()) {
                    writer.startNode((String)change.getKey());
                    writer.setValue(encode(change.getValue()));
                    writer.endNode();
                }
                writer.endNode();
            }
        }

        @Override
        public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
            Map<String, Properties> map = new HashMap<String, Properties>();

            while (reader.hasMoreChildren()) {
                reader.moveDown();
                String resource = reader.getNodeName();
                Properties changes = new Properties();                
                map.put(resource, changes);
                while (reader.hasMoreChildren()) {
                    reader.moveDown();
                    changes.put(reader.getNodeName(), decode(reader.getValue()));
                    reader.moveUp();
                }
                reader.moveUp();
            }
            return map;
        }
        
        private String encode(Object value){
            try {
                return URLEncoder.encode(value.toString(), UTF8);
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException(ex);
            }
        }
        private String decode(String value){
            try {
                return URLDecoder.decode(value, UTF8);
            } catch (UnsupportedEncodingException ex) {
                throw new RuntimeException(ex);
            }
        }
    }
}
