/*
 * Copyright 2012 ztefan.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.nb.translate;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import org.apache.log4j.Logger;

/**
 *
 * @author Stefan
 */
public class StartStopKeyEventDispatcher implements KeyEventDispatcher {
    
    private static final Logger LOG = Logger.getLogger(StartStopKeyEventDispatcher.class);    

    private static boolean registerd;
    private static final StartStopKeyEventDispatcher INSTANCE = new StartStopKeyEventDispatcher();
    
    public static StartStopKeyEventDispatcher getInstance(){
        return INSTANCE;
    }
    
    public static synchronized void registerDispatcher(){
        if(registerd){
            throw new IllegalStateException("StartStopKeyEventDispatcher is allready registerd");
        }
        KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        kfm.addKeyEventDispatcher(INSTANCE);
        registerd = true;
        LOG.info("StartStopKeyEventDispatcher registerd");
    }
    
    public static synchronized void unregisterDispatcher(){
        if(!registerd){
            KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
            kfm.removeKeyEventDispatcher(INSTANCE);
            registerd = false;
            LOG.info("StartStopKeyEventDispatcher unregisterd");
        }
    }
    /**
     * map containing all global actions
     */
    private HashMap<KeyStroke, Action> actionMap = new HashMap<KeyStroke, Action>();

    public void addAction(KeyStroke key, Action action){
        actionMap.put(key, action);
    }
    
    public void clearAction(KeyStroke key) {
        actionMap.remove(key);
    }
//    
//    /**
//     * call this somewhere in your GUI construction
//     */
//    private void setup() {
//        KeyStroke key1 = KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_DOWN_MASK);
//        actionMap.put(key1, new AbstractAction("action1") {
//
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println("Ctrl-A pressed: " + e);
//            }
//        });
//        // add more actions..
//
//        KeyboardFocusManager kfm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
//        kfm.addKeyEventDispatcher(new KeyEventDispatcher() {
//
//            @Override
//            public boolean dispatchKeyEvent(KeyEvent e) {
//                KeyStroke keyStroke = KeyStroke.getKeyStrokeForEvent(e);
//                if (actionMap.containsKey(keyStroke)) {
//                    final Action a = actionMap.get(keyStroke);
//                    final ActionEvent ae = new ActionEvent(e.getSource(), e.getID(), null);
//                    SwingUtilities.invokeLater(new Runnable() {
//
//                        @Override
//                        public void run() {
//                            a.actionPerformed(ae);
//                        }
//                    });
//                    return true;
//                }
//                return false;
//            }
//        });
//    }
//    
    @Override
    public boolean dispatchKeyEvent(KeyEvent e) {
        
         KeyStroke keyStroke = KeyStroke.getKeyStrokeForEvent(e);
        if (actionMap.containsKey(keyStroke)) {
            
            final Action a = actionMap.get(keyStroke);
            final ActionEvent ae = new ActionEvent(e.getSource(), e.getID(), null);
            SwingUtilities.invokeLater(new Runnable() {

                @Override
                public void run() {
                    a.actionPerformed(ae);
                }
            });
            return true;
        }
        return false;
    }
}
