package se.softstuff.nb.translate;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import org.apache.log4j.Logger;
import org.netbeans.core.spi.multiview.MultiViewElement;
import org.netbeans.core.spi.multiview.text.MultiViewEditorElement;
import org.openide.cookies.SaveCookie;
import org.openide.filesystems.FileLock;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObjectExistsException;
import org.openide.loaders.MultiDataObject;
import org.openide.loaders.MultiFileLoader;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.util.NbBundle.Messages;
import org.openide.windows.TopComponent;

public class SoftNbTranslateDataObject extends MultiDataObject {

    private final Logger log = Logger.getLogger(getClass());
    
    private SoftNbTranslate sot;
    

    public SoftNbTranslateDataObject(FileObject pf, MultiFileLoader loader) throws DataObjectExistsException, IOException {
        super(pf, loader);
        registerEditor("application/soft_translate+xml", true); //NOI18N

        sot = read();
        sot.addPropertyChangeListener(new ChangeTracker());
        
        getCookieSet().assign(SaveCookie.class, new Saver()); 
    }

    public SoftNbTranslate getContent() {
        return sot;
    }
    
    private SoftNbTranslate read() {
        
        log.trace("getContent begin");
        InputStream inputStream = null;
        try {
            inputStream = getPrimaryFile().getInputStream();
            InputStreamReader reader = new InputStreamReader(inputStream);
            SoftNbTranslate sot = SoftNbTranslateXmlHandler.fromXml(reader);
            log.trace("getContent end");
            return sot;
        } catch (FileNotFoundException ex) {
            throw new RuntimeException(ex);
        } finally {
            try {
                inputStream.close();
            } catch (IOException ignore) {
            }
        }
    }
//
//    public void save(SoftNbTranslate sot) throws IOException {
//
//        FileLock lock = getPrimaryFile().lock();
//        try {
//            save(sot, lock);
//        } finally {
//            lock.releaseLock();
//        }
//    }
//
//    public void save(SoftNbTranslate sot, FileLock lock) throws IOException {
//
//        OutputStream output = null;
//        try {
//            output = getPrimaryFile().getOutputStream(lock);
//            SoftNbTranslateXmlHandler.toXml(sot, output);
//        } finally {
//            try {
//                output.close();
//            } catch (IOException ignore) {
//            }
//        }
//    }

    @Override
    protected int associateLookup() {
        return 1;
    }

    @MultiViewElement.Registration(displayName = "#LBL_SoftNbTranslate_EDITOR",
    iconBase = "se/softstuff/nb/translate/nb_translate.png",
    mimeType = "application/soft_translate+xml",
    persistenceType = TopComponent.PERSISTENCE_ONLY_OPENED,
    preferredID = "SoftNbTranslate",
    position = 1000)
    @Messages("LBL_SoftNbTranslate_EDITOR=Source")
    public static MultiViewEditorElement createEditor(Lookup lkp) {
        return new MultiViewEditorElement(lkp);
    }

    private class Saver implements SaveCookie {

        @Override
        public void save() throws IOException {


            FileLock lock = getPrimaryFile().lock();
            OutputStream output = null;
            try {
                output = getPrimaryFile().getOutputStream(lock);
                SoftNbTranslateXmlHandler.toXml(sot, output);
            } finally {
                try {
                    output.close();
                } catch (IOException ignore) {
                }
                lock.releaseLock();
            }

        }
    }
    
    private class ChangeTracker implements PropertyChangeListener {

        @Override
        public void propertyChange(PropertyChangeEvent evt) {            
            SoftNbTranslateDataObject.this.setModified(true);
        }        
    }
}
