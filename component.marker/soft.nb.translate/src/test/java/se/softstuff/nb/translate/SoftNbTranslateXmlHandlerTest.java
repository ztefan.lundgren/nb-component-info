package se.softstuff.nb.translate;

import org.junit.*;
import static org.junit.Assert.*;



/**
 *
 * @author Stefan
 */
public class SoftNbTranslateXmlHandlerTest {
    
    private SoftNbTranslate testSot;
    private String testXml;
    
    @Before
    public void setUp() {
        
        testSot = new SoftNbTranslate();
        testSot.setVersion(123);
        testSot.addResourceFilter("se.softstuff", "eu.softstuff");
        testSot.addResourceChange("se.softstuff.nb.translate.Bundle_sv_SE.properties", "SoftNbTranslateXmlHandlerTest.title", "translator");
        
        testXml = 
"<sot>\n"+
"  <version>123</version>\n"+               
"  <resourceFilter>\n"+
"    <string>se.softstuff</string>\n"+
"    <string>eu.softstuff</string>\n"+
"  </resourceFilter>\n"+
"  <resourceChanges class=\"tree-map\">\n"+
"    <se.softstuff.nb.translate.Bundle__sv__SE.properties>\n"+
"      <SoftNbTranslateXmlHandlerTest.title>translator</SoftNbTranslateXmlHandlerTest.title>\n"+
"    </se.softstuff.nb.translate.Bundle__sv__SE.properties>\n"+
"  </resourceChanges>\n"+                   
"</sot>"; 
    }

    @Test
    public void toXml() {
        String xml = SoftNbTranslateXmlHandler.toXml(testSot);
        assertEquals(testXml, xml);
    }
    
    
    @Test
    public void fromXml() {
        SoftNbTranslate sot = SoftNbTranslateXmlHandler.fromXml(testXml);
        assertEquals(testSot.getVersion(), sot.getVersion());
        assertEquals(testSot.getResourceFilter().size(), sot.getResourceFilter().size());
        assertEquals(testSot.getResourceChanges().size(), sot.getResourceChanges().size());
    }
}
